-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 09, 2016 at 02:24 PM
-- Server version: 5.6.25
-- PHP Version: 5.5.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `plumber_script`
--

-- --------------------------------------------------------

--
-- Table structure for table `materials`
--

CREATE TABLE IF NOT EXISTS `materials` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `unit_price` double(9,2) NOT NULL,
  `type` int(5) NOT NULL,
  `org_id` int(11) DEFAULT NULL,
  `key` varchar(30) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `materials`
--

INSERT INTO `materials` (`id`, `name`, `unit_price`, `type`, `org_id`, `key`) VALUES
(1, 'PVC Pipe per metre', 200.00, 1, NULL, NULL),
(2, 'Junction', 152.00, 1, NULL, NULL),
(3, 'Inspection opening', 262.00, 1, NULL, NULL),
(4, 'Concrete BTS wa', 150.00, 1, NULL, NULL),
(5, 'Glue', 268.00, 1, NULL, NULL),
(6, 'Cleaner', 187.00, 1, NULL, NULL),
(7, 'Earthenware to PVC PQ', 232.00, 1, NULL, NULL),
(8, 'PVC to PVC PQ', 299.00, 1, NULL, NULL),
(9, 'Slab Repair', 297.00, 1, NULL, NULL),
(10, '45 Bend z', 286.00, 1, NULL, NULL),
(11, '90 Bend', 239.00, 1, NULL, NULL),
(12, 'Earthenware to slab repair coupling', 240.00, 1, NULL, NULL),
(13, 'Silicone', 179.00, 1, NULL, NULL),
(14, 'Sand/Cement', 279.00, 1, NULL, NULL),
(15, 'Gravel', 157.00, 1, NULL, NULL),
(16, 'Top Soil', 248.00, 1, NULL, NULL),
(17, 'Turf', 268.00, 1, NULL, NULL),
(18, 'Rubbish', 295.00, 1, NULL, NULL),
(19, 'Road Saw', 169.00, 2, NULL, NULL),
(20, 'Jackhammer', 265.00, 2, NULL, NULL),
(21, 'Wheelbarrow', 114.00, 2, NULL, NULL),
(22, 'Petrol', 281.00, 2, NULL, NULL),
(23, 'Hose', 157.00, 2, NULL, NULL),
(24, 'Spade/Shovel', 243.00, 2, NULL, NULL),
(25, 'Jet', 244.00, 2, NULL, NULL),
(26, 'CCTV', 192.00, 2, NULL, NULL),
(27, 'Broom', 129.00, 2, NULL, NULL),
(28, 'Barricades', 170.00, 2, NULL, NULL),
(29, 'Steel', 160.00, 2, NULL, NULL),
(30, 'Timber', 192.00, 2, NULL, NULL),
(31, 'Reo Bars', 181.00, 2, NULL, NULL),
(32, 'Stencil', 229.00, 2, NULL, NULL),
(33, 'Sewer Diagram', 100.00, 2, NULL, NULL),
(34, 'DBYD', 116.00, 2, NULL, NULL),
(35, 'Concrete Taxi', 180.00, 2, NULL, NULL),
(36, 'Timber Float', 252.00, 2, NULL, NULL),
(37, 'Council Fees', 220.00, 3, NULL, NULL),
(38, 'Road Plates / Week', 245.00, 3, NULL, NULL),
(39, 'Plate Compactor', 262.00, 3, NULL, NULL),
(40, 'Submersible Pump', 275.00, 3, NULL, NULL),
(41, 'Generator', 290.00, 3, NULL, NULL),
(42, 'Excavator', 121.00, 3, NULL, NULL),
(43, 'Steel Float', 242.00, 3, NULL, NULL),
(44, 'Day Rate', 141.00, 1, NULL, 'day_rate'),
(45, 'Hourly Rate', 282.00, 1, NULL, 'hourly_rate'),
(46, 'First Day Access', 287.00, 1, NULL, 'firstday_machineaccess'),
(47, 'All Day Access', 287.00, 1, NULL, 'alldays_machineaccess');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`version`) VALUES
(8);

-- --------------------------------------------------------

--
-- Table structure for table `organisations`
--

CREATE TABLE IF NOT EXISTS `organisations` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `organisations`
--

INSERT INTO `organisations` (`id`, `title`, `logo`) VALUES
(1, 'Organisation 1', 'Organisation Logo');

-- --------------------------------------------------------

--
-- Table structure for table `quotes`
--

CREATE TABLE IF NOT EXISTS `quotes` (
  `id` int(11) NOT NULL,
  `surface_id` int(11) NOT NULL,
  `org_id` int(11) DEFAULT NULL,
  `won` tinyint(1) DEFAULT NULL,
  `marked_for_deletion` timestamp NULL DEFAULT NULL,
  `ref_id` varchar(50) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `deleting_on_date` date DEFAULT NULL,
  `sub_cost` double(9,2) DEFAULT NULL,
  `margin` double(9,2) DEFAULT NULL,
  `extra_cost` double(9,2) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quotes`
--

INSERT INTO `quotes` (`id`, `surface_id`, `org_id`, `won`, `marked_for_deletion`, `ref_id`, `address`, `deleting_on_date`, `sub_cost`, `margin`, `extra_cost`, `created_by`) VALUES
(1, 1, 1, NULL, NULL, '12', 'rawe', NULL, NULL, NULL, NULL, NULL),
(2, 1, 1, NULL, NULL, 'ttest', 'test', NULL, NULL, NULL, NULL, 2),
(3, 2, 1, NULL, NULL, '223', 'test', NULL, 174143.00, 10.00, 23.00, 2),
(4, 1, 1, NULL, NULL, '23', 'test', NULL, 149801.00, NULL, NULL, 4);

-- --------------------------------------------------------

--
-- Table structure for table `quote_materials`
--

CREATE TABLE IF NOT EXISTS `quote_materials` (
  `id` int(11) NOT NULL,
  `quote_id` int(11) NOT NULL,
  `material_name` varchar(255) NOT NULL,
  `unit_price` double(9,2) NOT NULL,
  `quantity` int(5) NOT NULL,
  `total_price` double(9,2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=148 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quote_materials`
--

INSERT INTO `quote_materials` (`id`, `quote_id`, `material_name`, `unit_price`, `quantity`, `total_price`) VALUES
(1, 1, 'Junction', 152.00, 44, 6688.00),
(2, 1, 'Inspection opening', 262.00, 32, 8384.00),
(3, 1, 'Concrete BTS', 150.00, 35, 5250.00),
(4, 1, 'Glue', 268.00, 28, 7504.00),
(5, 1, 'Cleaner', 187.00, 44, 8228.00),
(6, 1, 'Earthenware to PVC PQ', 232.00, 33, 7656.00),
(7, 1, 'PVC to PVC PQ', 299.00, 9, 2691.00),
(8, 1, 'Slab Repair', 297.00, 7, 2079.00),
(9, 1, '45 Bend', 286.00, 49, 14014.00),
(10, 1, '90 Bend', 239.00, 12, 2868.00),
(11, 1, 'Earthenware to slab repair coupling', 240.00, 13, 3120.00),
(12, 1, 'Silicone', 179.00, 14, 2506.00),
(13, 1, 'Sand/Cement', 279.00, 9, 2511.00),
(14, 1, 'Gravel', 157.00, 43, 6751.00),
(15, 1, 'Rubbish', 295.00, 2, 590.00),
(16, 1, 'Road Saw', 169.00, 29, 4901.00),
(17, 1, 'Jackhammer', 265.00, 7, 1855.00),
(18, 1, 'Wheelbarrow', 114.00, 19, 2166.00),
(19, 1, 'Petrol', 281.00, 32, 8992.00),
(20, 1, 'Hose', 157.00, 17, 2669.00),
(21, 1, 'Spade/Shovel', 243.00, 28, 6804.00),
(22, 1, 'Jet', 244.00, 22, 5368.00),
(23, 1, 'CCTV', 192.00, 7, 1344.00),
(24, 1, 'Broom', 129.00, 39, 5031.00),
(25, 1, 'Barricades', 170.00, 38, 6460.00),
(26, 1, 'Steel', 160.00, 50, 8000.00),
(27, 1, 'Timber', 192.00, 13, 2496.00),
(28, 1, 'Reo Bars', 181.00, 14, 2534.00),
(29, 1, 'Steel Float', 242.00, 2, 484.00),
(30, 1, 'Timber Float', 252.00, 25, 6300.00),
(31, 1, 'Excavator', 121.00, 50, 6050.00),
(32, 1, 'PVC Pipe per metre', 200.00, 8, 1600.00),
(33, 1, 'Day Rate', 141.00, 23, 3243.00),
(34, 1, 'First Day Access', 287.00, 1, 287.00),
(35, 1, 'Council Fees', 220.00, 1, 220.00),
(36, 1, 'Plate Compactor', 262.00, 1, 262.00),
(37, 2, 'Junction', 152.00, 44, 6688.00),
(38, 2, 'Inspection opening', 262.00, 32, 8384.00),
(39, 2, 'Concrete BTS', 150.00, 35, 5250.00),
(40, 2, 'Glue', 268.00, 28, 7504.00),
(41, 2, 'Cleaner', 187.00, 44, 8228.00),
(42, 2, 'Earthenware to PVC PQ', 232.00, 33, 7656.00),
(43, 2, 'PVC to PVC PQ', 299.00, 9, 2691.00),
(44, 2, 'Slab Repair', 297.00, 7, 2079.00),
(45, 2, '45 Bend', 286.00, 49, 14014.00),
(46, 2, '90 Bend', 239.00, 12, 2868.00),
(47, 2, 'Earthenware to slab repair coupling', 240.00, 13, 3120.00),
(48, 2, 'Silicone', 179.00, 14, 2506.00),
(49, 2, 'Sand/Cement', 279.00, 9, 2511.00),
(50, 2, 'Gravel', 157.00, 43, 6751.00),
(51, 2, 'Rubbish', 295.00, 2, 590.00),
(52, 2, 'Road Saw', 169.00, 29, 4901.00),
(53, 2, 'Jackhammer', 265.00, 7, 1855.00),
(54, 2, 'Wheelbarrow', 114.00, 19, 2166.00),
(55, 2, 'Petrol', 281.00, 32, 8992.00),
(56, 2, 'Hose', 157.00, 17, 2669.00),
(57, 2, 'Spade/Shovel', 243.00, 28, 6804.00),
(58, 2, 'Jet', 244.00, 22, 5368.00),
(59, 2, 'CCTV', 192.00, 7, 1344.00),
(60, 2, 'Broom', 129.00, 39, 5031.00),
(61, 2, 'Barricades', 170.00, 38, 6460.00),
(62, 2, 'Steel', 160.00, 50, 8000.00),
(63, 2, 'Timber', 192.00, 13, 2496.00),
(64, 2, 'Reo Bars', 181.00, 14, 2534.00),
(65, 2, 'Steel Float', 242.00, 2, 484.00),
(66, 2, 'Timber Float', 252.00, 25, 6300.00),
(67, 2, 'Excavator', 121.00, 50, 6050.00),
(68, 2, 'PVC Pipe per metre', 200.00, 9, 1800.00),
(69, 2, 'Days Required', 141.00, 4, 564.00),
(70, 2, 'Days of Machine Access', 287.00, 5, 1435.00),
(71, 2, 'Council Fees', 220.00, 1, 220.00),
(72, 2, 'Road Plates / Week', 245.00, 1, 245.00),
(73, 2, 'Plate Compactor', 262.00, 1, 262.00),
(74, 3, 'Junction', 152.00, 27, 4104.00),
(75, 3, 'Inspection opening', 262.00, 39, 10218.00),
(76, 3, 'Concrete BTS', 150.00, 21, 3150.00),
(77, 3, 'Glue', 268.00, 16, 4288.00),
(78, 3, 'Cleaner', 187.00, 2, 374.00),
(79, 3, 'Earthenware to PVC PQ', 232.00, 12, 2784.00),
(80, 3, 'PVC to PVC PQ', 299.00, 50, 14950.00),
(81, 3, 'Slab Repair', 297.00, 34, 10098.00),
(82, 3, '45 Bend', 286.00, 18, 5148.00),
(83, 3, '90 Bend', 239.00, 7, 1673.00),
(84, 3, 'Earthenware to slab repair coupling', 240.00, 4, 960.00),
(85, 3, 'Silicone', 179.00, 25, 4475.00),
(86, 3, 'Sand/Cement', 279.00, 14, 3906.00),
(87, 3, 'Gravel', 157.00, 10, 1570.00),
(88, 3, 'Rubbish', 295.00, 41, 12095.00),
(89, 3, 'Road Saw', 169.00, 7, 1183.00),
(90, 3, 'Jackhammer', 265.00, 20, 5300.00),
(91, 3, 'Wheelbarrow', 114.00, 50, 5700.00),
(92, 3, 'Petrol', 281.00, 19, 5339.00),
(93, 3, 'Hose', 157.00, 48, 7536.00),
(94, 3, 'Spade/Shovel', 243.00, 31, 7533.00),
(95, 3, 'Jet', 244.00, 42, 10248.00),
(96, 3, 'CCTV', 192.00, 47, 9024.00),
(97, 3, 'Broom', 129.00, 25, 3225.00),
(98, 3, 'Barricades', 170.00, 15, 2550.00),
(99, 3, 'Steel', 160.00, 44, 7040.00),
(100, 3, 'Timber', 192.00, 28, 5376.00),
(101, 3, 'Reo Bars', 181.00, 35, 6335.00),
(102, 3, 'Steel Float', 242.00, 14, 3388.00),
(103, 3, 'Timber Float', 252.00, 9, 2268.00),
(104, 3, 'Excavator', 121.00, 46, 5566.00),
(105, 3, 'PVC Pipe per metre', 200.00, 6, 1200.00),
(106, 3, 'Days Required', 141.00, 23, 3243.00),
(107, 3, 'Days of Machine Access', 287.00, 3, 861.00),
(108, 3, 'Road Plates / Week', 245.00, 1, 245.00),
(109, 3, 'Plate Compactor', 262.00, 1, 262.00),
(110, 3, 'Submersible Pump', 275.00, 1, 275.00),
(111, 3, 'Generator', 290.00, 1, 290.00),
(112, 3, 'Excavator', 121.00, 1, 121.00),
(113, 3, 'Steel Float', 242.00, 1, 242.00),
(114, 4, 'Junction', 152.00, 2, 304.00),
(115, 4, 'Inspection opening', 262.00, 32, 8384.00),
(116, 4, 'Concrete BTS', 150.00, 35, 5250.00),
(117, 4, 'Glue', 268.00, 28, 7504.00),
(118, 4, 'Cleaner', 187.00, 44, 8228.00),
(119, 4, 'Earthenware to PVC PQ', 232.00, 33, 7656.00),
(120, 4, 'PVC to PVC PQ', 299.00, 9, 2691.00),
(121, 4, 'Slab Repair', 297.00, 7, 2079.00),
(122, 4, '45 Bend', 286.00, 49, 14014.00),
(123, 4, '90 Bend', 239.00, 12, 2868.00),
(124, 4, 'Earthenware to slab repair coupling', 240.00, 13, 3120.00),
(125, 4, 'Silicone', 179.00, 14, 2506.00),
(126, 4, 'Sand/Cement', 279.00, 9, 2511.00),
(127, 4, 'Gravel', 157.00, 43, 6751.00),
(128, 4, 'Rubbish', 295.00, 2, 590.00),
(129, 4, 'Road Saw', 169.00, 29, 4901.00),
(130, 4, 'Jackhammer', 265.00, 7, 1855.00),
(131, 4, 'Wheelbarrow', 114.00, 19, 2166.00),
(132, 4, 'Petrol', 281.00, 32, 8992.00),
(133, 4, 'Hose', 157.00, 17, 2669.00),
(134, 4, 'Spade/Shovel', 243.00, 28, 6804.00),
(135, 4, 'Jet', 244.00, 22, 5368.00),
(136, 4, 'CCTV', 192.00, 7, 1344.00),
(137, 4, 'Broom', 129.00, 39, 5031.00),
(138, 4, 'Barricades', 170.00, 38, 6460.00),
(139, 4, 'Steel', 160.00, 50, 8000.00),
(140, 4, 'Timber', 192.00, 13, 2496.00),
(141, 4, 'Reo Bars', 181.00, 14, 2534.00),
(142, 4, 'Steel Float', 242.00, 2, 484.00),
(143, 4, 'Timber Float', 252.00, 25, 6300.00),
(144, 4, 'Excavator', 121.00, 50, 6050.00),
(145, 4, 'PVC Pipe per metre', 200.00, 14, 2800.00),
(146, 4, 'Hours Required', 282.00, 3, 846.00),
(147, 4, 'Road Plates / Week', 245.00, 1, 245.00);

-- --------------------------------------------------------

--
-- Table structure for table `surfaceareas_materials_dynamic`
--

CREATE TABLE IF NOT EXISTS `surfaceareas_materials_dynamic` (
  `id` int(11) NOT NULL,
  `surface_id` int(11) NOT NULL,
  `material_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `quote_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `surface_areas`
--

CREATE TABLE IF NOT EXISTS `surface_areas` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `script` varchar(5000) NOT NULL,
  `org_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `surface_areas`
--

INSERT INTO `surface_areas` (`id`, `name`, `script`, `org_id`) VALUES
(1, 'Concrete', '', NULL),
(2, 'Pebblecrete', '', NULL),
(3, 'Bitumen', '', NULL),
(4, 'Pavers', '', NULL),
(5, 'Grass / Garden', '', NULL),
(6, 'Grass - Concrete', '', NULL),
(7, 'Grass - Bitumen', '', NULL),
(8, 'Grass - Pebblecrete', '', NULL),
(9, 'Grass - Pavers', '', NULL),
(10, 'Concrete - Bitumen', '', NULL),
(11, 'Concrete - Pebblecrete', '', NULL),
(12, 'Concrete - Pavers', '', NULL),
(13, 'Bitumen - Pebblecrete', '', NULL),
(14, 'Bitumen - Pavers', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `surface_areas_materials`
--

CREATE TABLE IF NOT EXISTS `surface_areas_materials` (
  `id` int(11) NOT NULL,
  `surface_id` int(11) NOT NULL,
  `material_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=431 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `surface_areas_materials`
--

INSERT INTO `surface_areas_materials` (`id`, `surface_id`, `material_id`, `quantity`) VALUES
(16, 1, 2, 44),
(17, 2, 2, 27),
(18, 3, 2, 2),
(19, 4, 2, 27),
(20, 5, 2, 31),
(21, 6, 2, 24),
(22, 7, 2, 27),
(23, 8, 2, 10),
(24, 9, 2, 18),
(25, 10, 2, 11),
(26, 11, 2, 50),
(27, 12, 2, 18),
(28, 13, 2, 39),
(29, 14, 2, 40),
(30, 1, 3, 32),
(31, 2, 3, 39),
(32, 3, 3, 47),
(33, 4, 3, 20),
(34, 5, 3, 7),
(35, 6, 3, 27),
(36, 7, 3, 12),
(37, 8, 3, 29),
(38, 9, 3, 9),
(39, 10, 3, 6),
(40, 11, 3, 2),
(41, 12, 3, 42),
(42, 13, 3, 6),
(43, 14, 3, 50),
(44, 1, 4, 35),
(45, 2, 4, 21),
(46, 3, 4, 1),
(47, 4, 4, 42),
(48, 5, 4, 7),
(49, 6, 4, 9),
(50, 7, 4, 24),
(51, 8, 4, 40),
(52, 9, 4, 29),
(53, 10, 4, 25),
(54, 11, 4, 39),
(55, 12, 4, 16),
(56, 13, 4, 15),
(57, 14, 4, 25),
(58, 1, 5, 28),
(59, 2, 5, 16),
(60, 3, 5, 46),
(61, 4, 5, 31),
(62, 5, 5, 18),
(63, 6, 5, 45),
(64, 7, 5, 22),
(65, 8, 5, 24),
(66, 9, 5, 3),
(67, 10, 5, 41),
(68, 11, 5, 47),
(69, 12, 5, 13),
(70, 13, 5, 22),
(71, 14, 5, 22),
(72, 1, 6, 44),
(73, 2, 6, 2),
(74, 3, 6, 28),
(75, 4, 6, 33),
(76, 5, 6, 30),
(77, 6, 6, 49),
(78, 7, 6, 8),
(79, 8, 6, 41),
(80, 9, 6, 29),
(81, 10, 6, 24),
(82, 11, 6, 33),
(83, 12, 6, 43),
(84, 13, 6, 12),
(85, 14, 6, 34),
(86, 1, 7, 33),
(87, 2, 7, 12),
(88, 3, 7, 12),
(89, 4, 7, 22),
(90, 5, 7, 22),
(91, 6, 7, 45),
(92, 7, 7, 9),
(93, 8, 7, 8),
(94, 9, 7, 12),
(95, 10, 7, 34),
(96, 11, 7, 36),
(97, 12, 7, 29),
(98, 13, 7, 34),
(99, 14, 7, 32),
(100, 1, 8, 9),
(101, 2, 8, 50),
(102, 3, 8, 22),
(103, 4, 8, 7),
(104, 5, 8, 20),
(105, 6, 8, 27),
(106, 7, 8, 27),
(107, 8, 8, 3),
(108, 9, 8, 31),
(109, 10, 8, 48),
(110, 11, 8, 46),
(111, 12, 8, 33),
(112, 13, 8, 30),
(113, 14, 8, 49),
(114, 1, 9, 7),
(115, 2, 9, 34),
(116, 3, 9, 49),
(117, 4, 9, 42),
(118, 5, 9, 13),
(119, 6, 9, 37),
(120, 7, 9, 47),
(121, 8, 9, 21),
(122, 9, 9, 16),
(123, 10, 9, 18),
(124, 11, 9, 38),
(125, 12, 9, 37),
(126, 13, 9, 21),
(127, 14, 9, 42),
(128, 1, 10, 49),
(129, 2, 10, 18),
(130, 3, 10, 42),
(131, 4, 10, 7),
(132, 5, 10, 9),
(133, 6, 10, 20),
(134, 7, 10, 26),
(135, 8, 10, 19),
(136, 9, 10, 16),
(137, 10, 10, 21),
(138, 11, 10, 6),
(139, 12, 10, 19),
(140, 13, 10, 25),
(141, 14, 10, 17),
(142, 1, 11, 12),
(143, 2, 11, 7),
(144, 3, 11, 48),
(145, 4, 11, 18),
(146, 5, 11, 47),
(147, 6, 11, 32),
(148, 7, 11, 16),
(149, 8, 11, 35),
(150, 9, 11, 27),
(151, 10, 11, 28),
(152, 11, 11, 9),
(153, 12, 11, 10),
(154, 13, 11, 24),
(155, 14, 11, 37),
(156, 1, 12, 13),
(157, 2, 12, 4),
(158, 3, 12, 29),
(159, 4, 12, 35),
(160, 5, 12, 35),
(161, 6, 12, 21),
(162, 7, 12, 47),
(163, 8, 12, 25),
(164, 9, 12, 31),
(165, 10, 12, 28),
(166, 11, 12, 50),
(167, 12, 12, 13),
(168, 13, 12, 14),
(169, 14, 12, 31),
(170, 1, 13, 14),
(171, 2, 13, 25),
(172, 3, 13, 33),
(173, 4, 13, 40),
(174, 5, 13, 1),
(175, 6, 13, 32),
(176, 7, 13, 9),
(177, 8, 13, 49),
(178, 9, 13, 16),
(179, 10, 13, 32),
(180, 11, 13, 13),
(181, 12, 13, 20),
(182, 13, 13, 7),
(183, 14, 13, 26),
(184, 1, 14, 9),
(185, 2, 14, 14),
(186, 3, 14, 45),
(187, 4, 14, 32),
(188, 5, 14, 24),
(189, 6, 14, 24),
(190, 7, 14, 47),
(191, 8, 14, 13),
(192, 9, 14, 21),
(193, 10, 14, 19),
(194, 11, 14, 31),
(195, 12, 14, 46),
(196, 13, 14, 38),
(197, 14, 14, 2),
(198, 1, 15, 43),
(199, 2, 15, 10),
(200, 3, 15, 22),
(201, 4, 15, 29),
(202, 6, 15, 26),
(203, 7, 15, 46),
(204, 8, 15, 49),
(205, 9, 15, 6),
(206, 10, 15, 32),
(207, 11, 15, 42),
(208, 12, 15, 13),
(209, 13, 15, 39),
(210, 14, 15, 5),
(211, 5, 16, 10),
(212, 6, 16, 32),
(213, 7, 16, 32),
(214, 8, 16, 10),
(215, 9, 16, 7),
(216, 1, 18, 2),
(217, 2, 18, 41),
(218, 3, 18, 46),
(219, 4, 18, 8),
(220, 5, 18, 2),
(221, 6, 18, 35),
(222, 7, 18, 18),
(223, 8, 18, 33),
(224, 9, 18, 11),
(225, 10, 18, 7),
(226, 11, 18, 2),
(227, 12, 18, 40),
(228, 13, 18, 40),
(229, 14, 18, 30),
(230, 1, 19, 29),
(231, 2, 19, 7),
(232, 3, 19, 47),
(233, 4, 19, 13),
(234, 6, 19, 25),
(235, 7, 19, 35),
(236, 8, 19, 50),
(237, 9, 19, 44),
(238, 10, 19, 18),
(239, 11, 19, 10),
(240, 12, 19, 45),
(241, 13, 19, 45),
(242, 14, 19, 38),
(243, 1, 20, 7),
(244, 2, 20, 20),
(245, 3, 20, 29),
(246, 4, 20, 32),
(247, 5, 20, 25),
(248, 6, 20, 28),
(249, 7, 20, 13),
(250, 8, 20, 31),
(251, 9, 20, 17),
(252, 10, 20, 43),
(253, 11, 20, 10),
(254, 12, 20, 22),
(255, 13, 20, 30),
(256, 14, 20, 32),
(257, 1, 21, 19),
(258, 2, 21, 50),
(259, 3, 21, 43),
(260, 4, 21, 15),
(261, 5, 21, 43),
(262, 6, 21, 20),
(263, 7, 21, 22),
(264, 8, 21, 49),
(265, 9, 21, 30),
(266, 10, 21, 50),
(267, 11, 21, 11),
(268, 12, 21, 5),
(269, 13, 21, 39),
(270, 14, 21, 30),
(271, 1, 22, 32),
(272, 2, 22, 19),
(273, 3, 22, 47),
(274, 4, 22, 31),
(275, 6, 22, 13),
(276, 7, 22, 20),
(277, 8, 22, 9),
(278, 9, 22, 37),
(279, 10, 22, 5),
(280, 11, 22, 16),
(281, 12, 22, 15),
(282, 13, 22, 25),
(283, 14, 22, 28),
(284, 1, 23, 17),
(285, 2, 23, 48),
(286, 3, 23, 39),
(287, 4, 23, 3),
(288, 5, 23, 45),
(289, 6, 23, 17),
(290, 7, 23, 47),
(291, 8, 23, 36),
(292, 9, 23, 37),
(293, 10, 23, 27),
(294, 11, 23, 22),
(295, 12, 23, 29),
(296, 13, 23, 28),
(297, 14, 23, 2),
(298, 1, 24, 28),
(299, 2, 24, 31),
(300, 3, 24, 20),
(301, 4, 24, 6),
(302, 5, 24, 21),
(303, 6, 24, 37),
(304, 7, 24, 22),
(305, 8, 24, 50),
(306, 9, 24, 30),
(307, 10, 24, 2),
(308, 11, 24, 19),
(309, 12, 24, 40),
(310, 13, 24, 42),
(311, 14, 24, 40),
(312, 1, 25, 22),
(313, 2, 25, 42),
(314, 3, 25, 42),
(315, 4, 25, 32),
(316, 5, 25, 36),
(317, 6, 25, 32),
(318, 7, 25, 2),
(319, 8, 25, 15),
(320, 9, 25, 19),
(321, 10, 25, 48),
(322, 11, 25, 31),
(323, 12, 25, 11),
(324, 13, 25, 13),
(325, 14, 25, 29),
(326, 1, 26, 7),
(327, 2, 26, 47),
(328, 3, 26, 13),
(329, 4, 26, 26),
(330, 5, 26, 38),
(331, 6, 26, 13),
(332, 7, 26, 1),
(333, 8, 26, 16),
(334, 9, 26, 26),
(335, 10, 26, 31),
(336, 11, 26, 26),
(337, 12, 26, 39),
(338, 13, 26, 15),
(339, 14, 26, 7),
(340, 1, 27, 39),
(341, 2, 27, 25),
(342, 3, 27, 6),
(343, 4, 27, 4),
(344, 5, 27, 3),
(345, 6, 27, 1),
(346, 7, 27, 48),
(347, 8, 27, 34),
(348, 9, 27, 27),
(349, 10, 27, 32),
(350, 11, 27, 27),
(351, 12, 27, 41),
(352, 13, 27, 23),
(353, 14, 27, 42),
(354, 1, 28, 38),
(355, 2, 28, 15),
(356, 3, 28, 9),
(357, 4, 28, 1),
(358, 5, 28, 25),
(359, 6, 28, 23),
(360, 7, 28, 41),
(361, 8, 28, 36),
(362, 9, 28, 5),
(363, 10, 28, 16),
(364, 11, 28, 13),
(365, 12, 28, 20),
(366, 13, 28, 9),
(367, 14, 28, 36),
(368, 1, 29, 50),
(369, 2, 29, 44),
(370, 3, 29, 21),
(371, 6, 29, 19),
(372, 7, 29, 31),
(373, 8, 29, 1),
(374, 10, 29, 8),
(375, 11, 29, 37),
(376, 12, 29, 13),
(377, 13, 29, 50),
(378, 14, 29, 13),
(379, 1, 30, 13),
(380, 2, 30, 28),
(381, 3, 30, 49),
(382, 6, 30, 11),
(383, 7, 30, 9),
(384, 8, 30, 10),
(385, 10, 30, 23),
(386, 11, 30, 33),
(387, 12, 30, 48),
(388, 13, 30, 38),
(389, 14, 30, 46),
(390, 1, 31, 14),
(391, 2, 31, 35),
(392, 3, 31, 30),
(393, 6, 31, 44),
(394, 7, 31, 28),
(395, 8, 31, 11),
(396, 10, 31, 17),
(397, 11, 31, 4),
(398, 12, 31, 17),
(399, 13, 31, 23),
(400, 14, 31, 14),
(401, 1, 43, 2),
(402, 2, 43, 14),
(403, 6, 43, 16),
(404, 8, 43, 37),
(405, 10, 43, 36),
(406, 11, 43, 20),
(407, 12, 43, 40),
(408, 13, 43, 39),
(409, 1, 36, 25),
(410, 2, 36, 9),
(411, 6, 36, 19),
(412, 8, 36, 16),
(413, 10, 36, 24),
(414, 11, 36, 23),
(415, 12, 36, 41),
(416, 13, 36, 35),
(417, 1, 42, 50),
(418, 2, 42, 46),
(419, 3, 42, 28),
(420, 4, 42, 1),
(421, 5, 42, 20),
(422, 6, 42, 49),
(423, 7, 42, 33),
(424, 8, 42, 18),
(425, 9, 42, 40),
(426, 10, 42, 47),
(427, 11, 42, 12),
(428, 12, 42, 20),
(429, 13, 42, 13),
(430, 14, 42, 5);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `org_id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(10) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `full_name`, `org_id`, `email`, `password`, `role`) VALUES
(5, 'Admin', 1, 'admin@yahoo.com', '$2y$10$Ed0I3KzuXn7h5Hsdn69tJe7oOybHRXZG4WmVYaID5PZDBfZYrpZuu', 'Admin'),
(6, 'Ops', 1, 'ops@yahoo.com', '$2y$10$Ed0I3KzuXn7h5Hsdn69tJe7oOybHRXZG4WmVYaID5PZDBfZYrpZuu', 'Ops'),
(7, 'SuperAdmin', 1, 'suadmin@yahoo.com', '$2y$10$Ed0I3KzuXn7h5Hsdn69tJe7oOybHRXZG4WmVYaID5PZDBfZYrpZuu', 'SuperAdmin'),
(8, 'Plumber', 1, 'plumber@yahoo.com', '$2y$10$Ed0I3KzuXn7h5Hsdn69tJe7oOybHRXZG4WmVYaID5PZDBfZYrpZuu', 'Plumber'),
(9, 'New admin', 1, 'admin2@yahoo.com', '$2y$10$72BSFBO3zQt/FWRs9NZ0QeC8ei.blLve/qhXsA2NylFoRDvdoZgwK', 'Admin'),
(10, 'Admin 3', 1, 'admin3@yahoo.com', '$2y$10$tmha.TDu0yEZcKWJQMu.c.r8YonZdLFAAYoweFCbtvd2YtORlBfWa', 'Admin'),
(11, 'admin 4', 0, 'admin4@yahoo.com', '$2y$10$xJ3PfQpuEWCgkSO.2OgsCewz3/cvr2PC71fkNEU4ilmaFNFOehB/m', 'Admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `materials`
--
ALTER TABLE `materials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organisations`
--
ALTER TABLE `organisations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quotes`
--
ALTER TABLE `quotes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quote_materials`
--
ALTER TABLE `quote_materials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `surfaceareas_materials_dynamic`
--
ALTER TABLE `surfaceareas_materials_dynamic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `surface_areas`
--
ALTER TABLE `surface_areas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `surface_areas_materials`
--
ALTER TABLE `surface_areas_materials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `materials`
--
ALTER TABLE `materials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `organisations`
--
ALTER TABLE `organisations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `quotes`
--
ALTER TABLE `quotes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `quote_materials`
--
ALTER TABLE `quote_materials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=148;
--
-- AUTO_INCREMENT for table `surfaceareas_materials_dynamic`
--
ALTER TABLE `surfaceareas_materials_dynamic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `surface_areas`
--
ALTER TABLE `surface_areas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `surface_areas_materials`
--
ALTER TABLE `surface_areas_materials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=431;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
