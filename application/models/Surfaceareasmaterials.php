<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once 'BaseModel.php';

class SurfaceAreasMaterials extends BaseModel
{
    public $id;
    public $surface_id;
    public $material_id;
    public $quantity;

    public function __construct()
    {
        parent::__construct();

        $this->table = 'surface_areas_materials';
    }

    public function generateOrganisationSurfaceAreaMaterials($orgId)
    {
        $materialAndQty = $this->db->select('A.id, B.quantity')
                        ->from('materials A')
                        ->join('template_surface_area_materials  B', 'A.name = B.name AND A.org_id = ' . $orgId, 'inner')
                        ->get();
        $surface = $this->db->select('id')
                        ->from('surface_areas')
                        ->where('org_id = ' . $orgId)
                        ->get();

        $surfaceResult = $surface->result_array();
        $materialAndQtyResult = $materialAndQty->result_array();
        $data = [];

        if ($surface->num_rows() > 0 && $materialAndQty->num_rows() > 0) {
            foreach ($surfaceResult as $key => $value) {
                foreach ($materialAndQtyResult as $keyx => $valuex) {
                    $data[] = [
                        'surface_id'    => $value['id'],
                        'material_id'   => $valuex['id'],
                        'quantity'      => $valuex['quantity'],
                    ];
                }
            }
            return $this->db->insert_batch($this->table, $data);
        } else {
            return false;
        }
    }
}
