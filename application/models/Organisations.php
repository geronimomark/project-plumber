<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once 'BaseModel.php';

class Organisations extends BaseModel
{

    public $id;
    public $title;
    public $logo;
    public $address;
    public $contact_number;

    public function __construct()
    {
        parent::__construct();

        $this->table = 'organisations';
    }

    public function getOrgsWithContact()
    {
        return $this->db->select('A.*, B.full_name')
                 ->from($this->table . ' A')
                 ->join('users B', 'A.id = B.org_id AND B.role = "Admin"')
                 ->group_by('A.id')
                 ->get();
    }
}
