<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once 'BaseModel.php';

class Users extends BaseModel
{

    public $id;
    public $full_name;
    public $org_id;
    public $email;
    public $password;
    public $type;

    public function __construct()
    {
        parent::__construct();

        $this->table = 'users';
    }
}
