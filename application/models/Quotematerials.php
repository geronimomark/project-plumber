<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once 'BaseModel.php';

class QuoteMaterials extends BaseModel
{

    public $id;
    public $quote_id;
    public $material_name;
    public $unit_price;
    public $quantity;
    public $total_price;

    public function __construct()
    {
        parent::__construct();

        $this->table = 'quote_materials';
    }

    public function getCurrentQuotes($where)
    {
        return $this->db->select('SUM(A.`total_price`) as `total`, A.quote_id, B.ref_id, B.address, B.script, C.id, C.email, C.full_name')
                ->from($this->table . ' A')
                ->join('quotes B', 'B.id = A.quote_id')
                ->join('users C', 'B.created_by = C.id')
                ->where($where)
                ->group_by('quote_id')
                ->get();
    }

    public function getQuoteMaterialsJoinQuotes($where)
    {
        return $this->db->select('A.*, B.won, B.sub_cost, B.margin, B.extra_cost, B.script, B.ref_id, B.address')
                ->from($this->table . ' A')
                ->join('quotes B', 'B.id = A.quote_id')
                ->where($where)
                ->order_by('A.`type`')
                ->get();
    }
}
