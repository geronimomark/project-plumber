<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once 'BaseModel.php';

class Surfacetodo extends BaseModel
{

    public $id;
    public $surface_id;
    public $todo;

    public function __construct()
    {
        parent::__construct();

        $this->table = 'surface_todo';
    }

    public function getPlumberTodo($where)
    {
        return $this->db->select('A.*, B.name')
                        ->from($this->table . ' A')
                        ->join('surface_areas B', 'A.surface_id = B.id')
                        ->where($where)
                        ->get();
    }
}
