<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once 'BaseModel.php';

class Materials extends BaseModel
{
    public $id;
    public $name;
    public $unit_price;
    public $type;
    public $org_id;
    public $key;

    public function __construct()
    {
        parent::__construct();

        $this->table = 'materials';
    }

    public function getStaticMaterials($surfaceId)
    {
        return $this->db->select('materials.*, surface_areas_materials.surface_id, surface_areas_materials.quantity, surface_areas.name as `surfaceName`')
                 ->from($this->table)
                 ->join('surface_areas_materials', 'materials.id = surface_areas_materials.material_id')
                 ->join('surface_areas', 'surface_areas.id = surface_areas_materials.surface_id')
                 ->where(['surface_areas_materials.surface_id' => $surfaceId])
                 ->get();
    }

    public function getAllStaticMaterials($where)
    {
        return $this->db->select('materials.*, surface_areas_materials.id as `staticId`, surface_areas_materials.surface_id, surface_areas_materials.quantity, surface_areas.name as `surfaceName`')
                 ->from($this->table)
                 ->join('surface_areas_materials', 'materials.id = surface_areas_materials.material_id')
                 ->join('surface_areas', 'surface_areas.id = surface_areas_materials.surface_id')
                 ->where($where)
                 ->order_by('surface_areas.name')
                 ->get();
    }

    public function generateOrganisationMaterials($orgId)
    {
        $materials = $this->db->get('template');
        $resultArray = $materials->result_array();
        if ($materials->num_rows() > 0) {
            foreach ($resultArray as $key => $value) {
                $resultArray[$key]['org_id'] = $orgId;
            }
            return $this->db->insert_batch($this->table, $resultArray);
        } else {
            return false;
        }
    }
}
