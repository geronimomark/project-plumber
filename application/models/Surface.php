<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once 'BaseModel.php';

class Surface extends BaseModel
{

    public $id;
    public $name;
    public $script;

    public function __construct()
    {
        parent::__construct();

        $this->table = 'surface_areas';
    }

    public function joinTodo($where)
    {
        return $this->db->select('A.name, A.id as `surface`, B.*')
                        ->from($this->table . ' A')
                        ->join('surface_todo B', 'A.id = B.surface_id', 'left')
                        ->where($where)
                        ->get();
    }

    public function generateOrganisationSurfaceArea($orgId)
    {
        $surface = $this->db->get('template_surface_area');
        $resultArray = $surface->result_array();
        if ($surface->num_rows() > 0) {
            foreach ($resultArray as $key => $value) {
                $resultArray[$key]['org_id'] = $orgId;
            }
            return $this->db->insert_batch($this->table, $resultArray);
        } else {
            return false;
        }
    }

    public function getSurfaceAreaNameById($id)
    {
        return $this->db->select('name')
                        ->from($this->table)                        
                        ->where(array('id' => $id))
                        ->get()->result_array();
                 
    }
}
