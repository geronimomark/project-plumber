<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once 'BaseModel.php';

class Quotes extends BaseModel
{
    public $id;
    public $surface_id;
    public $org_id;
    public $won;
    public $marked_for_deletion;
    public $ref_id;
    public $address;

    public function __construct()
    {
        parent::__construct();

        $this->table = 'quotes';
    }
}
