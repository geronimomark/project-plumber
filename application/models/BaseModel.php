<?php
defined('BASEPATH') or exit('No direct script access allowed');

class BaseModel extends CI_Model
{

    public $table;

    public function __construct()
    {
        parent::__construct();
    }

    public function get()
    {
        return $this->db->get($this->table);
    }

    public function getWhere($where)
    {
        return $this->db->where($where)->get($this->table);
    }

    public function post($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function insertAndGetId($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function insertBatch($data)
    {
        return $this->db->insert_batch($this->table, $data);
    }

    public function put($data, $where)
    {
        return $this->db->update($this->table, $data, $where);
    }

    public function delete($where)
    {
        return $this->db->delete($this->table, $where);
    }
}
