<script>
    $(function() {
        $('#table1').DataTable();

        $("button.delete").on({
            click: function() {
                var r = confirm("Are you sure you want to delete this user?");
                if (r == true) {
                    $(this).parent().find('form button').click();
                }
            }
        });
    });
</script>