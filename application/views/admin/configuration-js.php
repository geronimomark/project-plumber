<script>
    var materialName = '';
    var materialPrice = '';
    var materialId = '';
    var surfaceName = '';
    var materialName = '';
    var materialQty = '';
    var staticId = '';
    var surfaceName = '';
    var script = '';
    var surfaceId = '';
    $(function() {
        $('#table1').DataTable();
        $('#table2').DataTable();
        $('#table3').DataTable();

        $("div.hidden").addClass('modalDiv').removeClass('hidden');

        // $("button[name='edit']").on({
        $(document).on('click', 'button[name="edit"]', function(e) {

            // click: function () {

                $('div.modalDiv .btn-primary').attr('name', 'edit');

                materialName  = $(this).parent().parent().find('td[name="materialName"]').text(),
                onehundredmm  = $(this).parent().parent().find('td[name="onehundredmm"]').text().replace("$", "");
                onefiftymm    = $(this).parent().parent().find('td[name="onefiftymm"]').text().replace("$", "");
                twotwentyfivemm = $(this).parent().parent().find('td[name="twotwentyfivemm"]').text().replace("$", "");
                threehundredmm  = $(this).parent().parent().find('td[name="threehundredmm"]').text().replace("$", "");
                materialId = $(this).parent().parent().find('td input[name="materialId"]').val();

                $("#modalTrigger").click();
                $("#myModalLabel").html('Update Material');
                $("#formModal").remove();
                $("#guide").remove();
                $("div[name='modalMessage']").remove();

                $("#loader").hide().after(' <div id="formModal"><div id="modalMessage" class="alert" role="alert">Fill up the form to update</div><input type="text" name="materialNameUpdate" class="form-control" maxlength="100"/><input type="text" name="onehundredmmUpdate" class="form-control numberWithDecimal" maxlength="11"/><input type="text" name="onefiftymmUpdate" class="form-control numberWithDecimal" maxlength="11"/><input type="text" name="twotwentyfivemmUpdate" class="form-control numberWithDecimal" maxlength="11"/><input type="text" name="threehundredmmUpdate" class="form-control numberWithDecimal" maxlength="11"/></div>');

                $("input[name='materialNameUpdate']").val(materialName);
                $("input[name='onehundredmmUpdate']").val(onehundredmm);
                $("input[name='onefiftymmUpdate']").val(onefiftymm);
                $("input[name='twotwentyfivemmUpdate']").val(twotwentyfivemm);
                $("input[name='threehundredmmUpdate']").val(threehundredmm);

            // }

        // });
        });

        // $("button[name='editStatic']").on({
        $(document).on('click', 'button[name="editStatic"]', function(e) {

            // click: function () {

                $('div.modalDiv .btn-primary').attr('name', 'editStatic');

                surfaceName   = $(this).parent().parent().find('td[name="surfaceName"]').text(),
                materialName  = $(this).parent().parent().find('td[name="materialName"]').text(),
                materialQty   = $(this).parent().parent().find('td[name="materialQuantity"]').text();
                staticId      = $(this).parent().parent().find('td input[name="staticId"]').val();

                $("#modalTrigger").click();
                $("#myModalLabel").html('Update Quantity');
                $("#formModal").remove();
                $("#guide").remove();
                $("div[name='modalMessage']").remove();

                $("#loader").hide().after(' <div id="formModal"><div id="modalMessage" class="alert" role="alert">Fill up the form to update</div><input type="text" name="surfaceNameUpdate" class="form-control" disabled/><input type="text" name="materialNameUpdate" class="form-control" disabled/><input type="text" name="materialQtyUpdate" class="form-control numberOnly" maxlength="11"/></div>');

                $("input[name='surfaceNameUpdate']").val(surfaceName);
                $("input[name='materialNameUpdate']").val(materialName);
                $("input[name='materialQtyUpdate']").val(materialQty);
            // }

        });

        // $("button[name='editSurface']").on({
        $(document).on('click', 'button[name="editSurface"]', function(e) {

            // click: function () {

                $('div.modalDiv .btn-primary').attr('name', 'editSurface');

                surfaceName   = $(this).parent().parent().find('td[name="surfaceName"]').text(),
                script        = $(this).parent().parent().find('td[name="script"] textarea').val(),
                surfaceId     = $(this).parent().parent().find('td input[name="surfaceId"]').val();

                $("#modalTrigger").click();
                $("#myModalLabel").html('Update Surface');
                $("#formModal").remove();
                $("#guide").remove();
                $("div[name='modalMessage']").remove();

                var modalHtml = '<table id="guide" styling="font-size: 12px;" class="table table-bordered">\
                    <tr><td>Reference:</td><td>|refNumber|</td></tr>\
                    <tr><td>Address:</td><td>|address|</td></tr>\
                    <tr><td>Suburb:</td><td>|suburb|</td></tr>\
                    <tr><td>Sewer or stormwater:</td><td>|sewerOrStorm|</td>\
                    <tr><td>Cause of problem:</td><td>|problem|</td></tr>\
                    <tr><td>Length of excavation:</td><td>|lengthOfEx|</td></tr>\
                    <tr><td>Depth of excavation:</td><td>|depthOfEx|</td></tr>\
                    <tr><td>Pipe Diameter:</td><td>|pipeDiameter|</td></tr>\
                    <tr><td>Existing Pipe Material:</td><td>|existingPipe|</td></tr>\
                    <tr><td>Surface Area:</td><td>|surfaceArea|</td></tr>\
                    <tr><td>Location:</td><td>|location|</td></tr>\
                    <tr><td>Days/Hours:</td><td>|daysOrHours|</td></tr>\
                    <tr><td>Number of Days/Hours:</td><td>|numberOfDaysOrHours|</td></tr>\
                    <tr><td>Machine Access:</td><td>|machineAccess|</td></tr>\
                    <tr><td>Days of machine access</td><td>|machineDays|</td></tr>\
                    <tr><td>Council Fees</td><td>|councilFees|</td></tr>\
                    <tr><td>Road Plates</td><td>|roadPlates|</td></tr>\
                    </table>\
                    </div><div id="formModal">\
                    <div id="modalMessage" class="alert" role="alert">Fill up the form to update</div>\
                    <input type="text" name="surfaceNameUpdate" class="form-control" maxlength="100"/>\
                    <textarea name="scriptUpdate" class="form-control" maxlength="5000" rows="10"></textarea>\
                    </div>';
                $("#loader").hide().after(modalHtml);

                $("input[name='surfaceNameUpdate']").val(surfaceName);
                $("textarea[name='scriptUpdate']").text(script);
            // }

        });

        $('div.modalDiv .btn-primary').on({

            click: function() {
                if ($(this).attr('name') == 'edit') {
                    // Edit Material Block
                    var materialNameUpdate = $("input[name='materialNameUpdate']").val().trim();
                    var onehundredmmUpdate = $("input[name='onehundredmmUpdate']").val().trim();
                    var onefiftymmUpdate = $("input[name='onefiftymmUpdate']").val().trim();
                    var twotwentyfivemmUpdate = $("input[name='twotwentyfivemmUpdate']").val().trim();
                    var threehundredmmUpdate = $("input[name='threehundredmmUpdate']").val().trim();

                    if ((materialName != materialNameUpdate && materialNameUpdate != '') || (onehundredmm != onehundredmmUpdate && onehundredmmUpdate != '') || (onefiftymm != onefiftymmUpdate && onefiftymmUpdate != '') || (twotwentyfivemm != twotwentyfivemmUpdate && twotwentyfivemmUpdate != '') || (threehundredmm != threehundredmmUpdate && threehundredmmUpdate != '')) {
                        var row = $("input[type='hidden'][name='materialId'][value='"+materialId+"']").parent().parent();
                        var jqxhr = $.post( "<?=base_url('ajax/updateMaterialConfig')?>", { name : materialNameUpdate, onehundred : onehundredmmUpdate, onefifty : onefiftymmUpdate, twotwentyfive : twotwentyfivemmUpdate, threehundred : threehundredmmUpdate, id : materialId })
                            .done(function(data) {
                                if (data.success) {
                                    setTimeout(function () {
                                        row.find('td[name="materialName"]').html(materialNameUpdate);
                                        row.find('td[name="onehundredmm"]').html("$"+parseFloat(onehundredmmUpdate).toFixed(2));
                                        row.find('td[name="onefiftymm"]').html("$"+parseFloat(onefiftymmUpdate).toFixed(2));
                                        row.find('td[name="twotwentyfivemm"]').html("$"+parseFloat(twotwentyfivemmUpdate).toFixed(2));
                                        row.find('td[name="threehundredmm"]').html("$"+parseFloat(threehundredmmUpdate).toFixed(2));
                                        $("#table1").before(' <div name="modalMessage" class="alert alert-success" role="alert"><strong>Success!</strong> Material Updated</div>');
                                        },
                                        1000
                                    );
                                } else {
                                    errorMessage(undefined, "#table1");
                                }
                            })
                            .fail(function(data) {
                                errorMessage(undefined, "#table1");
                            })
                            .always(function(data) {
                                setTimeout(function () {
                                        $("div[name='modalMessage']").remove();
                                        $("#modalClose").click();
                                    },
                                    3000
                                );
                            });
                    } else if (materialName == materialNameUpdate && onehundredmm == onehundredmmUpdate && onefiftymm == onefiftymmUpdate && twotwentyfivemm == twotwentyfivemmUpdate && threehundredmm == threehundredmmUpdate) {
                        setTimeout(function () {
                        $("#modalClose").click();
                        }, 1);
                    } else {
                        errorMessage('Invalid Input!');
                        setTimeout(function () {
                        $("#modalClose").click();
                        }, 2000);
                    }

                } else if ($(this).attr('name') == 'editStatic') {
                    // Edit Static Block
                    var materialQtyUpdate = $("input[name='materialQtyUpdate']").val();

                    if (materialQtyUpdate != materialQty && parseInt(materialQtyUpdate) >= 0) {
                        var row = $("input[type='hidden'][name='staticId'][value='"+staticId+"']").parent().parent();
                        var jqxhr = $.post( "<?=base_url('ajax/updateStaticQtyConfig')?>", { qty : materialQtyUpdate, id : staticId })
                            .done(function(data) {
                                if (data.success) {
                                    setTimeout(function () {
                                        row.find('td[name="materialQuantity"]').html(materialQtyUpdate);
                                        $("#table2").before(' <div name="modalMessage" class="alert alert-success" role="alert"><strong>Success!</strong> Quantity Updated</div>');
                                        },
                                        1000
                                    );
                                } else {
                                    errorMessage(undefined, "#table2");
                                }
                            })
                            .fail(function(data) {
                                errorMessage(undefined, "#table2");
                            })
                            .always(function(data) {
                                setTimeout(function () {
                                        $("div[name='modalMessage']").remove();
                                        $("#modalClose").click();
                                    },
                                    3000
                                );
                            });
                    } else {
                        errorMessage('Invalid Input!');
                        setTimeout(function () {
                        $("#modalClose").click();
                        }, 2000);
                    }

                } else if ($(this).attr('name') == 'editSurface') {
                    // Edit Surface Block
                    var surfaceNameUpdate = $("input[name='surfaceNameUpdate']").val();
                    var scriptUpdate = $("textarea[name='scriptUpdate']").val();
                    
                    if ((surfaceName != surfaceNameUpdate && surfaceNameUpdate != '') || (script != scriptUpdate && scriptUpdate != '')) {
                        var row = $("input[type='hidden'][name='surfaceId'][value='"+surfaceId+"']").parent().parent();
                        var jqxhr = $.post( "<?=base_url('ajax/updateSurfaceConfig')?>", { name : surfaceNameUpdate, script : scriptUpdate, id : surfaceId })
                            .done(function(data) {
                                if (data.success) {
                                    setTimeout(function () {
                                        row.find('td[name="surfaceName"]').html(surfaceNameUpdate);
                                        row.find('td[name="script"] textarea').val(scriptUpdate);
                                        $("#table3").before(' <div name="modalMessage" class="alert alert-success" role="alert"><strong>Success!</strong> Surface Updated</div>');
                                        },
                                        1000
                                    );
                                } else {
                                    errorMessage(undefined, "#table3");
                                }
                            })
                            .fail(function(data) {
                                errorMessage(undefined, "#table3");
                            })
                            .always(function(data) {
                                setTimeout(function () {
                                        $("div[name='modalMessage']").remove();
                                        $("#modalClose").click();
                                    },
                                    3000
                                );
                            });
                    } else if (surfaceName == surfaceNameUpdate && script == scriptUpdate) {
                        setTimeout(function () {
                        $("#modalClose").click();
                        }, 1);
                    } else {
                        errorMessage('Invalid Input!');
                        setTimeout(function () {
                        $("#modalClose").click();
                        }, 2000);
                    }
                }
            }

        });
    });
</script>