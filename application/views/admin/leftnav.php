<div class="col-sm-3 col-md-2 sidebar collapse" id="leftnavbar">
    <ul class="nav nav-sidebar">
        <li class="<?=($leftnav=='index')?'active':''?>"><a href="<?=base_url('admin/index')?>">Quotes</a></li>
        <li class="<?=($leftnav=='configuration')?'active':''?>"><a href="<?=base_url('admin/configuration')?>">Configuration</a></li>
        <li class="<?=($leftnav=='users')?'active':''?>"><a href="<?=base_url('admin/users')?>">Users</a></li>
        <li class="<?=($leftnav=='newQuote')?'active':''?>"><a href="<?=base_url('admin/newJob')?>">New Quote</a></li>
        <li class="<?=($leftnav=='archived')?'active':''?>"><a href="<?=base_url('admin/archived')?>">Won Quotes</a></li>
        <?php if ($this->session->origOrgId) :?>
        <li><a href="<?=base_url('admin/logoff')?>">Back to Super Admin Module</a></li>
        <?php endif ?>
        <li><a href="<?=base_url('login/logoff')?>">Logout</a></li>        
    </ul>
</div>