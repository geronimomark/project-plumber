    <?php $this->load->view('common/navbar', $data)?>

    <div class="container-fluid">
        <div class="row">
            <?php $this->load->view('admin/leftnav', $data)?>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                <?php $this->load->view('common/successerror')?>
                <?php $this->load->view('common/viewquote')?>
            </div>
        </div><!-- /row -->
    </div><!-- /container -->