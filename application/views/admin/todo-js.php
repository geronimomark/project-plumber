<script>
    $(function() {
        $("#add").click(function() {
            var todonew = $("tr td input[name='todonew[]']:last"),
            newTodo = '<tr><td><input name="todonew[]" type="text" class="form-control" placeholder="Todo" value="" required></td><td>&nbsp;<a class="btn btn-danger btn-sm rmrf">-</a></td></tr>';
            if (todonew.length == 1) {
                if (todonew.val().trim() != '') {
                    todonew.parent().parent().after(newTodo);
                }
            } else {
                if ($("tr td input[name='todo[]']:last").val().trim() != '') {
                    $("tr td input[name='todo[]']:last").parent().parent().after(newTodo);
                }
            }
        });

        $(document).on('click', 'a.rmrf', function(e) {
            $(this).parent().parent().remove();
        });

        $("a.oldrmrf").click(function() {
            if ($("input[name='todo[]']").length > 1 || $("input[name='todonew[]']").length > 0) {
                window.location.replace("<?=base_url('admin/deleteTodo')?>/" + $(this).attr('data') + "/" + $("input[name='id']").val());
            }
        });
    });
</script>