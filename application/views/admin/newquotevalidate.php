    <?php $this->load->view('common/navbar', $data)?>

    <div class="container-fluid">
        <div class="row">
            <?php $this->load->view($module.'/leftnav', $data)?>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                <h1>Create New Quote</h1>
                <form class="form-signin" method="POST" action="<?=base_url()?><?=$module?>/postQuote">
                    <?php foreach ($this->input->post() as $key => $value) :?>
                        <?php if (is_array($value)) :?>
                            <?php foreach ($value as $key2 => $value2) :?>
                                <input type="hidden" name="<?=$key?>[]" value="<?=$value2?>"/>
                            <?php endforeach ?>
                        <?php else :?>
                            <input type="hidden" name="<?=$key?>" value="<?=$value?>"/>
                        <?php endif ?>
                    <?php endforeach ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel panel-success">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Job Ref# <?=$this->input->post('refNumber')?></h3>
                                    <h3 class="panel-title">Tech: <?=$this->session->fullName?></h3>
                                </div>
                                <div class="panel-body">
                                    <?=$script?>
                                    <input type="hidden" name="script" value="<?=$script?>"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a class="btn btn-lg btn-primary btn-block" href="<?=base_url()?><?=$module?>/newJob">Back</a>
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
                </form>
            </div>
        </div>

    </div>