    <?php $this->load->view('common/navbar', $data)?>

    <div class="container-fluid">
        <div class="row">
            <?php $this->load->view($module.'/leftnav', $data)?>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                <h1>Configuration</h1>
                <table id="table1" class="table table-striped table-striped" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th colspan="3">Materials</th>
                        </tr>
                        <tr>
                        <th>Name</th>
                        <th>100mm</th>
                        <th>150mm</th>
                        <th>225mm</th>
                        <th>300mm</th>
                        <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($materials->result_object() as $key => $value) :?>
                    <tr>
                        <td name="materialName"><?=$value->name?></td>
                        <td name="onehundredmm">$<?=$value->onehundredmm?></td>
                        <td name="onefiftymm">$<?=$value->onefiftymm?></td>
                        <td name="twotwentyfivemm">$<?=$value->twotwentyfivemm?></td>
                        <td name="threehundredmm">$<?=$value->threehundredmm?></td>
                        <td><input type="hidden" name="materialId" value="<?=$value->id?>"/><button type="button" name="edit" class="btn btn-primary btn-sm">Edit</button></td>
                    </tr>
                    <?php endforeach ?>
                    </tbody>
                </table>
                <hr />
                <table id="table2" class="table table-striped table-striped" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th colspan="4">Static Materials</th>
                        </tr>
                        <tr>
                        <th>Surface Area</th>
                        <th>Material Name</th>
                        <th>Quantity</th>
                        <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($static->result_object() as $key => $value) :?>
                    <tr>
                        <td name="surfaceName"><?=$value->surfaceName?></td>
                        <td name="materialName"><?=$value->name?></td>
                        <td name="materialQuantity"><?=$value->quantity?></td>
                        <td><input type="hidden" name="staticId" value="<?=$value->staticId?>"/><button type="button" name="editStatic" class="btn btn-primary btn-sm">Edit</button></td>
                    </tr>
                    <?php endforeach ?>
                    </tbody>
                </table>
                <hr />
                <table id="table3" class="table table-striped table-striped" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th colspan="3">Surface Areas</th>
                        </tr>
                        <tr>
                        <th>Name</th>
                        <th>Script</th>
                        <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($surface->result_object() as $key => $value) :?>
                    <tr>
                        <td name="surfaceName"><?=$value->name?></td>
                        <td name="script"><textarea class="form-control" maxlength="255" disabled><?=$value->script?></textarea></td>
                        <td>
                            <input type="hidden" name="surfaceId" value="<?=$value->id?>"/>
                            <button type="button" name="editSurface" class="btn btn-primary btn-sm">Edit</button>
                            <a class="btn btn-primary btn-sm" href="<?=base_url($module.'/todo/'.$value->id)?>">Manage To-do List</a>
                        </td>
                    </tr>
                    <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div><!-- /row -->

        <div class="row">
            <div class="col-sm-3 main pull-right">
                <button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
            </div>
        </div><!-- /row -->

    </div><!-- /container -->
    <?php $this->load->view('common/modal') ?>