    <?php $this->load->view('common/navbar', $data)?>

    <div class="container-fluid">
        <div class="row">
            <?php $this->load->view($module.'/leftnav', $data)?>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                <form class="form-signin col-sm-6 col-sm-offset-3" method="POST" action="<?=base_url()?><?=$module?>/addTodo">
                <?php $this->load->view('common/successerror') ?>
                <h3>Manage To-do</h3>
                <?php if ($todo->num_rows() > 0) :?>
                    <?php $surfaceName   = $todo->result_object()[0]->name ?>
                    <?php $surfaceId = $todo->result_object()[0]->surface ?>
                <?php endif ?>
                <h4>Surface Area: <?=$surfaceName?></h4>
                <input type="hidden" name="id" value="<?=$surfaceId?>"/>
                <label>
                    Add / Remove Todo
                    <button type="button" id="add" class="btn btn-primary btn-sm">+</button>
                </label>
                <br />
                <br />
                <table>
                <?php foreach($todo->result_object() as $key => $value) :?>
                    <tr>
                        <td>
                            <input name="todo[]" type="text" class="form-control" placeholder="Todo" value="<?=$value->todo?>" required>
                        </td>
                        <td>
                            &nbsp;<a class="btn btn-danger btn-sm oldrmrf" data="<?=$value->id?>">-</a>
                        </td>
                    </tr>
                <?php endforeach ?>
                </table>
                <br />
                <a class="btn btn-lg btn-warning btn-block" href="<?=base_url('admin/configuration')?>">Back</a>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
                </form>
            </div>
        </div><!-- /row -->

    </div><!-- /container -->

    <?php $this->load->view('common/modal') ?>