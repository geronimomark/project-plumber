    <?php $this->load->view('common/navbar', $data)?>

    <div class="container-fluid newQuote">
        <div class="row">
            <?php $this->load->view($module.'/leftnav', $data)?>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                <h1>Create New Quote</h1>
                <form id="form" class="form-signin newquote" method="POST" action="<?=base_url()?><?=$module?>/newJobValidate" enctype="multipart/form-data">
                    <?php $this->load->view('common/successerror')?>
                    <?php if (validation_errors()) :?>
                    <div class="alert alert-danger" role="alert">
                        <strong>Error!</strong><?=validation_errors()?>
                    </div>
                    <?php endif ?>
                <input type="hidden" name="redirectError" value="<?=$module?>/newJob" />
                <input type="hidden" name="redirectSuccess" value="<?=$module?>/newJob" />
                <input type="text" name="refNumber" value="<?=set_value('refNumber')?>" class="form-control" placeholder="Reference #" required maxlength="50"/>
                <textarea name="address" class="form-control" placeholder="Address" required maxlength="255"><?=set_value('address')?></textarea>
                <input type="text" name="suburb" value="<?=set_value('suburb')?>" class="form-control" placeholder="Suburb" required maxlength="50"/>
                <select name="sewerOrStorm" class="form-control" required>
                    <option value="">Sewer or Stormwater</option>
                    <option value="Sewer">Sewer</option>
                    <option value="Stormwater">Stormwater</option>
                </select>
                <select name="problem" class="form-control" required>
                    <option value="">Cause of Problem</option>
                    <option value="tree roots">Tree Roots</option>
                    <option value="broken pipework">Broken Pipework</option>
                    <option value="misaligned pipework">Misaligned Pipework</option>
                    <option value="tree roots and broken pipework">Tree Roots and Broken Pipework</option>
                </select>
                <select name="lengthOfEx" class="form-control" required>
                    <option value="">Length of excavation in meters</option>
                    <?php for ($i=1; $i<=12; $i++) :?>
                        <option value="<?=($i * 1)?>"><?=($i * 1)?></option>
                    <?php endfor ?>
                </select>
                <select name="depthOfEx" class="form-control" required>
                    <option value="">Depth of excavation in mm</option>
                    <?php for ($i=1; $i<=30; $i++) :?>
                        <option value="<?=($i * 100)?>"><?=($i * 100)?></option>
                    <?php endfor ?>
                </select>
                <select name="pipeDiameter" class="form-control" required>
                    <option value="">Pipe diameter</option>
                    <option value="onehundredmm">100mm</option>
                    <option value="onefiftymm">150mm</option>
                    <option value="twotwentyfivemm">225mm</option>
                    <option value="threehundredmm">300mm</option>
                </select>
                <select name="existingPipe" class="form-control" required>
                    <option value="">Existing pipe material</option>
                    <option value="Earthenware">Earthenware</option>
                    <option value="PVC">PVC</option>
                </select>
                <input type="text" name="steelReo" value="<?=set_value('steelReo')?>" maxlength="5" class="form-control hidden numberOnly" placeholder="Size of steel/reo bars" required/>
                <input type="text" name="gravel" value="<?=set_value('gravel')?>" maxlength="5" class="form-control hidden numberOnly" placeholder="Size of gravel" required/>
                <input type="text" name="topSoil" value="<?=set_value('topSoil')?>" maxlength="5" class="form-control hidden numberOnly" placeholder="Size of top soil" required/>
                <input type="text" name="turf" value="<?=set_value('turf')?>" maxlength="5" class="form-control hidden numberOnly" placeholder="Size of turf" required/>
                <input type="text" name="rubbish" value="<?=set_value('rubbish')?>" maxlength="5" class="form-control hidden numberOnly" placeholder="Size of rubbish" required/>
                <input type="text" name="concrete" value="<?=set_value('concrete')?>" maxlength="5" class="form-control hidden numberOnly" placeholder="Size of concrete" required/>
                <input type="text" name="pebbles" value="<?=set_value('pebbles')?>" maxlength="5" class="form-control hidden numberOnly" placeholder="Size of pebbles" required/>
                <input type="text" name="bitumen" value="<?=set_value('bitumen')?>" maxlength="5" class="form-control hidden numberOnly" placeholder="Size of bitumen" required/>
                <select name="surfaceArea" class="form-control" required>
                    <option value="">Surface Area</option>
                    <?php if ($surface->num_rows() > 0) :?>
                        <?php foreach ($surface->result_object() as $value) :?>
                            <option value="<?=$value->id?>"><?=$value->name?></option>
                        <?php endforeach ?>
                    <?php endif ?>
                </select>
                <select name="location" class="form-control" required>
                    <option value="">Location</option>
                    <option value="front of property">front of property</option>
                    <option value="rear of property">rear of property</option>
                    <option value="left of property">left of property</option>
                    <option value="right of property">right of property</option>
                </select>
                <select name="daysOrHours" class="form-control" required>
                    <option value="">Days / Hours</option>
                    <option value="day_rate">Days</option>
                    <option value="hourly_rate">Hours</option>
                </select>
                <input type="text" name="numberOfDaysOrHours" value="<?=set_value('numberOfDaysOrHours')?>" maxlength="8" class="form-control numberWithDecimal" placeholder="Number of Days / Hours" required/>
                <select name="machineAccess" class="form-control" required>
                    <option value="">Machine Access/Excavator</option>
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                <input type="text" name="machineDays" value="<?=set_value('machineDays')?>" maxlength="8" class="form-control numberWithDecimal" placeholder="Number of days of machine access"/>
                <br />
                <label class="">Special Plant</label>
                <br /><input type="checkbox" value="excludeCouncilFees" name="excludeCouncilFees"/> Excludes Council Fees
                <input type="text" name="councilFees" value="<?=set_value('councilFees')?>" maxlength="10" class="form-control numberWithDecimal" placeholder="Council Fees"/>
                <select name="roadPlates" class="form-control" required>
                    <option value="0">Road Plates</option>
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                </select>
                <?php foreach ($specialPlant->result_object() as $value) :?>
                    <?php if ($value->name != "Steel Float") :?>
                        <input type="checkbox" value="<?=$value->id.'|'.$value->name.'|'.$value->onehundredmm.'|'.$value->onefiftymm.'|'.$value->twotwentyfivemm.'|'.$value->threehundredmm?>" 
                        <?php if (is_array($this->input->post('specialPlants')) && in_array($value->id.'|'.$value->name.'|'.$value->onehundredmm.'|'.$value->onefiftymm.'|'.$value->twotwentyfivemm.'|'.$value->threehundredmm, $this->input->post('specialPlants'))) echo 'checked'?> name="specialPlants[]" /> <?=$value->name?><br />
                    <?php endif ?>
                <?php endforeach ?>

                <!-- <a id="uploader" class="btn btn-lg btn-primary btn-block" href="">Upload image</a> -->
                <br />
                <textarea name="comments" class="form-control" placeholder="Comment" required maxlength="255"><?=set_value('comments')?></textarea>
                <br />
                <button class="btn btn-lg btn-primary btn-block postBtn hidden" type="submit">Continue</button>
                </form>

                <!-- <hr /> -->

    <!-- The file upload form used as target for the file upload widget -->
    <form id="fileupload" action="<?=base_url().$module?>/uploader" method="POST" enctype="multipart/form-data">
        <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
        <div class="row fileupload-buttonbar">
            <div class="col-lg-7">
                <!-- The fileinput-button span is used to style the file input field as button -->
                <span class="btn btn-success fileinput-button">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span>Add a photo</span>
                    <input type="file" name="files[]" multiple>
                </span>
                <button type="submit" class="btn btn-primary start">
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start upload</span>
                </button>
                <button type="reset" class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel upload</span>
                </button>
                <button type="button" class="btn btn-danger delete">
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Delete</span>
                </button>
                <input type="checkbox" class="toggle">
                <!-- The global file processing state -->
                <span class="fileupload-process"></span>
            </div>
            <!-- The global progress state -->
            <div class="col-lg-5 fileupload-progress fade">
                <!-- The global progress bar -->
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                </div>
                <!-- The extended global progress state -->
                <div class="progress-extended">&nbsp;</div>
            </div>
        </div>
        <!-- The table listing the files available for upload/download -->
        <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
    </form>

    <!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            <strong class="error text-danger"></strong>
        </td>
        <td>
            <p class="size">Processing...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>
        <td>
            {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start" disabled>
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        <td>
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                {% } %}
            </span>
        </td>
        <td>
            <p class="name">
                {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                {% } else { %}
                    <span>{%=file.name%}</span>
                {% } %}
            </p>
            {% if (file.error) { %}
                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td>
            {% if (file.deleteUrl) { %}
                <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Delete</span>
                </button>
                <input type="checkbox" name="delete" value="1" class="toggle">
            {% } else { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>

                <form id="imageForm">
                <input id="userfile" type="file" name="userfile[]" multiple size="20" class="form-control hidden" placeholder="Upload Image" required/>
                </form>

                <button class="btn btn-lg btn-primary btn-block postBtnTrigger" type="submit">Continue</button>

            </div>
        </div>

    </div>
    <?php $this->load->view('common/modal') ?>
