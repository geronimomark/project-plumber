    <?php $this->load->view('common/navbar', $data)?>

    <div class="container-fluid">
        <div class="row">
            <?php $this->load->view($module.'/leftnav', $data)?>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                <h1>User Management</h1>
                <?php $this->load->view('common/successerror') ?>
                <table id="table1" class="table table-striped">
                    <thead>
                        <tr>
                        <th>User</th>
                        <th>E-mail</th>
                        <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($users->result_object() as $key => $value) :?>
                        <?php if ($value->id != $this->session->id) :?>
                        <tr class="">
                            <td><?=$value->full_name?></td>
                            <td><?=$value->email?></td>
                            <td>
                                <a href="<?=base_url($module.'/editUser/'.$value->id)?>" class="btn btn-sm btn-primary" type="submit">Edit</a>
                                <button class="btn btn-sm btn-primary delete" type="submit">Delete</button>
                                <form class="hidden" method="POST" action="<?=base_url($module.'/delUser')?>">
                                    <input type="hidden" name="id" value="<?=$value->id?>"/> 
                                    <button class="btn btn-sm btn-primary hidden" type="submit">Delete</button>
                                </form>
                            </td>
                        </tr>
                        <?php endif ?>
                    <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div><!-- /row -->

        <div class="row">
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                <form class="form-signin col-sm-6 col-sm-offset-3" method="POST" action="<?=base_url()?><?=$module?>/addUser">
                <h3>Add User</h3>
                <input name="name" type="text" class="form-control" placeholder="Full Name" required>
                <input name="email" type="email" class="form-control" placeholder="Email address" required>
                <input name="password" type="password" class="form-control" placeholder="Password" required>
                <input name="cpassword" type="password" class="form-control" placeholder="ConfirmPassword" required/>
                <select name="role" class="form-control" required>
                    <option value="">SELECT ROLE</option>
                    <option value="Plumber">Plumber</option>
                    <option value="Ops">Ops</option>
                    <option value="Admin">Admin</option>
                    <!-- <option value="SuperAdmin">SuperAdmin</option> -->
                </select>
                <button class="btn btn-lg btn-primary btn-block newUser" type="submit">Add New User</button>
                </form>
            </div>
        </div><!-- /row -->

    </div><!-- /container -->

    <?php $this->load->view('common/modal') ?>