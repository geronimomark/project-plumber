    <?php $this->load->view('common/navbar', $data)?>

    <div class="container-fluid">
        <div class="row">
            <?php $this->load->view('admin/leftnav', $data)?>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                <form class="form-signin col-sm-6 col-sm-offset-3" method="POST" action="<?=base_url()?><?=$module?>/updateUser">
                <?php $this->load->view('common/successerror') ?>
                <h3>Edit User</h3>
                <input type="hidden" name="id" value="<?=$users->id?>"/>
                <input name="name" type="text" class="form-control" placeholder="Full Name" value="<?=$users->full_name?>" required>
                <input name="email" type="email" class="form-control" placeholder="Email address" value="<?=$users->email?>" required>
                <input name="password" type="password" class="form-control" placeholder="Password" required>
                <input name="cpassword" type="password" class="form-control" placeholder="ConfirmPassword" required/>
                <select name="role" class="form-control" required>
                    <option value="">SELECT ROLE</option>
                    <option value="Plumber" <?=($users->role == 'Plumber') ? 'selected' : ''?>>Plumber</option>
                    <option value="Ops" <?=($users->role == 'Ops') ? 'selected' : ''?>>Ops</option>
                    <option value="Admin" <?=($users->role == 'Admin') ? 'selected' : ''?>>Admin</option>
                    <!-- <option value="SuperAdmin" <?=($users->role == 'SuperAdmin') ? 'selected' : ''?>>SuperAdmin</option> -->
                </select>
                <button class="btn btn-lg btn-primary btn-block newUser" type="submit">Update User</button>
                </form>
            </div>
        </div><!-- /row -->

    </div><!-- /container -->

    <?php $this->load->view('common/modal') ?>