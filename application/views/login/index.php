        <div class="container">

            <form class="form-signin" method="post" action="<?=base_url()?>login/authenticate">
                <h2 class="form-signin-heading">Sign in</h2>
                <?php $this->load->view('common/successerror')?>

                <label for="username">Email</label>
                <input type="text" id="username" name="username" class="form-control" placeholder="johndoe" required autofocus>
                <label for="password">Password</label>
                <input type="password" id="password" name="password" class="form-control" placeholder="********" required>
                <div class="checkbox">
                    <label>
                    <!-- <a href="#">Forgot Password</a> -->
                    </label>
                </div>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                <div class="checkbox">
                    <label>
                        <a href="#">Forgot Password</a>
                    </label>
                </div>
            </form>

        </div> <!-- /container -->