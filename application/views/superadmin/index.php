    <?php $this->load->view('common/navbar', $data)?>

    <div class="container theme-showcase" role="main">
        <div class="row">
            <div class="col-md-3 pull-right">
                <form class="navbar-form navbar-right">
                    <input type="text" class="form-control" placeholder="Search..."> 
                </form>
            </div>
            <div class="col-md-12">
                <table class="table table-striped">
                    <thead>
                        <tr>
                        <th>Company</th>
                        <th>Primary contact</th>
                        <th>Contact Number</th>
                        <th>Address</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($organisations->result_object() as $key => $value) :?>
                    <tr>
                        <td><?=$value->title?></td>
                        <td><?=$value->full_name?></td>
                        <td><?=$value->contact_number?></td>
                        <td>
                            <?=$value->address?>
                            <form method="POST" action=<?=base_url() . $module . '/delete'?>>
                            <input type="hidden" name="id" value="<?=$value->id?>" />
                            <button class="btn btn-sm btn-primary pull-right" type="submit">Delete</button>
                            </form>
                            <form method="GET" action=<?=base_url() . $module . '/editorg/' . $value->id?>>
                            <button class="btn btn-sm btn-primary pull-right" type="submit">Edit</button>
                            </form>
                            <form method="GET" action=<?=base_url() . $module . '/administer/' . $value->id?>>
                            <button class="btn btn-sm btn-primary pull-right" type="submit">Administer</button>
                            </form>
                        </td>
                    </tr>
                    <?php endforeach ?>
                    </tbody>
                </table>
            </div>

            <div class="col-md-3 col-sm-offset-4">
            <a class="btn btn-lg btn-primary btn-block userbtn" type="submit" href="<?=base_url('superadmin/addCompany')?>">Add New Company</a>
            </div>

        </div><!-- /row -->
    </div><!-- /container -->
