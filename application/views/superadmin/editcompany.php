    <?php $this->load->view('common/navbar', $data)?>

    <div class="container theme-showcase" role="main">
        <div class="row">
            <form class="addComp" method="post" action="<?=base_url('superadmin/updateCompany')?>">
                <?php $this->load->view('common/successerror') ?>
                <input type="hidden" name="id" value="<?=$organisation->id?>"/>
                <table class="table">
                    <tr>
                        <td>Company Name</td>
                        <td>
                            <div class="form-group">
                            <input type="text" class="form-control" name="companyname" placeholder="XYZ company" maxlength="100" value="<?=$organisation->title?>" required>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Contact Number</td>
                        <td>
                            <div class="form-group">
                            <input type="text" class="form-control numberOnly" name="number" placeholder="09999999999" maxlength="20" value="<?=$organisation->contact_number?>" required>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Address</td>
                        <td>
                            <div class="form-group">
                            <input type="text" class="form-control" name="address" placeholder="#91 Keegan XX" maxlength="150" value="<?=$organisation->address?>" required>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <button type="submit" class="btn btn-lg btn-primary btn-block">Update</button>
                        </td>
                    </tr>
                </table>
            </form>
        </div><!-- /row -->
    </div><!-- /container -->
