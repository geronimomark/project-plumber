    <?php $this->load->view('common/navbar', $data)?>

    <div class="container theme-showcase" role="main">
        <div class="row">
            <form class="addComp" method="post" action="<?=base_url('superadmin/saveCompany')?>">
                <?php $this->load->view('common/successerror') ?>
                <table class="table">
                    <tr>
                        <td>Company Name</td>
                        <td>
                            <div class="form-group">
                            <input type="text" class="form-control" name="companyname" placeholder="XYZ company" maxlength="100" required>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Primary Contact</td>
                        <td>
                            <div class="form-group">
                            <input type="text" class="form-control" name="name" placeholder="Mark Zuckerberg" maxlength="100" required>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Email Address</td>
                        <td>
                            <div class="form-group">
                            <input name="email" type="email" class="form-control" placeholder="Email address" maxlength="50" required>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Password</td>
                        <td>
                            <div class="form-group">
                            <input name="password" type="password" class="form-control" placeholder="Password" required>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Confirm Password</td>
                        <td>
                            <div class="form-group">
                            <input name="cpassword" type="password" class="form-control" placeholder="ConfirmPassword" required/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Contact Number</td>
                        <td>
                            <div class="form-group">
                            <input type="text" class="form-control numberOnly" name="number" placeholder="09999999999" maxlength="20" required>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Address</td>
                        <td>
                            <div class="form-group">
                            <input type="text" class="form-control" name="address" placeholder="#91 Keegan XX" maxlength="150" required>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <button type="submit" class="btn btn-lg btn-primary btn-block">Submit</button>
                        </td>
                    </tr>
                </table>
            </form>
        </div><!-- /row -->
    </div><!-- /container -->
