<script>
    function computeTotalCost(subCost, margin, extraCost) {
        return (parseFloat(subCost) + parseFloat(extraCost) + parseFloat(margin / 100 * parseFloat(subCost))).toFixed(2);
    }

    function computeGST(total) {
        return (parseFloat(total) * 1.1).toFixed(2);
    }

    function errorMessage(message, target) {
        setTimeout(function () {
                $("#modalMessage").remove();
                if (message !== undefined) {
                    $("#loader").after(' <div name="modalMessage" class="alert alert-danger" role="alert"><strong>Error!</strong> ' + message + '</div>');
                } else {
                    if (target === undefined) {
                        $("#loader").after(' <div name="modalMessage" class="alert alert-danger" role="alert"><strong>System Error!</strong> Please contact support.</div>');
                    } else {
                        $(target).before(' <div name="modalMessage" class="alert alert-danger" role="alert"><strong>System Error!</strong> Please contact support.</div>');
                    }
                }
            },
            1000
        );
    }

    $(function() {
        $(document).on('keydown', '.numberOnly', function(e) {
            // Ensure that it is a number and stop the keypress
            if (!(event.keyCode == 8                                // backspace
                || event.keyCode == 9                               // tab
                || event.keyCode == 46                              // delete
                || (event.keyCode >= 35 && event.keyCode <= 40)     // arrow keys/home/end
                || (event.keyCode >= 48 && event.keyCode <= 57)     // numbers on keyboard
                || (event.keyCode >= 96 && event.keyCode <= 105))   // number on keypad
                ) {
                e.preventDefault();
            }
        });

        $(document).on('keydown', '.numberWithDecimal', function(e) {
            // Ensure that it is a number and stop the keypress
            if (!(event.keyCode == 8                                // backspace
                || event.keyCode == 9                               // tab
                || event.keyCode == 46                              // delete
                || event.keyCode == 110                             // dot
                || (event.keyCode >= 35 && event.keyCode <= 40)     // arrow keys/home/end
                || (event.keyCode >= 48 && event.keyCode <= 57)     // numbers on keyboard
                || (event.keyCode >= 96 && event.keyCode <= 105))   // number on keypad
                ) {
                e.preventDefault();
            }

            var text = $(this).val();

            if ((text.indexOf('.') != -1) && event.keyCode == 110) {
                e.preventDefault();
            }

            if ((text.indexOf('.') != -1) && (text.substring(text.indexOf('.')).length > 2) && (event.which != 0 && event.which != 8) && ($(this)[0].selectionStart >= text.length - 2)) {
                e.preventDefault();
            }

        });

        $(document).on('blur', 'input[name="cpassword"]', function(e) {
            if ($(this).val() != $("input[name='password']").val()) {
                errorMessage('Password does not match!');
                $(this).val('');
            }
        });
    });
</script>