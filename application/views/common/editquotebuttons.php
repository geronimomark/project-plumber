<div class="col-md-6">
    <button type="button" class="btn btn-lg btn-primary btn-block" data-toggle="modal" data-target="#AddMargin">Add Margin</button>
</div>
<!-- <div class="col-md-4 middiv"> -->
    <?php //$this->load->view('common/image'); ?>
<!--     <form id="imageForm">
    <input type="file" name="userfile[]" multiple size="20" class="form-control" placeholder="Upload Image" required="" style="margin-bottom:5px">
    </form>
    <button id="uploadImage" class="btn btn-lg btn-primary btn-block" type="submit">Update Image</button>
</div> -->
<div class="col-md-6">
    <button type="button" class="btn btn-lg btn-primary btn-block" data-toggle="modal" data-target="#AddExtra">Add Extra Cost</button>
    <a class="btn btn-lg btn-primary btn-block" type="submit" href="<?=base_url($module.'/index')?>">Done</a>
</div>

<div class="col-md-12">
<br />
        <!-- The file upload form used as target for the file upload widget -->
    <form id="fileupload" action="<?=base_url().$module?>/editUploader" method="POST" enctype="multipart/form-data">
        <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
        <div class="row fileupload-buttonbar">
            <div class="col-lg-7">
                <!-- The fileinput-button span is used to style the file input field as button -->
                <span class="btn btn-success fileinput-button">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span>Add a photo</span>
                    <input type="file" name="files[]" multiple>
                </span>
                <button type="submit" class="btn btn-primary start">
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start upload</span>
                </button>
                <button type="reset" class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel upload</span>
                </button>
                <button type="button" class="btn btn-danger delete">
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Delete</span>
                </button>
                <input type="checkbox" class="toggle">
                <!-- The global file processing state -->
                <span class="fileupload-process"></span>
            </div>
            <!-- The global progress state -->
            <div class="col-lg-5 fileupload-progress fade">
                <!-- The global progress bar -->
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                </div>
                <!-- The extended global progress state -->
                <div class="progress-extended">&nbsp;</div>
            </div>
        </div>
        <!-- The table listing the files available for upload/download -->
        <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
    </form>

    <!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            <strong class="error text-danger"></strong>
        </td>
        <td>
            <p class="size">Processing...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>
        <td>
            {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start" disabled>
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        <td>
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                {% } %}
            </span>
        </td>
        <td>
            <p class="name">
                {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                {% } else { %}
                    <span>{%=file.name%}</span>
                {% } %}
            </p>
            {% if (file.error) { %}
                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td>
            {% if (file.deleteUrl) { %}
                <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Delete</span>
                </button>
                <input type="checkbox" name="delete" value="1" class="toggle">
            {% } else { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
</div>

<?php foreach ($quotematerials->result_object() as $key => $value) :?>
<?php $quoteData['margin'] = ($value->margin) ? $value->margin : 0 ?>
<?php $quoteData['extra'] = ($value->extra_cost) ? $value->extra_cost : 0 ?>
<?php break ?>
<?php endforeach ?>

<!-- Add Margin -->
<div class="modal fade" id="AddMargin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" name="myModalLabel">Add Margin</h4>
            </div>
            <div class="modal-body">
                <input type="text" id="margin" class="form-control numberWithDecimal" maxlength="5" placeholder="Margin Percentage e.g. 20" value="<?=$quoteData['margin']?>"/>
            </div>
            <div class="modal-footer">
                <button id="marginClose" type="button" class="btn btn-default btnModal" data-dismiss="modal">Cancel</button>
                <button id="marginSubmit" type="button" class="btn btn-primary btnModal">Save changes</button>
            </div>
        </div>
    </div>
</div>

<!-- Add Extra -->
<div class="modal fade" id="AddExtra" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" name="myModalLabel">Add Extra Cost</h4>
            </div>
            <div class="modal-body">
                <input type="text" id="extraCost" class="form-control numberWithDecimal" maxlength="11" placeholder="Extra Cost" value="<?=$quoteData['extra']?>"/>
            </div>
            <div class="modal-footer">
                <button id="extraClose" type="button" class="btn btn-default btnModal" data-dismiss="modal">Cancel</button>
                <button id="extraSubmit" type="button" class="btn btn-primary btnModal">Save changes</button>
            </div>
        </div>
    </div>
</div>