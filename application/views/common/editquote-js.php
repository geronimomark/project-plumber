<script>
    $(function(){
        var materials = $("input[type='text'][name='quantity']");
        var margin = $("#margin");
        var extra = $("#extraCost");
        var script = $("textarea[name='newscript']");

        margin.val($("#quoteMargin").val());
        extra.val($("#quoteExtra").val())

        script.on({
            blur: function () {
                var originalScript = $("input[type='hidden'][name='script']").val(),
                    newScript = $(this).val();
                if (originalScript != newScript) {
                    updateScript(newScript);
                }
            }
        });

        materials.on({
            blur: function () {
                var originalQuantity = $(this).parent().find("input[type='hidden'][name='originalQuantity']").val(),
                    unitPrice  = $(this).parent().find("input[type='hidden'][name='unitPrice']").val(),
                    materialId = $(this).parent().find("input[type='hidden'][name='materialId']").val(),
                    quantity   = $(this).val();
                if (quantity != originalQuantity) {
                    updateMaterial(materialId, quantity, unitPrice, $(this));
                }
            }
        });

        function updateScript(newScript) {
            $("#modalTrigger").click();
            $("#myModalLabel").html('Updating Script');
            $("#loader").after(' <div id="modalMessage" class="alert" role="alert"><strong>Saving ...</strong> Please Wait</div>');
            var jqxhr = $.post( "<?=base_url('ajax/updateScript')?>", { script : newScript, quoteId : $("#quoteId").val() })
                .done(function(data) {
                    if (data.success) {
                        setTimeout(function () {
                            $("#modalMessage").remove();
                            $("input[type='hidden'][name='script']").val(newScript);
                            $("#loader").after(' <div name="modalMessage" class="alert alert-success" role="alert"><strong>Success!</strong> Script Updated</div>');
                            },
                            1000
                        );
                    } else {
                        errorMessage();
                    }
                })
                .fail(function(data) {
                    errorMessage();
                })
                .always(function(data) {
                    setTimeout(function () {
                            $("div[name='modalMessage']").remove();
                            $("#modalClose").click();
                        },
                        2000
                    );
                });
        }

        function updateMaterial(materialId, quantity, unitPrice, object) {
            $("#modalTrigger").click();
            $("#myModalLabel").html('Updating Material');
            $("#loader").after(' <div id="modalMessage" class="alert" role="alert"><strong>Saving ...</strong> Please Wait</div>');
            var jqxhr = $.post( "<?=base_url('ajax/updateMaterial')?>", { materialId : materialId, quantity : quantity, unitPrice : unitPrice, quoteId : $("#quoteId").val() })
                .done(function(data) {
                    if (data.success) {
                        setTimeout(function () {
                            $("#modalMessage").remove();
                            object.parent().parent().find('td:last').html('$'+data.newTotalCost);
                            object.parent().find("input[type='hidden'][name='originalQuantity']").val(quantity);
                            $("#quoteSubCost").val(data.subCost);
                            $("#quoteSubCostTd").html('$' + $("#quoteSubCost").val());
                            var total = computeTotalCost($("#quoteSubCost").val(), $("#quoteMargin").val(), $("#quoteExtra").val());
                            $("#quoteTotalTd").html('$' + total);
                            $("#quoteGST").html('$' + computeGST(total));
                            $("#loader").after(' <div name="modalMessage" class="alert alert-success" role="alert"><strong>Success!</strong> Material Updated</div>');
                            },
                            1000
                        );
                    } else {
                        errorMessage();
                    }
                })
                .fail(function(data) {
                    errorMessage();
                })
                .always(function(data) {
                    setTimeout(function () {
                            $("div[name='modalMessage']").remove();
                            $("#modalClose").click();
                        },
                        2000
                    );
                });
        }

        $("#marginSubmit").on({
            click: function() {
                if (margin.val() != '' && margin.val() != $("#quoteMargin").val()) {
                    $("#marginClose").click();
                    $("#modalTrigger").click();
                    $("#myModalLabel").html('Updating Margin');
                    $("#loader").after(' <div id="modalMessage" class="alert" role="alert"><strong>Saving ...</strong> Please Wait</div>');
                    var jqxhr = $.post( "<?=base_url('ajax/updateMargin')?>", { quoteId : $("#quoteId").val(), margin : margin.val() })
                        .done(function(data) {
                            if (data.success) {
                                setTimeout(function () {
                                    $("#modalMessage").remove();
                                    $("#quoteMargin").val(margin.val());
                                    $("#quoteMarginTd").html(margin.val() + '%');
                                    var total = computeTotalCost($("#quoteSubCost").val(), $("#quoteMargin").val(), $("#quoteExtra").val());
                                    $("#quoteTotalTd").html('$' + total);
                                    $("#quoteGST").html('$' + computeGST(total));
                                    $("#loader").after(' <div name="modalMessage" class="alert alert-success" role="alert"><strong>Success!</strong> Margin Updated</div>');
                                    },
                                    1000
                                );
                            } else {
                                errorMessage();
                            }
                        })
                        .fail(function(data) {
                            errorMessage();
                        })
                        .always(function(data) {
                            setTimeout(function () {
                                    $("div[name='modalMessage']").remove();
                                    $("#modalClose").click();
                                },
                                2000
                            );
                        });

                }
            }
        });

        $("#extraSubmit").on({
            click: function() {
                if (extra.val() != '' && extra.val() != $("#quoteExtra").val()) {
                    $("#extraClose").click();
                    $("#modalTrigger").click();
                    $("#myModalLabel").html('Updating Extra Cost');
                    $("#loader").after(' <div id="modalMessage" class="alert" role="alert"><strong>Saving ...</strong> Please Wait</div>');
                    var jqxhr = $.post( "<?=base_url('ajax/updateExtra')?>", { quoteId : $("#quoteId").val(), extra : extra.val() })
                        .done(function(data) {
                            if (data.success) {
                                setTimeout(function () {
                                    $("#modalMessage").remove();
                                    $("#quoteExtra").val(extra.val());
                                    $("#quoteExtraTd").html('$' + extra.val());
                                    var total = computeTotalCost($("#quoteSubCost").val(), $("#quoteMargin").val(), $("#quoteExtra").val());
                                    $("#quoteTotalTd").html('$' + total);
                                    $("#quoteGST").html('$' + computeGST(total));
                                    $("#loader").after(' <div name="modalMessage" class="alert alert-success" role="alert"><strong>Success!</strong> Extra Cost Updated</div>');
                                    },
                                    1000
                                );
                            } else {
                                errorMessage();
                            }
                        })
                        .fail(function(data) {
                            errorMessage();
                        })
                        .always(function(data) {
                            setTimeout(function () {
                                    $("div[name='modalMessage']").remove();
                                    $("#modalClose").click();
                                },
                                2000
                            );
                        });

                }
            }
        });

        $("#uploadImage").on({
            click: function() {
                if ($("input[type='file'][name='userfile']").val() != '') {
                    $("#modalTrigger").click();
                    $("#myModalLabel").html('Updating Image');
                    $("#loader").after(' <div id="modalMessage" class="alert" role="alert"><strong>Saving ...</strong> Please Wait</div>');

                    $.ajax({
                        url: '<?=base_url("ajax/updateImage")?>/' + $("#quoteId").val(),
                        data: new FormData($("#imageForm")[0]),
                        cache: false,
                        contentType: false,
                        processData: false,
                        type: 'POST',
                        success: function(data){
                            if (data.success) {
                                var random = Math.floor(Date.now() / 1000);
                                var count = 0;
                                $("img").each(function() {
                                    $(this).attr("src", "<?=base_url()?>uploads/attachment-" + $("#quoteId").val() + "-" + count + ".jpg?" + random);
                                    count++;
                                });
                                setTimeout(function () {
                                    $("#modalMessage").remove();
                                    $("#loader").after(' <div name="modalMessage" class="alert alert-success" role="alert"><strong>Success!</strong> Image Updated</div>');
                                    },
                                    1000
                                );
                            } else {
                                errorMessage();
                            }
                            setTimeout(function () {
                                    $("div[name='modalMessage']").remove();
                                    $("#modalClose").click();
                                },
                                2000
                            );
                        }
                    });
                }
            }
        });
    });

    $(function() {
        $.ajax({
            url: '<?=base_url("ajax/getImages")?>/<?=$this->uri->segment(3)?>',
            success: function(data){
                var $form = $('#fileupload');
                var files = data;
                $form.fileupload();
                $form.fileupload('option', 'done').call($form, $.Event('done'), {result: {files: files}});
            }
        });
    });
</script>