<script>
    $(function() {
        $("select[name='sewerOrStorm']").val("<?=set_value('sewerOrStorm')?>");
        $("select[name='problem']").val("<?=set_value('problem')?>");
        $("select[name='pipeDiameter']").val("<?=set_value('pipeDiameter')?>");
        $("select[name='existingPipe']").val("<?=set_value('existingPipe')?>");
        $("select[name='surfaceArea']").val("<?=set_value('surfaceArea')?>");
        $("select[name='location']").val("<?=set_value('location')?>");
        $("select[name='daysOrHours']").val("<?=set_value('daysOrHours')?>");
        $("select[name='machineAccess']").val("<?=set_value('machineAccess')?>");
        $("select[name='roadPlates']").val("<?=set_value('roadPlates')?>");
        $("select[name='lengthOfEx']").val("<?=set_value('lengthOfEx')?>");
        $("select[name='depthOfEx']").val("<?=set_value('depthOfEx')?>");

        $("input[type=text], textarea").blur(function() {

            if ($(this).val().trim() != '') {
                $(this).css('background-color', '#7fff7f');
            } else {
                $(this).css('background-color', '#fff');
            }

        });

        $("select").change(function() {

            if ($(this).val().trim() != '') {
                $(this).css('background-color', '#7fff7f');
            } else {
                $(this).css('background-color', '#fff');
            }

        });

        $("select[name='lengthOfEx']").change(function() {
            var depth = $(this).val();
            $("input[name='steelReo']").val(depth);
            $("input[name='gravel']").val(depth);
            $("input[name='topSoil']").val(depth);
            $("input[name='turf']").val(depth);
            $("input[name='rubbish']").val(depth);
            $("input[name='concrete']").val(depth);
            $("input[name='pebbles']").val(depth);
            $("input[name='bitumen']").val(depth);
        });

    });

    $("select[name='daysOrHours']").change(function() {
        var value = $(this).val(),
        input = $("input[name='numberOfDaysOrHours']");
        if (value == 'day_rate') {
            input.attr("placeholder", "Number of days");
        } else if (value == 'hourly_rate') {
            input.attr("placeholder", "Number of hours");
        } else {
            input.attr("placeholder", "Number of Days / Hours");
        }
    });

    $("select[name='machineAccess']").change(function() {
        var value = $(this).val(),
        input = $("input[name='machineDays']");
        if (value == 'yes') {
            input.attr('disabled', false);
        } else {
            input.val('');
            input.attr('disabled', true);
        }
    });


    $("#uploader").click(function(e) {
        e.preventDefault();
        $("#userfile").trigger('click');
    });

    $("button.postBtnTrigger").click(function() {
        $("button.postBtn").trigger('click');
    });

    $(function() {
        $('#fileupload').fileupload({
            // Uncomment the following to send cross-domain cookies:
            //xhrFields: {withCredentials: true},
            url: './uploader'
        });

        $('input[name=excludeCouncilFees]').click(function(){
            var state   = $(this).prop('checked'),
                comment = $('textarea[name=comments]'),
                fee     = $('input[name=councilFees]'),
                text    = comment.val();
            if (state == true) {
                comment.val("Excludes Council Fees" + text);
                fee.val("").prop('disabled', true).css('background-color', '#eee');
            } else {
                text = text.replace("Excludes Council Fees", "");
                comment.val(text);
                fee.prop('disabled', false).css('background-color', 'rgb(255, 255, 255)');
            }
        });
    });

</script>