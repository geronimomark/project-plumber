<script>
    $(function(){
        $("#modalTrigger").remove();
        $(".modal-footer").removeClass('hidden');
        $(".modal-footer .btn-primary").text('Send email');
        
        $(".modal-footer .btn-primary").click(function() {
            $("#hiddenEmail").click();
        });

        var qouteId = $("#sendEmail").attr('data');
        var header = '<tr><td>Add Email</td><td>&nbsp;<a class="btn btn-primary btn-sm add">+</a></td></tr>';
        var email = '<tr><td><input name="email[]" type="email" class="form-control" placeholder="Email Address" required></td><td>&nbsp;<a class="btn btn-danger btn-sm remove">-</a></td></tr>';

        $("#sendEmail").click(function(){
            $(".modal-body").empty().append('<form action="<?=base_url().$module?>/sendEmail" method="POST"><input type="hidden" name="quoteId" value="'+qouteId+'"/><table id="tbl" class="table">' + header + email + '</table><button type="submit" class="hidden" id="hiddenEmail">Save changes</button>');
            $("#myModalLabel").html('Input E-mail Address');
        });

        $(document).on('click', 'a.remove', function(e) {
            var todonew = $("tr td input[name='email[]']");
            if (todonew.length > 1) {
                $(this).parent().parent().remove();
            }
        });

        $(document).on('click', 'a.add', function(e) {
            var todonew = $("tr td input[name='email[]']:last");
            if (todonew.length == 1) {
                if (todonew.val().trim() != '') {
                    todonew.parent().parent().after(email);
                }
            }
        });

    });
</script>