<!DOCTYPE html>
<html lang="en">
    <head>
        <style>
            body {
                font-family: Arial;
            }
            table {
                border-collapse: collapse;
                width: 100%;
                margin-bottom: 30px;
            }

            th, td {
                width: 50%;
                padding: 10px;
                text-align: left;
                border-bottom: 1px solid #ddd;
            }
            .subcost th, .subcost td {
                width: 25%;
            }
            .script th, .script td {
                width: 100%;
            }
        </style>
    </head>
    <body>
        <div class="col-md-9">
            <h1>Generate Quote</h1>
            <p>Reference Number: <?=$quotematerials->result_object()[0]->ref_id?></p>
            <p>Address: <?=$quotematerials->result_object()[0]->address?></p>
            <table class="table table-striped">
                <thead>
                    <tr>
                    <th>Material</th>
                    <th>Quantity</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($quotematerials->result_object() as $key => $value) :?>
                <?php if ($value->type == 1 || $value->type == 4) :?>
                <tr>
                    <td><?=$value->material_name?></td>
                    <td><?=$value->quantity?></td>
                </tr>
                <?php $quoteId   = $value->quote_id; ?>
                <?php $wonStatus = $value->won; ?>
                <?php $quoteData['subCost'] = ($value->sub_cost) ? $value->sub_cost : 0 ?>
                <?php $quoteData['margin'] = ($value->margin) ? $value->margin : 0 ?>
                <?php $quoteData['extra'] = ($value->extra_cost) ? $value->extra_cost : 0 ?>
                <?php $quoteData['id'] = $value->quote_id ?>
                <?php $quoteData['script'] = $value->script ?>
                <?php endif ?>
                <?php endforeach ?>
                </tbody>
            </table>
            <table class="table table-striped">
                <thead>
                    <tr>
                    <th>Plants</th>
                    <th>Quantity</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($quotematerials->result_object() as $key => $value) :?>
                <?php if ($value->type == 2) :?>
                <tr>
                    <td><?=$value->material_name?></td>
                    <td><?=$value->quantity?></td>
                </tr>
                <?php endif ?>
                <?php endforeach ?>
                </tbody>
            </table>
            <table class="table table-striped">
                <thead>
                    <tr>
                    <th>Special Plants</th>
                    <th>Quantity</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($quotematerials->result_object() as $key => $value) :?>
                <?php if ($value->type == 3) :?>
                <tr>
                    <td><?=$value->material_name?></td>
                    <td><?=$value->quantity?></td>
                </tr>
                <?php endif ?>
                <?php endforeach ?>
                </tbody>
            </table>
            <table class="table table-striped subcost">
                <thead>
                    <tr>
                    <th>Sub Cost</th>
                    <th>Margin %</th>
                    <th>Extra Cost</th>
                    <th>Pre GST</th>
                    <th>Including GST</th>
                    </tr>
                </thead>
                <tr>
                    <td>$<?=$quoteData['subCost']?></td>
                    <td><?=$quoteData['margin']?>%</td>
                    <td>$<?=$quoteData['extra']?></td>
                    <td>$<?php $totalCost = computeTotalCost($quoteData['subCost'], $quoteData['margin'], $quoteData['extra']); echo $totalCost.' + gst'?></td>
                    <td>$<?=computeGST($totalCost)?></td>
                </tr>
            </table>
            <table class="table table-striped script">
                <thead>
                    <tr>
                    <th>Script</th>
                    </tr>
                </thead>
                <tr>
                    <td><?=$quoteData['script']?></td>
                </tr>
            </table>

        </div>

        <?php foreach (glob('uploads/attachment-'.$id.'-*') as $file) : ?>
        <!-- <img class="img-thumbnail" alt="200x200" src="<?=$_SERVER["DOCUMENT_ROOT"].'/'.$file?>" data-holder-rendered="true" style="width: 200px; height: 200px;"> -->
        <img class="img-thumbnail" alt="200x200" src="<?=base_url().$file?>" data-holder-rendered="true" style="width: 200px; height: 200px;">
        <?php endforeach ?>

    </body>
</html>