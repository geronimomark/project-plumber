        <div class="container">
            <form class="form-signin" method="POST" action="<?=base_url()?><?=$module?>/newJobValidate">
                <?php foreach ($this->input->post() as $key => $value) :?>
                    <?php if (is_array($value)) :?>
                        <?php foreach ($value as $key2 => $value2) :?>
                            <input type="hidden" name="<?=$key?>[]" value="<?=$value2?>"/>
                        <?php endforeach ?>
                    <?php else :?>
                        <input type="hidden" name="<?=$key?>" value="<?=$value?>"/>
                    <?php endif ?>
                <?php endforeach ?>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                <th>Materials</th>
                                <th>Unit Price</th>
                                <th>Qty</th>
                                <th>Total Price</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $total = 0; foreach ($dynamicMaterials as $key => $value) :?>
                            <?php if ($value['type'] == 1 || $value['type'] == 4) :?>
                            <tr>
                                <td><?=$value['material_name']?></td>
                                <td align="right"><?=$value['unit_price']?></td>
                                <td><?=$value['quantity']?></td>
                                <td align="right"><?=number_format(computeCost($value['unit_price'], $value['quantity']), 2);
                                $total+=computeCost($value['unit_price'], $value['quantity']);?></td>
                            </tr>
                            <?php endif ?>
                            <?php endforeach ?>                            
                            <?php foreach ($staticMaterials->result_object() as $key => $value) :?>
                            <?php if ($value->type == 1) :?>
                            <tr>
                                <td><?=$value->name?></td>
                                <td align="right"><?=$value->{$this->input->post('pipeLength')}?></td>
                                <td><?=$value->quantity?></td>
                                <td align="right"><?=number_format(computeCost($value->{$this->input->post('pipeLength')}, $value->quantity), 2);
                                $total+=computeCost($value->{$this->input->post('pipeLength')}, $value->quantity);?></td>
                            </tr>
                            <?php endif ?>
                            <?php endforeach ?>
                            <tr>
                                <td colspan="3">Total</td>
                                <td align="right"><?=number_format($total, 2)?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-12">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                <th>Plants</th>
                                <th>Unit Price</th>
                                <th>Qty</th>
                                <th>Total Price</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $total = 0; foreach ($dynamicMaterials as $key => $value) :?>
                            <?php if ($value['type'] == 2) :?>
                            <tr>
                                <td><?=$value['material_name']?></td>
                                <td align="right"><?=$value['unit_price']?></td>
                                <td><?=$value['quantity']?></td>
                                <td align="right"><?=number_format(computeCost($value['unit_price'], $value['quantity']), 2);
                                $total+=computeCost($value['unit_price'], $value['quantity']);?></td>
                            </tr>
                            <?php endif ?>
                            <?php endforeach ?>                            
                            <?php foreach ($staticMaterials->result_object() as $key => $value) :?>
                            <?php if ($value->type == 2) :?>
                            <tr>
                                <td><?=$value->name?></td>
                                <td align="right"><?=$value->{$this->input->post('pipeLength')}?></td>
                                <td><?=$value->quantity?></td>
                                <td align="right"><?=number_format(computeCost($value->{$this->input->post('pipeLength')}, $value->quantity), 2);
                                $total+=computeCost($value->{$this->input->post('pipeLength')}, $value->quantity);?></td>
                            </tr>
                            <?php endif ?>
                            <?php endforeach ?>
                            <tr>
                                <td colspan="3">Total</td>
                                <td align="right"><?=number_format($total, 2)?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-12">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                <th>Special Plant</th>
                                <th>Unit Price</th>
                                <th>Qty</th>
                                <th>Total Price</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $total = 0; foreach ($specialPlants as $key => $value) :?>
                            <tr>
                                <td><?=$value['material_name']?></td>
                                <td aling="right"><?=$value['unit_price']?></td>
                                <td><?=$value['quantity']?></td>
                                <td aling="right"><?=number_format($value['total_price'], 2);
                                $total += $value['total_price'];?></td>
                            </tr>
                            <?php endforeach ?>
                            <tr>
                                <td colspan="3">Total</td>
                                <td aling="right"><?=number_format($total, 2)?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <a class="btn btn-lg btn-primary btn-block" href="<?=base_url()?><?=$module?>/newJob">Back</a>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Continue</button>
            </form>
        </div><!-- /container -->