<button type="button" class="btn btn-lg btn-primary btn-block hidden" data-toggle="modal" data-target="#myModal" id="modalTrigger">Submit</button>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body">
                <span id="loader" class="glyphicon glyphicon-refresh spinning"></span>
            </div>
            <div class="modal-footer hidden">
                <button id="modalClose" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>