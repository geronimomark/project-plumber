                <h1>Edit Quote</h1>
                <table class="table table-striped">
                    <thead>
                        <tr>
                        <th>Material</th>
                        <th>Quantity</th>
                        <th>Selling price</th>
                        <th>Total price</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($quotematerials->result_object() as $key => $value) :?>
                    <?php if ($value->type == 1 || $value->type == 4) :?>
                    <tr>
                        <td><?=$value->material_name?></td>
                        <td>
                            <input type="hidden" name="materialId" value="<?=$value->id?>"/>
                            <input type="hidden" name="originalQuantity" value="<?=$value->quantity?>"/>
                            <input type="hidden" name="unitPrice" value="<?=$value->unit_price?>"/>
                            <input type="text" name="quantity" value="<?=$value->quantity?>" class="form-control numberWithDecimal" maxlength="11"/>
                        </td>
                        <td align="right">$<?=$value->unit_price?></td>
                        <td align="right">$<?=$value->total_price?></td>
                    </tr>
                    <?php $quoteData['subCost'] = ($value->sub_cost) ? $value->sub_cost : 0 ?>
                    <?php $quoteData['margin'] = ($value->margin) ? $value->margin : 0 ?>
                    <?php $quoteData['extra'] = ($value->extra_cost) ? $value->extra_cost : 0 ?>
                    <?php $quoteData['id'] = $value->quote_id ?>
                    <?php $quoteData['script'] = $value->script ?>
                    <?php endif ?>
                    <?php endforeach ?>
                    </tbody>
                    <input type="hidden" id="quoteId" value="<?=$quoteData['id']?>"/>
                    <input type="hidden" id="quoteSubCost" value="<?=$quoteData['subCost']?>"/>
                    <input type="hidden" id="quoteMargin" value="<?=$quoteData['margin']?>"/>
                    <input type="hidden" id="quoteExtra" value="<?=$quoteData['extra']?>"/>
                </table>
                <table class="table table-striped">
                    <thead>
                        <tr>
                        <th>Plants</th>
                        <th>Quantity</th>
                        <th>Selling price</th>
                        <th>Total price</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($quotematerials->result_object() as $key => $value) :?>
                    <?php if ($value->type == 2) :?>
                    <tr>
                        <td><?=$value->material_name?></td>
                        <td>
                            <input type="hidden" name="materialId" value="<?=$value->id?>"/>
                            <input type="hidden" name="originalQuantity" value="<?=$value->quantity?>"/>
                            <input type="hidden" name="unitPrice" value="<?=$value->unit_price?>"/>
                            <input type="text" name="quantity" value="<?=$value->quantity?>" class="form-control numberWithDecimal" maxlength="11"/>
                        </td>
                        <td align="right">$<?=$value->unit_price?></td>
                        <td align="right">$<?=$value->total_price?></td>
                    </tr>
                    <?php endif ?>
                    <?php endforeach ?>
                    </tbody>
                </table>
                <table class="table table-striped">
                    <thead>
                        <tr>
                        <th>Special Plants</th>
                        <th>Quantity</th>
                        <th>Selling price</th>
                        <th>Total price</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($quotematerials->result_object() as $key => $value) :?>
                    <?php if ($value->type == 3) :?>
                    <tr>
                        <td><?=$value->material_name?></td>
                        <td>
                            <input type="hidden" name="materialId" value="<?=$value->id?>"/>
                            <input type="hidden" name="originalQuantity" value="<?=$value->quantity?>"/>
                            <input type="hidden" name="unitPrice" value="<?=$value->unit_price?>"/>
                            <input type="text" name="quantity" value="<?=$value->quantity?>" class="form-control numberWithDecimal" maxlength="11"/>
                        </td>
                        <td align="right">$<?=$value->unit_price?></td>
                        <td align="right">$<?=$value->total_price?></td>
                    </tr>
                    <?php endif ?>
                    <?php endforeach ?>
                    </tbody>
                </table>                
                <table class="table table-striped">
                    <thead>
                        <tr>
                        <th>Sub Cost</th>
                        <th>Margin %</th>
                        <th>Extra Cost</th>
                        <th>Pre GST</th>
                        <th>Including GST</th>
                        </tr>
                    </thead>
                    <tr>
                        <td id="quoteSubCostTd">$<?=$quoteData['subCost']?></td>
                        <td id="quoteMarginTd"><?=$quoteData['margin']?>%</td>
                        <td id="quoteExtraTd">$<?=$quoteData['extra']?></td>
                        <td id="quoteTotalTd">$<?php $totalCost = computeTotalCost($quoteData['subCost'], $quoteData['margin'], $quoteData['extra']); echo $totalCost.' + gst'?></td>
                        <td id="quoteGST">$<?=computeGST($totalCost)?></td>
                    </tr>
                </table>
                <table class="table table-striped">
                    <thead>
                        <tr>
                        <th>Script</th>
                        </tr>
                    </thead>
                    <tr>
                        <td>
                            <textarea class="form-control" name="newscript"><?=$quoteData['script']?></textarea>
                            <input type="hidden" name="script" value="<?=$quoteData['script']?>"/>
                        </td>
                    </tr>
                </table>
                <?php $this->load->view('common/modal'); ?>