<?php foreach (glob('uploads/attachment-'.$this->uri->segment(3).'-*') as $file) : ?>
    <?php if ($this->uri->segment(2) == 'generatePDF' || $this->uri->segment(2) == 'sendEmail') : ?>
        <img class="img-thumbnail" alt="200x200" src="<?=$_SERVER["DOCUMENT_ROOT"].'/'.$file?>" data-holder-rendered="true" style="width: 200px; height: 200px;">
    <?php else : ?>
        <img class="img-thumbnail" alt="200x200" src="<?=base_url().$file?>" data-holder-rendered="true" style="width: 200px; height: 200px;">
    <?php endif ?>
<?php endforeach ?>