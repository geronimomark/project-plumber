<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('common/header', $data); ?>

        <!-- Custom styles for this template -->
        <link href="<?=base_url()?>assets/css/<?=$data['css']?>.css" rel="stylesheet">

        <?php if (isset($data['cssArray'])) :?>
            <?php foreach ($data['cssArray'] as $value) :?>
                <link href="<?=base_url()?>assets/css/<?=$value?>" rel="stylesheet">
            <?php endforeach ?>
        <?php endif ?>
    </head>

    <body>

        <?php $this->load->view($data['content'], $data); ?>

        <?php $this->load->view('common/bottomjs'); ?>

        <?php if (isset($data['js'])) $this->load->view($data['js']); ?>

        <?php if (isset($data['blueimp'])) :?>
            <?php foreach ($data['blueimp'] as $value) :?>
                <script src="<?=$value?>"></script>
            <?php endforeach ?>
        <?php endif ?>

        <?php if (isset($data['jsArray'])) :?>    
            <?php foreach ($data['jsArray'] as $value) :?>
                <script src="<?=base_url()?>assets/js/<?=$value?>"></script>
            <?php endforeach ?>
        <?php endif ?>

    </body>
</html>
