<script>
    $(function() {
        $('#table1').DataTable();
        
        $(document).on('click', 'tr.clickable', function(e) {
            var id = $(this).children('input').eq(0).val();
            window.location = "<?=base_url()?><?=$module?>/viewQuote/" + id;
        });

        $(document).on('click', 'button.btnTrigger', function(e) {
            $(this).parent().parent().trigger('click');
        });

    });
</script>