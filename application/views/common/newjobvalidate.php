        <div class="container">
            <h1>Create New Job</h1>
            <form class="form-signin" method="POST" action="<?=base_url()?><?=$module?>/postQuote">
                <?php foreach ($this->input->post() as $key => $value) :?>
                    <?php if (is_array($value)) :?>
                        <?php foreach ($value as $key2 => $value2) :?>
                            <input type="hidden" name="<?=$key?>[]" value="<?=$value2?>"/>
                        <?php endforeach ?>
                    <?php else :?>
                        <input type="hidden" name="<?=$key?>" value="<?=$value?>"/>
                    <?php endif ?>
                <?php endforeach ?>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">Job Ref# <?=$this->input->post('refNumber')?></h3>
                                <h3 class="panel-title">Tech: <?=$this->session->fullName?></h3>
                            </div>
                            <div class="panel-body">
                                <?=$script?>
                                <input type="hidden" name="script" value="<?=$script?>"/>
                            </div>
                        </div>
                    </div>
                </div>
                <a class="btn btn-lg btn-primary btn-block" href="<?=base_url()?><?=$module?>/newJob">Back</a>
                <!-- <button type="button" class="btn btn-lg btn-primary btn-block" data-toggle="modal" data-target="#myModal">Submit</button> -->
                <button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
            </form>
        </div><!-- /container -->