                <h1><?=($this->uri->segment(2) == "won" ? "Won" : "Current")?> Quotes</h1>
                <table id="table1" class="table table-striped">
                    <thead>
                        <tr>
                        <th>Action</th>
                        <th class="<?=($module=='dashboard') ? 'hidden' : ''?>">Quotation amount</th>
                        <th>Made by User</th>
                        <th>Reference</th>
                        <th>Address</th>
                        <th class="<?=($module=='dashboard') ? 'hidden' : ''?>">Script</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($currentQuotes->result_object() as $key => $value) :?>
                    <tr class="clickable">
                        <input type="hidden" name="qouteId" value="<?=$value->quote_id?>"/>
                        <td><button class="btn btn-primary btn-sm btnTrigger">View</button></td>
                        <td class="<?=($module=='dashboard') ? 'hidden' : ''?>">$<?=$value->total?></td>
                        <td><?=$value->email?></td>
                        <td><?=$value->ref_id?></td>
                        <td><?=$value->address?></td>
                        <td class="<?=($module=='dashboard') ? 'hidden' : ''?>"><?=substr($value->script, 0, 100)?><?=(strlen($value->script) > 100) ? '...' : ''?></td>
                    </tr>
                    <?php endforeach ?>
                    </tbody>
                </table>