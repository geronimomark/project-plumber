<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <?php $target = "#navbar"; if ($this->session->role === 'Admin') $target = "#leftnavbar"; ?>
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="<?=$target?>" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?=base_url('base/home')?>"><?=$project?></a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <?php if ($this->session->origOrgId) :?>
        <li><a href="<?=base_url('admin/logoff')?>">Back to Super Admin Module</a></li>
        <?php endif ?>
        <li><a href="<?=base_url('login/logoff')?>">Logout</a></li>
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</nav>
