            <div class="col-md-9">
                <h1>View Quote</h1>
                <table class="table table-striped">
                    <thead>
                        <tr>
                        <th>Material</th>
                        <th>Quantity</th>
                        <th class="<?=($module=='ops' && strpos($referer, 'quotes') !== false) ? 'hidden' : ''?>">Selling price</th>
                        <th class="<?=($module=='ops' && strpos($referer, 'quotes') !== false) ? 'hidden' : ''?>">Total price</th>
                        <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($quotematerials->result_object() as $key => $value) :?>
                    <?php if ($value->type == 1 || $value->type == 4) :?>
                    <tr>
                        <td><?=$value->material_name?></td>
                        <td><?=$value->quantity?></td>
                        <td align="right" class="<?=($module=='ops' && strpos($referer, 'quotes') !== false) ? 'hidden' : ''?>">$<?=$value->unit_price?></td>
                        <td align="right" class="<?=($module=='ops' && strpos($referer, 'quotes') !== false) ? 'hidden' : ''?>">$<?=$value->total_price?></td>
                        <td style='color:red'><?php if ($value->material_name == 'Concrete BTS' && $value->quantity > 32) { echo 'Concrete Taxi'; } ?></td>
                    </tr>
                    <?php $quoteId   = $value->quote_id; ?>
                    <?php $wonStatus = $value->won; ?>
                    <?php $quoteData['subCost'] = ($value->sub_cost) ? $value->sub_cost : 0 ?>
                    <?php $quoteData['margin'] = ($value->margin) ? $value->margin : 0 ?>
                    <?php $quoteData['extra'] = ($value->extra_cost) ? $value->extra_cost : 0 ?>
                    <?php $quoteData['id'] = $value->quote_id ?>
                    <?php $quoteData['script'] = $value->script ?>
                    <?php endif ?>
                    <?php endforeach ?>
                    </tbody>
                </table>
                <table class="table table-striped">
                    <thead>
                        <tr>
                        <th>Plants</th>
                        <th>Quantity</th>
                        <th class="<?=($module=='ops' && strpos($referer, 'quotes') !== false) ? 'hidden' : ''?>">Selling price</th>
                        <th class="<?=($module=='ops' && strpos($referer, 'quotes') !== false) ? 'hidden' : ''?>">Total price</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($quotematerials->result_object() as $key => $value) :?>
                    <?php if ($value->type == 2) :?>
                    <tr>
                        <td><?=$value->material_name?></td>
                        <td><?=$value->quantity?></td>
                        <td align="right" class="<?=($module=='ops' && strpos($referer, 'quotes') !== false) ? 'hidden' : ''?>">$<?=$value->unit_price?></td>
                        <td align="right" class="<?=($module=='ops' && strpos($referer, 'quotes') !== false) ? 'hidden' : ''?>">$<?=$value->total_price?></td>
                    </tr>
                    <?php endif ?>
                    <?php endforeach ?>
                    </tbody>
                </table>
                <table class="table table-striped">
                    <thead>
                        <tr>
                        <th>Special Plants</th>
                        <th>Quantity</th>
                        <th class="<?=($module=='ops' && strpos($referer, 'quotes') !== false) ? 'hidden' : ''?>">Selling price</th>
                        <th class="<?=($module=='ops' && strpos($referer, 'quotes') !== false) ? 'hidden' : ''?>">Total price</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($quotematerials->result_object() as $key => $value) :?>
                    <?php if ($value->type == 3) :?>
                    <tr>
                        <td><?=$value->material_name?></td>
                        <td><?=$value->quantity?></td>
                        <td align="right" class="<?=($module=='ops' && strpos($referer, 'quotes') !== false) ? 'hidden' : ''?>">$<?=$value->unit_price?></td>
                        <td align="right" class="<?=($module=='ops' && strpos($referer, 'quotes') !== false) ? 'hidden' : ''?>">$<?=$value->total_price?></td>
                    </tr>
                    <?php endif ?>
                    <?php endforeach ?>
                    </tbody>
                </table>
                <table class="table table-striped">
                    <thead>
                        <tr>
                        <th>Sub Cost</th>
                        <th>Margin %</th>
                        <th>Extra Cost</th>
                        <th>Pre GST</th>
                        <th>Including GST</th>
                        </tr>
                    </thead>
                    <tr>
                        <td>$<?=$quoteData['subCost']?></td>
                        <td><?=$quoteData['margin']?>%</td>
                        <td>$<?=$quoteData['extra']?></td>
                        <td>$<?php $totalCost = computeTotalCost($quoteData['subCost'], $quoteData['margin'], $quoteData['extra']); echo $totalCost.' + gst'?></td>
                        <td>$<?=computeGST($totalCost)?></td>
                    </tr>
                </table>
                <table class="table table-striped">
                    <thead>
                        <tr>
                        <th>Script</th>
                        </tr>
                    </thead>
                    <tr>
                        <td><textarea rows="10" class="form-control" disabled="true"><?=$quoteData['script']?></textarea></td>
                    </tr>
                </table>

            </div>
            <div class="col-md-3 pdfbuttons">
                <a href="<?=base_url($module.'/generatePDF/'.$quoteId)?>" class="btn btn-lg btn-primary btn-block" type="submit" target="_blank">Generate PDF</button>
                <!-- <button class="btn btn-lg btn-primary btn-block" type="submit">Archive Quote</button> -->
                <a class="btn btn-lg btn-primary btn-block" type="submit" href="<?=base_url($module.'/editQuote/'.$quoteId.'/'.$wonStatus)?>">Edit</a>
                <button type="button" class="btn btn-lg btn-primary btn-block" data-toggle="modal" data="<?=$quoteId?>" data-target="#myModal" id="sendEmail">Send E-mail</button>
                <?php if ($wonStatus != 1) :?>
                <form method="POST" action="<?=base_url()?><?=$module?>/quoteWon">
                <input type="hidden" name="quoteId" value="<?=$quoteId?>"/>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Quote Won</button>
                </form>
                <?php endif ?>
            </div>
            <?php $this->load->view('common/modal') ?>
