    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="<?=base_url()?>vendor/twbs/bootstrap/docs/assets/js/ie10-viewport-bug-workaround.js"></script>
        <script src="<?=base_url()?>vendor/components/jquery/jquery.min.js"></script>
        <script src="<?=base_url()?>vendor/twbs/bootstrap/dist/js/bootstrap.min.js"></script>

        <?php $this->load->view('common/commonfunctions-js') ?>

        <?php if ($this->uri->segment(2) == 'configuration') : ?>
            <?php $this->load->view('common/datatable-js') ?>
            <?php $this->load->view('admin/configuration-js') ?>
        <?php endif ?>

        <?php if ($this->uri->segment(2) == 'users') : ?>
            <?php $this->load->view('common/datatable-js') ?>
            <?php $this->load->view('admin/users-js') ?>
        <?php endif ?>

        <?php $this->load->view('common/newjobupload-js') ?> 
