        <?php $this->load->view('common/navbar', $data)?>

        <div class="container theme-showcase" role="main">
            <?php $this->load->view('common/successerror')?>
            <div class="row">
                <form method="get" action="<?=base_url()?>ops/newJob">
                <div class="col-sm-4">
                    <button class="btn btn-lg btn-primary btn-block btn-ops" type="submit">Add Quote</button>
                </div>
                </form>
                <form method="get" action="<?=base_url()?>ops/won">
                <div class="col-sm-4">
                    <button class="btn btn-lg btn-primary btn-block btn-ops" type="submit">Won Quotes</button>
                </div>
                </form>
                <form method="get" action="<?=base_url()?>ops/quotes">
                <div class="col-sm-4">
                    <button class="btn btn-lg btn-primary btn-block btn-ops" type="submit">Current Quotes</button>
                </div>
                </form>

            </div> <!-- /row -->
        </div> <!-- /container -->