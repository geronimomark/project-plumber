    <?php $this->load->view('common/navbar', $data)?>

    <div class="container theme-showcase" role="main">
        <div class="row">
            <div class="col-md-12">
                <?php $this->load->view('common/quotematerials'); ?>
            </div>
        </div>
        <div class="row">
            <?php $this->load->view('common/editquotebuttons') ?>
        </div>
    </div><!-- /container -->