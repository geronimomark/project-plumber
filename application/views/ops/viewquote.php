    <?php $this->load->view('common/navbar', $data)?>

    <div class="container theme-showcase" role="main">
        <div class="row">
            <?php $this->load->view('common/viewquotes') ?>
        </div>

        <div class="row">
            <div class="col-md-12 middiv">
                <?php $this->load->view('common/image') ?>
            </div>
        </div>
    </div><!-- /container -->