        <div class="container">
            <h1>View Job</h1>
            <form class="form-signin" method="get" action="<?=base_url()?>dashboard">
                <?php if ($todo->num_rows() > 0) :?>
                    <?php foreach ($todo->result_object() as $key => $value) : ?>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" /> <?=ucfirst($value->todo)?>
                        </label>
                    </div>   
                    <?php endforeach ?>
                <?php else : ?>
                    <h3>No To do list found!</h3>
                <?php endif ?>
                <!-- <button class="btn btn-lg btn-primary btn-block" type="submit">Attach Image</button> -->
                <a class="btn btn-lg btn-primary btn-block" href="<?=base_url()?>dashboard/current">Back</a>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Save</button>
            </form>
        </div><!-- /container -->