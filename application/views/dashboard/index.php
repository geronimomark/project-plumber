        <div class="container">
            <h1>Plumber Home</h1>
            <?php $this->load->view('common/successerror')?>
            <form class="form-signin" method="get" action="<?=base_url()?>dashboard/newJob">
                <button class="btn btn-lg btn-primary btn-block" type="submit">New Job</button>
                <div class="checkbox">
                    <label>
                    </label>
                </div>
            </form>

            <form class="form-signin" method="get" action="<?=base_url()?>dashboard/current">
                <button class="btn btn-lg btn-primary btn-block" type="submit">Current</button>
                <div class="checkbox">
                    <label>
                        <a href="<?=base_url()?>login/logoff">Logout</a>
                    </label>
                </div>
            </form>

        </div> <!-- /container -->