<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Problem extends CI_Migration
{

    public function up()
    {
        $fields = [
            'problem' => [
                'type'       => 'VARCHAR',
                'constraint' => '50',
            ],
        ];
        $this->dbforge->add_column('quotes', $fields);
    }

    public function down()
    {
        $this->dbforge->drop_column('quotes', 'problem');
    }
}
