<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Create_Template_Surface extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_field([
            'name' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ],
            'script' => [
                'type'       => 'VARCHAR',
                'constraint' => '5000',
            ],

        ]);
        $this->dbforge->create_table('template_surface_area');

        $this->dbforge->add_field([
            'name' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ],
            'quantity' => [
                'type'       => 'INT',
                'constraint' => '2',
            ],

        ]);
        $this->dbforge->create_table('template_surface_area_materials');
    }

    public function down()
    {
        $this->dbforge->drop_table('template_surface_area', true);
        $this->dbforge->drop_table('template_surface_area_materials', true);
    }
}
