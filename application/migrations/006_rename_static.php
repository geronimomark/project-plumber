<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Rename_Static extends CI_Migration
{

    public function up()
    {
        $this->renameStatic();
    }

    private function renameStatic()
    {
        $this->dbforge->rename_table('surface_areas_materials_static', 'surface_areas_materials');
    }
    
    public function down()
    {
        $this->dbforge->rename_table('surface_areas_materials', 'surface_areas_materials_static');
    }
}
