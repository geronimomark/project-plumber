<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Pipelength_price extends CI_Migration
{

    public function up()
    {
        $this->dbforge->drop_column('materials', 'unit_price');

        $fields = [
            'onehundredmm' => [
                'type'       => 'DOUBLE',
                'constraint' => '9,2',
            ],
            'onefiftymm' => [
                'type'       => 'DOUBLE',
                'constraint' => '9,2',
            ],
            'twotwentyfivemm' => [
                'type'       => 'DOUBLE',
                'constraint' => '9,2',
            ],
            'threehundredmm' => [
                'type'       => 'DOUBLE',
                'constraint' => '9,2',
            ],
        ];
        $this->dbforge->add_column('materials', $fields);
    }

    public function down()
    {
        $this->dbforge->drop_column('materials', 'onehundredmm');
        $this->dbforge->drop_column('materials', 'onefiftymm');
        $this->dbforge->drop_column('materials', 'twotwentyfivemm');
        $this->dbforge->drop_column('materials', 'threehundredmm');

        $fields = [
            'unit_price' => [
                'type'       => 'DOUBLE',
                'constraint' => '9,2',
            ],        
        ];
        $this->dbforge->add_column('materials', $fields);
    }
}
