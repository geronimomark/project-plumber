<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Script_In_Quotes extends CI_Migration
{

    public function up()
    {
        $fields = [
            'script' => [
                'type'       => 'VARCHAR',
                'constraint' => '5000',
            ]
        ];
        $this->dbforge->add_column('quotes', $fields);
    }

    public function down()
    {
        $this->dbforge->drop_column('quotes', 'script');
    }
}
