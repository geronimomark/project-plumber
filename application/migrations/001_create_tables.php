<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Create_Tables extends CI_Migration
{

    public function up()
    {
        $this->createSurfaceAreas();
        $this->createMaterials();
        $this->createSurfaceMaterialStatic();
        $this->createQuotations();
        $this->createQuoteMaterials();
        $this->createSurfaceMaterialDynamic();

    }

    private function createSurfaceAreas()
    {
        $this->dbforge->add_field([
            'id' => [
                'type'           => 'INT',
                'auto_increment' => true,
            ],
            'name' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ],
            'script' => [
                'type'       => 'VARCHAR',
                'constraint' => '5000',
            ]
        ]);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('surface_areas');
    }

    private function createMaterials()
    {
        $this->dbforge->add_field([
            'id' => [
                'type'           => 'INT',
                'auto_increment' => true,
            ],
            'name' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ],
            'unit_price' => [
                'type'       => 'DOUBLE',
                'constraint' => '9,2',
            ],
            'unique_mat' => [
                'type'       => 'BOOLEAN',
            ],
            'static' => [
                'type'       => 'BOOLEAN',
            ],
            'type' => [
                'type'       => 'INT',
                'constraint' => '5',
            ]
        ]);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('materials');
    }

    private function createSurfaceMaterialStatic()
    {
        $this->dbforge->add_field([
            'id' => [
                'type'           => 'INT',
                'auto_increment' => true,
            ],
            'surface_id' => [
                'type'       => 'INT',
            ],
            'material_id' => [
                'type'       => 'INT',
            ],
            'quantity' => [
                'type'       => 'INT',
            ]
        ]);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('surface_areas_materials_static');
    }

    private function createQuotations()
    {
        $this->dbforge->add_field([
            'id' => [
                'type'           => 'INT',
                'auto_increment' => true,
            ],
            'surface_id' => [
                'type'       => 'INT',
            ]
        ]);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('quotes');
    }

    private function createQuoteMaterials()
    {
        $this->dbforge->add_field([
            'id' => [
                'type'           => 'INT',
                'auto_increment' => true,
            ],
            'quote_id' => [
                'type'       => 'INT',
            ],
            'material_name' => [
                'type'       => 'VARCHAR',
                'constraint' => '255',
            ],
            'unit_price' => [
                'type'       => 'DOUBLE',
                'constraint' => '9,2',
            ],
            'quantity' => [
                'type'       => 'INT',
                'constraint' => '5',
            ],
            'total_price' => [
                'type'       => 'DOUBLE',
                'constraint' => '9,2',
            ]
        ]);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('quote_materials');
    }

    private function createSurfaceMaterialDynamic()
    {
        $this->dbforge->add_field([
            'id' => [
                'type'           => 'INT',
                'auto_increment' => true,
            ],
            'surface_id' => [
                'type'       => 'INT',
            ],
            'material_id' => [
                'type'       => 'INT',
            ],
            'quantity' => [
                'type'       => 'INT',
            ],
            'quote_id' => [
                'type'       => 'INT',
            ]
        ]);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('surfaceareas_materials_dynamic');
    }

    public function down()
    {
        $this->dbforge->drop_table('surface_areas', true);
        $this->dbforge->drop_table('materials', true);
        $this->dbforge->drop_table('surface_areas_materials_static', true);
        $this->dbforge->drop_table('quotes', true);
        $this->dbforge->drop_table('quote_materials', true);
        $this->dbforge->drop_table('surfaceareas_materials_dynamic', true);
    }
}