<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Type_Quote_Materials extends CI_Migration
{

    public function up()
    {
        $fields = [
            'type' => [
                'type'       => 'INT',
                'constraint' => '1',
            ],
        ];
        $this->dbforge->add_column('quote_materials', $fields);
    }

    public function down()
    {
        $this->dbforge->drop_column('quote_materials', 'type');
    }
}
