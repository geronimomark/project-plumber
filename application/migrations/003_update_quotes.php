<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Update_Quotes extends CI_Migration
{

    public function up()
    {
        $this->addNewFieldsInQuotes();
    }

    private function addNewFieldsInQuotes()
    {
        $fields = [
            'ref_id' => [
                'type'       => 'VARCHAR',
                'constraint' => '50',
            ],
            'address' => [
                'type'       => 'VARCHAR',
                'constraint' => '255',
            ],
        ];
        $this->dbforge->add_column('quotes', $fields);
    }

    public function down()
    {
        $this->dbforge->drop_column('quotes', 'ref_id');
        $this->dbforge->drop_column('quotes', 'address');
    }
}
