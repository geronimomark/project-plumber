<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Update_Margin extends CI_Migration
{

    public function up()
    {
        $this->addNewFieldsInQuotes();
    }

    private function addNewFieldsInQuotes()
    {
        $fields = [
            'margin' => [
                'type'       => 'DOUBLE',
                'constraint' => '9,2'
            ]
        ];
        $this->dbforge->modify_column('quotes', $fields);
    }

    public function down()
    {
        $fields = [
            'margin' => [
                'type'       => 'INT',
                'constraint' => '11'
            ]
        ];
        $this->dbforge->modify_column('quotes', $fields);
    }
}
