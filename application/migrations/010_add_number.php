<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Number extends CI_Migration
{

    public function up()
    {
        $this->addNewFieldsInUsers();
    }

    private function addNewFieldsInUsers()
    {
        $fields = [
            'contact_number' => [
                'type'       => 'VARCHAR',
                'constraint' => '20',
            ],
        ];
        $this->dbforge->add_column('organisations', $fields);
    }

    public function down()
    {
        $this->dbforge->drop_column('organisations', 'contact_number');
    }
}
