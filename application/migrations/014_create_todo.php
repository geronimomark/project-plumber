<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Create_Todo extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_field([
            'id' => [
                'type'           => 'INT',
                'auto_increment' => true,
            ],
            'surface_id' => [
                'type'       => 'INT',
                'constraint' => '11',
            ],
            'todo' => [
                'type'       => 'VARCHAR',
                'constraint' => '255',
            ],
        ]);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('surface_todo');
    }

    public function down()
    {
        $this->dbforge->drop_table('surface_todo', true);
    }
}
