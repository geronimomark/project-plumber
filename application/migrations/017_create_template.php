<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Create_Template extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_field([
            'name' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ],
            'type' => [
                'type'       => 'INT',
                'constraint' => '5',
            ],
            'key' => [
                'type'       => 'VARCHAR',
                'constraint' => '30',
                'null'       => TRUE,
            ],
            'onehundredmm' => [
                'type'       => 'DOUBLE',
                'constraint' => '9,2',
            ],
            'onefiftymm' => [
                'type'       => 'DOUBLE',
                'constraint' => '9,2',
            ],
            'twotwentyfivemm' => [
                'type'       => 'DOUBLE',
                'constraint' => '9,2',
            ],
            'threehundredmm' => [
                'type'       => 'DOUBLE',
                'constraint' => '9,2',
            ],
        ]);
        $this->dbforge->create_table('template');
    }

    public function down()
    {
        $this->dbforge->drop_table('template', true);
    }
}
