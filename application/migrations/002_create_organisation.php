<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Create_Organisation extends CI_Migration {

    public function up()
    {
        $this->addOrgInSurfaceArea();
        $this->addOrgInMaterials();
        $this->addOrgInQuotes();
    }

    private function addOrgInSurfaceArea()
    {
        $fields = [
            'org_id' => [
                'type' => 'INT'
            ]
        ];
        $this->dbforge->add_column('surface_areas', $fields);
    }

    private function addOrgInMaterials()
    {
        $this->dbforge->drop_column('materials', 'unique_mat');
        $this->dbforge->drop_column('materials', 'static');

        $fields = [
            'org_id' => [
                'type' => 'INT'
            ],
            'key' => [
                'type'       => 'VARCHAR',
                'constraint' => '30'
            ],
        ];
        $this->dbforge->add_column('materials', $fields);
    }

    private function addOrgInQuotes()
    {
        $fields = [
            'org_id' => [
                'type' => 'INT'
            ],
            'won' => [
                'type' => 'BOOLEAN'
            ],
            'marked_for_deletion' => [
                'type'    => 'TIMESTAMP',
                'null'    => true,
                'default' => null
            ],
        ];
        $this->dbforge->add_column('quotes', $fields);
    }

    public function down()
    {
        $this->dbforge->drop_column('surface_areas', 'org_id');

        $fields = [
            'unique_mat' => [
                'type'       => 'BOOLEAN',
            ],
            'static' => [
                'type'       => 'BOOLEAN',
            ],
        ];
        $this->dbforge->add_column('materials', $fields);
        $this->dbforge->drop_column('materials', 'org_id');
        $this->dbforge->drop_column('materials', 'key');

        $this->dbforge->drop_column('quotes', 'org_id');
        $this->dbforge->drop_column('quotes', 'won');
        $this->dbforge->drop_column('quotes', 'marked_for_deletion');

    }

}
