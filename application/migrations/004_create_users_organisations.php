<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Create_Users_Organisations extends CI_Migration
{

    public function up()
    {
        $this->createUsers();
        $this->createOrganisations();
        $this->addNewFieldsInQuotes();
    }

    private function createUsers()
    {
        $this->dbforge->add_field([
            'id' => [
                'type'           => 'INT',
                'auto_increment' => true,
            ],
            'full_name' => [
                'type'       => 'VARCHAR',
                'constraint' => '255',
            ],
            'org_id' => [
                'type'       => 'INT',
            ],
            'email' => [
                'type'       => 'VARCHAR',
                'constraint' => '100',
            ],
            'password' => [
                'type'       => 'VARCHAR',
                'constraint' => '255',
            ],
        ]);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('users');
    }

    private function createOrganisations()
    {
        $this->dbforge->add_field([
            'id' => [
                'type'           => 'INT',
                'auto_increment' => true,
            ],
            'title' => [
                'type'       => 'VARCHAR',
                'constraint' => '255',
            ],
            'logo' => [
                'type'       => 'VARCHAR',
                'constraint' => '255',
            ],
        ]);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('organisations');
    }

    private function addNewFieldsInQuotes()
    {
        $fields = [
            'deleting_on_date' => [
                'type'       => 'DATE',
            ],
            'sub_cost' => [
                'type'       => 'DOUBLE',
                'constraint' => '9,2',
            ],
            'margin' => [
                'type'       => 'DOUBLE',
                'constraint' => '9,2',
            ],
            'extra_cost' => [
                'type'       => 'DOUBLE',
                'constraint' => '9,2',
            ],
            'created_by' => [
                'type'       => 'INT',
            ],
        ];
        $this->dbforge->add_column('quotes', $fields);
    }

    public function down()
    {
        $this->dbforge->drop_table('users', true);
        $this->dbforge->drop_table('organisations', true);
        
        $this->dbforge->drop_column('quotes', 'deleting_on_date');
        $this->dbforge->drop_column('quotes', 'sub_cost');
        $this->dbforge->drop_column('quotes', 'margin');
        $this->dbforge->drop_column('quotes', 'extra_cost');
        $this->dbforge->drop_column('quotes', 'created_by');
    }
}
