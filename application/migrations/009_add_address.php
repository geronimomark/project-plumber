<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Address extends CI_Migration
{

    public function up()
    {
        $this->addNewFieldsInUsers();
    }

    private function addNewFieldsInUsers()
    {
        $fields = [
            'address' => [
                'type'       => 'VARCHAR',
                'constraint' => '255',
            ],
        ];
        $this->dbforge->add_column('organisations', $fields);
    }

    public function down()
    {
        $this->dbforge->drop_column('organisations', 'address');
    }
}
