<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Suburb extends CI_Migration
{

    public function up()
    {
        $fields = [
            'suburb' => [
                'type'       => 'VARCHAR',
                'constraint' => '50',
            ],
        ];
        $this->dbforge->add_column('quotes', $fields);
    }

    public function down()
    {
        $this->dbforge->drop_column('quotes', 'suburb');
    }
}
