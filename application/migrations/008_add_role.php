<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Role extends CI_Migration
{

    public function up()
    {
        $this->addNewFieldsInUsers();
    }

    private function addNewFieldsInUsers()
    {
        $fields = [
            'role' => [
                'type'       => 'VARCHAR',
                'constraint' => '10',
            ],
        ];
        $this->dbforge->add_column('users', $fields);
    }

    public function down()
    {
        $this->dbforge->drop_column('users', 'role');
    }
}
