<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migrate extends CI_Controller
{

    public function index($version = null)
    {
        $this->load->library('migration');

        if ($version !== null) {
            if ($this->migration->version($version) === false) {
                show_error($this->migration->error_string());
            }
        } elseif ($this->migration->current() === false) {
            show_error($this->migration->error_string());
        }
    }

    public function latest()
    {
        $this->load->library('migration');
        $this->migration->latest();
        echo "Updated database";
    }

    public function initializeDB()
    {
        $this->seedData();
        $this->seedOrganisation();
        $this->seedUsers();
        $this->seedScript();
        $this->updatePvcKey();
        $this->updateMaterials();
        $this->seedPipePrices();
        $this->updateSomeMaterialTypesToFour();
        $this->deleteGravelInStaticMaterials();
        $this->truncateQuotes();
    }

    public function seedData()
    {
        $this->db->query("
            INSERT INTO `materials` (`name`, `onehundredmm`, `type`, `key`) VALUES
            ('PVC Pipe per metre', 100, 1, null),
            ('Junction', 0, 1, null),
            ('Inspection opening', 0, 1, null),
            ('Concrete BTS', 0, 1, null),
            ('Glue', 0, 1, null),
            ('Cleaner', 0, 1, null),
            ('Earthenware to PVC PQ', 0, 1, null),
            ('PVC to PVC PQ', 0, 1, null),
            ('Slab Repair', 0, 1, null),
            ('45 Bend', 0, 1, null),
            ('90 Bend', 0, 1, null),
            ('Earthenware to slab repair coupling', 0, 1, null),
            ('Silicone', 0, 1, null),
            ('Sand/Cement', 0, 1, null),
            ('Gravel', 0, 1, null),
            ('Top Soil', 0, 1, null),
            ('Turf', 0, 1, null),
            ('Rubbish', 0, 1, null),
            ('Road Saw', 0, 2, null),
            ('Jackhammer', 0, 2, null),
            ('Wheelbarrow', 0, 2, null),
            ('Petrol', 0, 2, null),
            ('Hose', 0, 2, null),
            ('Spade/Shovel', 0, 2, null),
            ('Jet', 0, 2, null),
            ('CCTV', 0, 2, null),
            ('Broom', 0, 2, null),
            ('Barricades', 0, 2, null),
            ('Steel', 0, 2, null),
            ('Timber', 0, 2, null),
            ('Reo Bars', 0, 2, null),
            ('Stencil', 0, 2, null),
            ('Sewer Diagram', 0, 2, null),
            ('DBYD', 0, 2, null),
            ('Concrete Taxi', 0, 2, null),
            ('Timber Float', 0, 2, null),
            ('Council Fees', 0, 3, null),
            ('Road Plates / Week', 0, 3, null),
            ('Plate Compactor', 0, 3, null),
            ('Submersible Pump', 0, 3, null),
            ('Generator', 0, 3, null),
            ('Excavator', 0, 3, null),
            ('Steel Float', 0, 3, null),
            ('Day Rate', 150, 1, 'day_rate'),
            ('Hourly Rate', 15, 1, 'hourly_rate'),
            ('First Day Access', 200, 1, 'firstday_machineaccess'),
            ('All Day Access', 150, 1, 'alldays_machineaccess');

            INSERT INTO `surface_areas` (`id`, `name`, `script`) VALUES
            (1, 'Concrete', ''),
            (2, 'Pebblecrete', ''),
            (3, 'Bitumen', ''),
            (4, 'Pavers', ''),
            (5, 'Grass / Garden', ''),
            (6, 'Grass - Concrete', ''),
            (7, 'Grass - Bitumen', ''),
            (8, 'Grass - Pebblecrete', ''),
            (9, 'Grass - Pavers', ''),
            (10, 'Concrete - Bitumen', ''),
            (11, 'Concrete - Pebblecrete', ''),
            (12, 'Concrete - Pavers', ''),
            (13, 'Bitumen - Pebblecrete', ''),
            (14, 'Bitumen - Pavers', '');

            TRUNCATE `surface_areas_materials`;
            INSERT INTO `surface_areas_materials` (`id`, `surface_id`, `material_id`, `quantity`) VALUES
            (2, 1, 1, 0),
            (3, 2, 1, 0),
            (4, 3, 1, 0),
            (5, 4, 1, 0),
            (6, 5, 1, 0),
            (7, 6, 1, 0),
            (8, 7, 1, 0),
            (9, 8, 1, 0),
            (10, 9, 1, 0),
            (11, 10, 1, 0),
            (12, 11, 1, 0),
            (13, 12, 1, 0),
            (14, 13, 1, 0),
            (15, 14, 1, 0),
            (16, 1, 2, 0),
            (17, 2, 2, 0),
            (18, 3, 2, 0),
            (19, 4, 2, 0),
            (20, 5, 2, 0),
            (21, 6, 2, 0),
            (22, 7, 2, 0),
            (23, 8, 2, 0),
            (24, 9, 2, 0),
            (25, 10, 2, 0),
            (26, 11, 2, 0),
            (27, 12, 2, 0),
            (28, 13, 2, 0),
            (29, 14, 2, 0),
            (30, 1, 3, 0),
            (31, 2, 3, 0),
            (32, 3, 3, 0),
            (33, 4, 3, 0),
            (34, 5, 3, 0),
            (35, 6, 3, 0),
            (36, 7, 3, 0),
            (37, 8, 3, 0),
            (38, 9, 3, 0),
            (39, 10, 3, 0),
            (40, 11, 3, 0),
            (41, 12, 3, 0),
            (42, 13, 3, 0),
            (43, 14, 3, 0),
            (44, 1, 4, 0),
            (45, 2, 4, 0),
            (46, 3, 4, 0),
            (47, 4, 4, 0),
            (48, 5, 4, 0),
            (49, 6, 4, 0),
            (50, 7, 4, 0),
            (51, 8, 4, 0),
            (52, 9, 4, 0),
            (53, 10, 4, 0),
            (54, 11, 4, 0),
            (55, 12, 4, 0),
            (56, 13, 4, 0),
            (57, 14, 4, 0),
            (58, 1, 5, 0),
            (59, 2, 5, 0),
            (60, 3, 5, 0),
            (61, 4, 5, 0),
            (62, 5, 5, 0),
            (63, 6, 5, 0),
            (64, 7, 5, 0),
            (65, 8, 5, 0),
            (66, 9, 5, 0),
            (67, 10, 5, 0),
            (68, 11, 5, 0),
            (69, 12, 5, 0),
            (70, 13, 5, 0),
            (71, 14, 5, 0),
            (72, 1, 6, 0),
            (73, 2, 6, 0),
            (74, 3, 6, 0),
            (75, 4, 6, 0),
            (76, 5, 6, 0),
            (77, 6, 6, 0),
            (78, 7, 6, 0),
            (79, 8, 6, 0),
            (80, 9, 6, 0),
            (81, 10, 6, 0),
            (82, 11, 6, 0),
            (83, 12, 6, 0),
            (84, 13, 6, 0),
            (85, 14, 6, 0),
            (86, 1, 7, 0),
            (87, 2, 7, 0),
            (88, 3, 7, 0),
            (89, 4, 7, 0),
            (90, 5, 7, 0),
            (91, 6, 7, 0),
            (92, 7, 7, 0),
            (93, 8, 7, 0),
            (94, 9, 7, 0),
            (95, 10, 7, 0),
            (96, 11, 7, 0),
            (97, 12, 7, 0),
            (98, 13, 7, 0),
            (99, 14, 7, 0),
            (100, 1, 8, 0),
            (101, 2, 8, 0),
            (102, 3, 8, 0),
            (103, 4, 8, 0),
            (104, 5, 8, 0),
            (105, 6, 8, 0),
            (106, 7, 8, 0),
            (107, 8, 8, 0),
            (108, 9, 8, 0),
            (109, 10, 8, 0),
            (110, 11, 8, 0),
            (111, 12, 8, 0),
            (112, 13, 8, 0),
            (113, 14, 8, 0),
            (114, 1, 9, 0),
            (115, 2, 9, 0),
            (116, 3, 9, 0),
            (117, 4, 9, 0),
            (118, 5, 9, 0),
            (119, 6, 9, 0),
            (120, 7, 9, 0),
            (121, 8, 9, 0),
            (122, 9, 9, 0),
            (123, 10, 9, 0),
            (124, 11, 9, 0),
            (125, 12, 9, 0),
            (126, 13, 9, 0),
            (127, 14, 9, 0),
            (128, 1, 10, 0),
            (129, 2, 10, 0),
            (130, 3, 10, 0),
            (131, 4, 10, 0),
            (132, 5, 10, 0),
            (133, 6, 10, 0),
            (134, 7, 10, 0),
            (135, 8, 10, 0),
            (136, 9, 10, 0),
            (137, 10, 10, 0),
            (138, 11, 10, 0),
            (139, 12, 10, 0),
            (140, 13, 10, 0),
            (141, 14, 10, 0),
            (142, 1, 11, 0),
            (143, 2, 11, 0),
            (144, 3, 11, 0),
            (145, 4, 11, 0),
            (146, 5, 11, 0),
            (147, 6, 11, 0),
            (148, 7, 11, 0),
            (149, 8, 11, 0),
            (150, 9, 11, 0),
            (151, 10, 11, 0),
            (152, 11, 11, 0),
            (153, 12, 11, 0),
            (154, 13, 11, 0),
            (155, 14, 11, 0),
            (156, 1, 12, 0),
            (157, 2, 12, 0),
            (158, 3, 12, 0),
            (159, 4, 12, 0),
            (160, 5, 12, 0),
            (161, 6, 12, 0),
            (162, 7, 12, 0),
            (163, 8, 12, 0),
            (164, 9, 12, 0),
            (165, 10, 12, 0),
            (166, 11, 12, 0),
            (167, 12, 12, 0),
            (168, 13, 12, 0),
            (169, 14, 12, 0),
            (170, 1, 13, 0),
            (171, 2, 13, 0),
            (172, 3, 13, 0),
            (173, 4, 13, 0),
            (174, 5, 13, 0),
            (175, 6, 13, 0),
            (176, 7, 13, 0),
            (177, 8, 13, 0),
            (178, 9, 13, 0),
            (179, 10, 13, 0),
            (180, 11, 13, 0),
            (181, 12, 13, 0),
            (182, 13, 13, 0),
            (183, 14, 13, 0),
            (184, 1, 14, 0),
            (185, 2, 14, 0),
            (186, 3, 14, 0),
            (187, 4, 14, 0),
            (188, 5, 14, 0),
            (189, 6, 14, 0),
            (190, 7, 14, 0),
            (191, 8, 14, 0),
            (192, 9, 14, 0),
            (193, 10, 14, 0),
            (194, 11, 14, 0),
            (195, 12, 14, 0),
            (196, 13, 14, 0),
            (197, 14, 14, 0),
            (198, 1, 15, 0),
            (199, 2, 15, 0),
            (200, 3, 15, 0),
            (201, 4, 15, 0),
            (202, 6, 15, 0),
            (203, 7, 15, 0),
            (204, 8, 15, 0),
            (205, 9, 15, 0),
            (206, 10, 15, 0),
            (207, 11, 15, 0),
            (208, 12, 15, 0),
            (209, 13, 15, 0),
            (210, 14, 15, 0),
            (211, 5, 16, 0),
            (212, 6, 16, 0),
            (213, 7, 16, 0),
            (214, 8, 16, 0),
            (215, 9, 16, 0),
            (216, 1, 18, 0),
            (217, 2, 18, 0),
            (218, 3, 18, 0),
            (219, 4, 18, 0),
            (220, 5, 18, 0),
            (221, 6, 18, 0),
            (222, 7, 18, 0),
            (223, 8, 18, 0),
            (224, 9, 18, 0),
            (225, 10, 18, 0),
            (226, 11, 18, 0),
            (227, 12, 18, 0),
            (228, 13, 18, 0),
            (229, 14, 18, 0),
            (230, 1, 19, 0),
            (231, 2, 19, 0),
            (232, 3, 19, 0),
            (233, 4, 19, 0),
            (234, 6, 19, 0),
            (235, 7, 19, 0),
            (236, 8, 19, 0),
            (237, 9, 19, 0),
            (238, 10, 19, 0),
            (239, 11, 19, 0),
            (240, 12, 19, 0),
            (241, 13, 19, 0),
            (242, 14, 19, 0),
            (243, 1, 20, 0),
            (244, 2, 20, 0),
            (245, 3, 20, 0),
            (246, 4, 20, 0),
            (247, 5, 20, 0),
            (248, 6, 20, 0),
            (249, 7, 20, 0),
            (250, 8, 20, 0),
            (251, 9, 20, 0),
            (252, 10, 20, 0),
            (253, 11, 20, 0),
            (254, 12, 20, 0),
            (255, 13, 20, 0),
            (256, 14, 20, 0),
            (257, 1, 21, 0),
            (258, 2, 21, 0),
            (259, 3, 21, 0),
            (260, 4, 21, 0),
            (261, 5, 21, 0),
            (262, 6, 21, 0),
            (263, 7, 21, 0),
            (264, 8, 21, 0),
            (265, 9, 21, 0),
            (266, 10, 21, 0),
            (267, 11, 21, 0),
            (268, 12, 21, 0),
            (269, 13, 21, 0),
            (270, 14, 21, 0),
            (271, 1, 22, 0),
            (272, 2, 22, 0),
            (273, 3, 22, 0),
            (274, 4, 22, 0),
            (275, 6, 22, 0),
            (276, 7, 22, 0),
            (277, 8, 22, 0),
            (278, 9, 22, 0),
            (279, 10, 22, 0),
            (280, 11, 22, 0),
            (281, 12, 22, 0),
            (282, 13, 22, 0),
            (283, 14, 22, 0),
            (284, 1, 23, 0),
            (285, 2, 23, 0),
            (286, 3, 23, 0),
            (287, 4, 23, 0),
            (288, 5, 23, 0),
            (289, 6, 23, 0),
            (290, 7, 23, 0),
            (291, 8, 23, 0),
            (292, 9, 23, 0),
            (293, 10, 23, 0),
            (294, 11, 23, 0),
            (295, 12, 23, 0),
            (296, 13, 23, 0),
            (297, 14, 23, 0),
            (298, 1, 24, 0),
            (299, 2, 24, 0),
            (300, 3, 24, 0),
            (301, 4, 24, 0),
            (302, 5, 24, 0),
            (303, 6, 24, 0),
            (304, 7, 24, 0),
            (305, 8, 24, 0),
            (306, 9, 24, 0),
            (307, 10, 24, 0),
            (308, 11, 24, 0),
            (309, 12, 24, 0),
            (310, 13, 24, 0),
            (311, 14, 24, 0),
            (312, 1, 25, 0),
            (313, 2, 25, 0),
            (314, 3, 25, 0),
            (315, 4, 25, 0),
            (316, 5, 25, 0),
            (317, 6, 25, 0),
            (318, 7, 25, 0),
            (319, 8, 25, 0),
            (320, 9, 25, 0),
            (321, 10, 25, 0),
            (322, 11, 25, 0),
            (323, 12, 25, 0),
            (324, 13, 25, 0),
            (325, 14, 25, 0),
            (326, 1, 26, 0),
            (327, 2, 26, 0),
            (328, 3, 26, 0),
            (329, 4, 26, 0),
            (330, 5, 26, 0),
            (331, 6, 26, 0),
            (332, 7, 26, 0),
            (333, 8, 26, 0),
            (334, 9, 26, 0),
            (335, 10, 26, 0),
            (336, 11, 26, 0),
            (337, 12, 26, 0),
            (338, 13, 26, 0),
            (339, 14, 26, 0),
            (340, 1, 27, 0),
            (341, 2, 27, 0),
            (342, 3, 27, 0),
            (343, 4, 27, 0),
            (344, 5, 27, 0),
            (345, 6, 27, 0),
            (346, 7, 27, 0),
            (347, 8, 27, 0),
            (348, 9, 27, 0),
            (349, 10, 27, 0),
            (350, 11, 27, 0),
            (351, 12, 27, 0),
            (352, 13, 27, 0),
            (353, 14, 27, 0),
            (354, 1, 28, 0),
            (355, 2, 28, 0),
            (356, 3, 28, 0),
            (357, 4, 28, 0),
            (358, 5, 28, 0),
            (359, 6, 28, 0),
            (360, 7, 28, 0),
            (361, 8, 28, 0),
            (362, 9, 28, 0),
            (363, 10, 28, 0),
            (364, 11, 28, 0),
            (365, 12, 28, 0),
            (366, 13, 28, 0),
            (367, 14, 28, 0),
            (368, 1, 29, 0),
            (369, 2, 29, 0),
            (370, 3, 29, 0),
            (371, 6, 29, 0),
            (372, 7, 29, 0),
            (373, 8, 29, 0),
            (374, 10, 29, 0),
            (375, 11, 29, 0),
            (376, 12, 29, 0),
            (377, 13, 29, 0),
            (378, 14, 29, 0),
            (379, 1, 30, 0),
            (380, 2, 30, 0),
            (381, 3, 30, 0),
            (382, 6, 30, 0),
            (383, 7, 30, 0),
            (384, 8, 30, 0),
            (385, 10, 30, 0),
            (386, 11, 30, 0),
            (387, 12, 30, 0),
            (388, 13, 30, 0),
            (389, 14, 30, 0),
            (390, 1, 31, 0),
            (391, 2, 31, 0),
            (392, 3, 31, 0),
            (393, 6, 31, 0),
            (394, 7, 31, 0),
            (395, 8, 31, 0),
            (396, 10, 31, 0),
            (397, 11, 31, 0),
            (398, 12, 31, 0),
            (399, 13, 31, 0),
            (400, 14, 31, 0),
            (401, 1, 43, 0),
            (402, 2, 43, 0),
            (403, 6, 43, 0),
            (404, 8, 43, 0),
            (405, 10, 43, 0),
            (406, 11, 43, 0),
            (407, 12, 43, 0),
            (408, 13, 43, 0),
            (409, 1, 36, 0),
            (410, 2, 36, 0),
            (411, 6, 36, 0),
            (412, 8, 36, 0),
            (413, 10, 36, 0),
            (414, 11, 36, 0),
            (415, 12, 36, 0),
            (416, 13, 36, 0),
            (417, 1, 42, 0),
            (418, 2, 42, 0),
            (419, 3, 42, 0),
            (420, 4, 42, 0),
            (421, 5, 42, 0),
            (422, 6, 42, 0),
            (423, 7, 42, 0),
            (424, 8, 42, 0),
            (425, 9, 42, 0),
            (426, 10, 42, 0),
            (427, 11, 42, 0),
            (428, 12, 42, 0),
            (429, 13, 42, 0),
            (430, 14, 42, 0);
            DELETE FROM `surface_areas_materials` WHERE `material_id` = 1;
            UPDATE materials SET onehundredmm = FLOOR(RAND() * 201) + 100, `org_id` = 1;
            UPDATE surface_areas SET `org_id` = 1;
            UPDATE surface_areas_materials SET quantity = FLOOR(RAND() * 50) + 1;");
            echo "Seeded Initial material data<br/ >";
    }

    public function seedOrganisation()
    {
        $this->db->query('INSERT INTO `organisations` (`title`, `logo`) VALUES ("Organisation 1", "Organisation Logo")');
        echo "Seeded Initial organisation data<br/ >";
    }

    public function seedUsers()
    {
        $password = password_hash('password', 1);
        $this->db->query('INSERT INTO `users` VALUES (null, "Admin", 1, "admin@yahoo.com", "'.$password.'", "Admin")');
        $this->db->query('INSERT INTO `users` VALUES (null, "Ops", 1, "ops@yahoo.com", "'.$password.'", "Ops")');
        $this->db->query('INSERT INTO `users` VALUES (null, "SuperAdmin", 1, "suadmin@yahoo.com", "'.$password.'", "SuperAdmin")');
        $this->db->query('INSERT INTO `users` VALUES (null, "Plumber", 1, "plumber@yahoo.com", "'.$password.'", "Plumber")');
        echo "Seeded Initial users data<br/ >";
    }

    public function seedScript()
    {
        $this->db->query('UPDATE `surface_areas` SET `script` = CONCAT(FLOOR(RAND() * 50) + 1 ,"Lorem |refNumber| ipsum |address| dolor |category| sit |problem| amet |pipeSize|, consectetur |excavation| adipiscing |surface| elit. In |location| eu |timeline| feugiat |timeQuantity| mi. |machineAccess| Aliquam |machineDays|.")');
        echo "Seeded Initial script data<br/ >";
    }

    public function updatePvcKey()
    {
        $this->db->query('UPDATE `materials` SET `key` = "pvc" WHERE `name` = "PVC Pipe per metre"');
        echo "Updated PVC key<br/ >";
    }

    public function updateMaterials()
    {
        $this->db->update("materials", ["key"=>"pvc"], ["id"=>1]);
        $this->db->update("materials", ["key"=>"steelreo", "name"=>"Steel/Reo Bars"], ["id"=>29]);
        $this->db->update("materials", ["key"=>"gravel"], ["id"=>15]);
        $this->db->update("materials", ["key"=>"topsoil"], ["id"=>16]);
        $this->db->update("materials", ["key"=>"turf"], ["id"=>17]);
        echo "Updated Key of PVC, Steel, Gravel, Top Soil and Turf to Organisation 1 Materials<br />";
        $this->removeLinkOfSteelReo();
    }

    public function removeLinkOfSteelReo()
    {
        $this->db->query('DELETE FROM surface_areas_materials WHERE id IN (SELECT id FROM (select id from surface_areas_materials where material_id IN (29, 31)) X)');
        echo "Deleted Steel and Reo surface area link<br/ >";
    }

    public function seedPipePrices()
    {
        $this->db->query('UPDATE materials SET `onehundredmm` = FLOOR(RAND() * 201) + 100, `onefiftymm` = FLOOR(RAND() * 201) + 150, `twotwentyfivemm` = FLOOR(RAND() * 201) + 225, `threehundredmm` = FLOOR(RAND() * 201) + 300');
        echo "Seed Pipe Prices<br/ >";
    }

    public function updateSomeMaterialTypesToFour()
    {
        $this->db->update("materials", ["type"=>4], "id IN (44,45,46,47)");
        echo "Updated Some materials to type 4<br />";
    }

    public function deleteGravelInStaticMaterials()
    {
        $this->db->query('DELETE FROM surface_areas_materials WHERE material_id = 15');
        echo 'Deleted gravel in static materials<br />';
    }

    public function truncateQuotes()
    {
        $this->db->query('TRUNCATE `quotes`');
        echo 'Truncated quotes<br />';
    }

    public function runDBChangesForMaterialCategories()
    {
        $this->updateSomeMaterialTypesToFour();
        $this->deleteGravelInStaticMaterials();
        $this->truncateQuotes();
    }

    public function setRubbish()
    {
        $this->db->query('UPDATE `materials` SET `key` = "rubbish" WHERE `id` = 18');
        $this->db->query('DELETE FROM `surface_areas_materials` WHERE `material_id` = 18');
        echo 'Set Rubbish material key. Removed from surface areas materials table<br />';
    }

    public function removeCouncilAndRoadPlates()
    {
        $this->db->query('UPDATE `materials` SET `org_id` = NULL WHERE `id` IN (37, 38)');
        echo 'Remove Council and Road Plates<br />';
    }

    public function addServiceCall()
    {
        $data = $this->db->select("*")
                         ->from("materials")
                         ->where(["id"=>48])
                         ->get();
        if ($data->num_rows() === 0) {
            $this->db->query("INSERT INTO `materials` (`name`, `onehundredmm`, `type`, `key`, `org_id`, `onefiftymm`, `twotwentyfivemm`, `threehundredmm`) 
                  VALUES ('Service Call', 100, 1, null, 1, 150, 225, 300)");
            for ($i=1; $i <= 14; $i++) {
                $this->db->query("INSERT INTO `surface_areas_materials` (`surface_id`, `material_id`, `quantity`) VALUES (".$i.", 48, 1)");
            }
            echo "Added service call";
        } else {
            echo "Already added service call";
        }
    }

    public function removeExcavator()
    {
        $result = $this->db->select("*")
                         ->from("materials")
                         ->where(["name"=>"Excavator"])
                         ->get();
        $data = $result->result_object();
        if ($result->num_rows() > 0) {
            echo "Excavator material found" . PHP_EOL;
            echo "Removing excavator ..." . PHP_EOL;
            foreach ($data as $key => $value) {
                $this->db->delete('materials', array('id' => $value->id));
                $this->db->delete('surface_areas_materials', array('material_id' => $value->id));
                echo "Removed " . $value->name . " with ID " . $value->id . PHP_EOL;
            }
        } else {
            echo "No Excavator material found";
        }
    }

    public function addRoadPlates()
    {
        $result = $this->db->select("*")
                         ->from("materials")
                         ->where(["name"=>"Road Plates"])
                         ->get();
        $data = $result->result_object();
        if ($result->num_rows() > 0) {
            echo "Road Plates already exist";
        } else {
            echo "Adding Road Plates..." . PHP_EOL;
            $data = [
                "name"   => "Road Plates",
                "type"   => "1",
                "org_id" => "1",
                "key"    => "road_plates",
                "onehundredmm"    => 500.00,
                "onefiftymm"      => 500.00,
                "twotwentyfivemm" => 500.00,
                "threehundredmm"  => 500.00,
            ];
            $this->db->insert("materials", $data);
        }
    }

    public function updateMaterialsFromFeedbackThree()
    {
        $result = $this->db->select("*")
                         ->from("materials")
                         ->where(["name"=>"Concrete Taxi"])
                         ->get();
        $data = $result->result_object();
        if ($result->num_rows() > 0) {
            $this->db->delete('materials', array('id' => $data[0]->id));
            echo "Deleted Concrete Taxi material <br/>";
        } else {
            echo "Concrete Taxi doesnt exist! <br/>";
        }

        $result = $this->db->select("*")
                         ->from("materials")
                         ->where(["name"=>"Concrete BTS"])
                         ->get();
        $data = $result->result_object();
        if ($result->num_rows() > 0) {
            $this->db->update("materials", ["key"=>"concrete"], ["id"=>$data[0]->id]);
            echo "Updated Concrete! <br/>";
            $this->db->delete('surface_areas_materials', array('material_id' => $data[0]->id));
        } else {
            echo "Concrete BTS doesnt exist! <br/>";
        }

        // Add Peebles | Bitumen
        $result = $this->db->select("*")
                         ->from("materials")
                         ->where(["name"=>"Pebbles"])
                         ->get();
        $data = $result->result_object();
        if ($result->num_rows() > 0) {
            echo "Pebbles and Bitumen already exist! <br/>";
        } else {
            $this->db->query("INSERT INTO `materials` (`name`, `onehundredmm`, `type`, `key`, `org_id`, `onefiftymm`, `twotwentyfivemm`, `threehundredmm`) 
                  VALUES ('Pebbles', 100, 1, 'pebbles', 1, 150, 225, 300)");
            $this->db->query("INSERT INTO `materials` (`name`, `onehundredmm`, `type`, `key`, `org_id`, `onefiftymm`, `twotwentyfivemm`, `threehundredmm`) 
                  VALUES ('Bitumen', 100, 1, 'bitumen', 1, 150, 225, 300)");
            echo "Added Pebbles and Bitumen! <br/>";
        }
    }

    private function _test()
    {
        $result = $this->db->select("*")
                         ->from("materials")
                         ->where(["org_id"=>"1"])
                         ->get();
        $data = $result->result_array();
        echo '<pre>';
        foreach ($data as $key => $value) {
            unset($data[$key]['id']);
            unset($data[$key]['org_id']);
        }
        print_r($data);
        $this->db->insert_batch('template', $data);
    }

    public function addGrassGarden()
    {
        $script = "We have recently been asked to attend site to investigate an issue with the drainage. Along with a CCTV camera inspection we have identified that there was a problem with the|sewerOrStorm| .It was obvious that the drainage has|problem| within the line and you will not be able to maintain this line for much longer. This should be excavated and repaired as it will continue to back up in the future. There is approximately|lengthOfEx| metres of drainage that we can clearly identify requires replacement. We note the affected section of the drainage to be under two surfaces; grass and garden area, located in the |location|. We will run the CCTV camera again at no charge to clearly identify any further issues within the line, if necessary.



We will carry out the following:



Repair in Garden Area

* Spot dig the affected area.

* Cut the section of  out and replace with new PVC pipework.

* Raise an inspection to ground level and finish with concrete.

* Back fill and consolidate ground making good the garden area.



Repair in Grass

* Spot dig the affected area.

* Cut the section of out and replace with new PVC pipework.

* Raise an inspection to ground level and finish with concrete.

* Back fill and consolidate ground making good the turf area.

* Remove any rubbish from site.

All works will be carried out to Australian Standards AS3500 and will be within OH&S Guidelines.



Please note:

No allowances have been made for any damages to underground services however; every care will be taken whilst on site.";

        $result = $this->db->query("SELECT * FROM `template_surface_area` WHERE `name` IN ('Grass', 'Garden')");
        echo '<pre>';
        if ($result->num_rows() == 0) {
            $data = [
                [
                    "name"   => "Grass",
                    "script" => $script,
                ],
                [
                    "name"   => "Garden",
                    "script" => $script,
                ],
            ];
            if ($this->db->insert_batch('template_surface_area', $data)) {
                echo "Successfully added Grass and Garden to template_surface_area<br />";
            }
            $result = $this->db->query("SELECT `id` FROM `organisations`");
            foreach ($result->result_object() as $key => $value) {
                $resultx = $this->db->query("SELECT `name` FROM `surface_areas` WHERE `org_id` = ".$value->id." AND `name` IN ('Grass', 'Garden')");
                $orgId = $value->id;
                if ($resultx->num_rows() == 0) {
                    $data = [
                        [
                            "name"   => "Grass",
                            "script" => $script,
                            "org_id" => $value->id,
                        ],
                        [
                            "name"   => "Garden",
                            "script" => $script,
                            "org_id" => $value->id,
                        ],
                    ];
                    if ($this->db->insert_batch('surface_areas', $data)) {
                        echo "Successfully added Grass and Garden to surface_areas for Org Id ".$value->id." <br />";
                        
                    }
                }
                $materialAndQty = $this->db->select('A.id, B.quantity')
                                ->from('materials A')
                                ->join('template_surface_area_materials  B', 'A.name = B.name AND A.org_id = ' . $orgId, 'inner')
                                ->get();
                $surface = $this->db->select('id')
                                ->from('surface_areas')
                                ->where('org_id = ' . $orgId . " AND `name` IN ('Grass', 'Garden')")
                                ->get();

                $surfaceResult = $surface->result_array();
                $materialAndQtyResult = $materialAndQty->result_array();
                $data = [];

                if ($surface->num_rows() > 0 && $materialAndQty->num_rows() > 0) {
                    foreach ($surfaceResult as $key => $value) {
                        foreach ($materialAndQtyResult as $keyx => $valuex) {
                            $data[] = [
                                'surface_id'    => $value['id'],
                                'material_id'   => $valuex['id'],
                                'quantity'      => $valuex['quantity'],
                            ];
                        }
                    }
                    if ($this->db->insert_batch("surface_areas_materials", $data)) {
                        echo "Successfully added Grass and Garden materials to surface_areas_materials<br />";
                    }
                }
            }

        } else {
            echo "Grass and Garden already exist";
        }
    }
}
