<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'Base.php';

/**
 * Admin class
 *
 * Class for Admin
 */
class Admin extends Base
{
    /**
     * [__construct description]
     */
    public function __construct()
    {
        parent::__construct();

        if ($this->session->role !== 'Admin') {
            $this->session->set_flashdata('error', ERR_NOT_ALLOWED);
            redirect(base_url());
        }

        $this->data['data']['title']    = 'Admin';
        $this->data['data']['css']      = 'admin';
        $this->data['data']['module']   = 'admin';
    }

    /**
     * [index description]
     *
     * @return void
     */
    public function index()
    {
        $this->data['data']['content'] = 'admin/index';
        $this->data['data']['leftnav'] = 'index';

        $this->getQuotes('B.won IS NULL AND B.org_id = ' . $this->session->orgId);
    }

    /**
     * [viewQuote description]
     *
     * @param  int $id Quote Id
     *
     * @return void
     */
    public function viewQuote($id = null, $pdf = false)
    {
        $this->load->library('user_agent');
        if (strpos($this->agent->referrer(), 'index') !== false) {
            $leftnav = 'index';
        } else {
            $leftnav = 'archived';
        }

        $this->data['data']['content'] = 'admin/viewquote';
        $this->data['data']['leftnav'] = $leftnav;
        $this->data['data']['js'] = 'common/viewquote-js';

        $this->load->model('quotematerials');
        $this->data['data']['quotematerials'] = $this->quotematerials->getQuoteMaterialsJoinQuotes(['quote_id' => $id, 'org_id' => $this->session->orgId]);

        if ($this->data['data']['quotematerials']->num_rows() === 0) {
            $this->session->set_flashdata('error', 'Invalid Quote Id');
            redirect(base_url().'admin/index');
        }

        if ($pdf) {
            return $this->load->view('common/index', $this->data, $pdf);
        } else {
            $this->load->view('common/index', $this->data);
        }
    }

    /**
     * [editQuote description]
     *
     * @param  int $id Quote Id
     * @param  int $won Won Status
     *
     * @return void
     */
    public function editQuote($id = null, $won = null)
    {
        if ($won == 1) {
            $leftnav = 'archived';
        } else {
            $leftnav = 'index';
        }

        $this->data['data']['content'] = 'admin/editquote';
        $this->data['data']['leftnav'] = $leftnav;
        $this->data['data']['js']      = 'common/editquote-js';

        $this->data['data']['cssArray'][] = "style.css";
        $this->data['data']['cssArray'][] = "jquery.fileupload.css";
        $this->data['data']['cssArray'][] = "jquery.fileupload-ui.css";

        // $this->data['data']['jsArray'][]  = "vendor/jquery.ui.widget.js";
        $this->data['data']['jsArray'][]  = "jquery.iframe-transport.js";
        $this->data['data']['jsArray'][]  = "jquery.fileupload.js";
        $this->data['data']['jsArray'][]  = "jquery.fileupload-process.js";
        $this->data['data']['jsArray'][]  = "jquery.fileupload-image.js";
        // $this->data['data']['jsArray'][]  = "jquery.fileupload-audio.js";
        // $this->data['data']['jsArray'][]  = "jquery.fileupload-video.js";
        $this->data['data']['jsArray'][]  = "jquery.fileupload-validate.js";
        $this->data['data']['jsArray'][]  = "jquery.fileupload-ui.js";

        $this->data['data']['blueimp'][]  = base_url() . "assets/js/vendor/jquery.ui.widget.js";
        $this->data['data']['blueimp'][]  = "//blueimp.github.io/JavaScript-Templates/js/tmpl.min.js";
        $this->data['data']['blueimp'][]  = "//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js";
        $this->data['data']['blueimp'][]  = "//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js";
 
        // $this->data['data']['jsArray'][]  = "main.js";

        $this->load->model('quotematerials');
        $this->data['data']['quotematerials'] = $this->quotematerials->getQuoteMaterialsJoinQuotes(['quote_id' => $id, 'org_id' => $this->session->orgId]);

        if ($this->data['data']['quotematerials']->num_rows() === 0) {
            $this->session->set_flashdata('error', 'Invalid Quote Id');
            redirect(base_url().'admin/index');
        }

        $this->load->view('common/index', $this->data);
    }

    /**
     * [configuration description]
     *
     * @return void
     */
    public function configuration()
    {
        $this->data['data']['content'] = 'admin/configuration';
        $this->data['data']['leftnav'] = 'configuration';

        $this->load->model('materials');
        $this->load->model('surface');
        
        $this->data['data']['materials'] = $this->materials->getWhere(['org_id' => $this->session->orgId]);
        $this->data['data']['static']    = $this->materials->getAllStaticMaterials(['materials.org_id' => $this->session->orgId]);
        $this->data['data']['surface']   = $this->surface->getWhere(['org_id' => $this->session->orgId]);

        $this->load->view('common/index', $this->data);
    }

    /**
     * [users description]
     *
     * @return void
     */
    public function users()
    {
        $this->data['data']['content'] = 'admin/users';
        $this->data['data']['leftnav'] = 'users';

        $this->load->model('users');
        $this->data['data']['users']   = $this->users->getWhere(['org_id' => $this->session->orgId]);

        $this->load->view('common/index', $this->data);
    }

    public function editUser($id)
    {
        $this->data['data']['content'] = 'admin/edituser';
        $this->data['data']['leftnav'] = 'users';

        $this->load->model('users');
        $result = $this->users->getWhere(['id'=>$id, 'org_id' => $this->session->orgId]);

        if ($result->num_rows() === 0) {
            $this->session->set_flashdata('error', 'Invalid User Id!');
            redirect(base_url().'admin/users');
        }
        $this->data['data']['users'] = $result->result_object()[0];
        $this->load->view('common/index', $this->data);
    }

    public function updateUser()
    {
        if ($this->input->post()) {
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('role', 'Role', 'required');
            $this->form_validation->set_rules('id', 'ID', 'required');
            if ($this->form_validation->run() === false) {
                $this->session->set_flashdata('error', validation_errors());
            } else {
                $data = [
                    "full_name" => $this->input->post('name'),
                    "email"     => $this->input->post('email'),
                    "password"  => password_hash($this->input->post('password'), 1),
                    "role"      => $this->input->post('role'),
                ];

                $this->load->model('users');
                $checkEmail = $this->getDuplicateEmail();
                if ($checkEmail->num_rows() == 1) {
                    if ($checkEmail->result_object()[0]->id == $this->input->post('id')) {
                        if ($this->users->put($data, ['id'=>$this->input->post('id'), 'org_id' => $this->session->orgId])) {
                            $this->session->set_flashdata('success', 'User updated!');
                        } else {
                            $this->session->set_flashdata('error', 'Error saving to database');
                        }
                    } else {
                        $this->session->set_flashdata('error', 'Duplicate Email Address');
                    }
                } else {
                    if ($this->users->put($data, ['id'=>$this->input->post('id'), 'org_id' => $this->session->orgId])) {
                        $this->session->set_flashdata('success', 'User updated!');
                    } else {
                        $this->session->set_flashdata('error', 'Error saving to database');
                    }
                }
            }
        } else {
            $this->session->set_flashdata('error', 'Invalid Request');
        }
        redirect(base_url().'admin/users');
    }

    public function addUser()
    {
        if ($this->input->post()) {
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('role', 'Role', 'required');
            if ($this->form_validation->run() === false) {
                $this->session->set_flashdata('error', validation_errors());
            } else {
                $data = [
                    "full_name" => $this->input->post('name'),
                    "email"     => $this->input->post('email'),
                    "password"  => password_hash($this->input->post('password'), 1),
                    "role"      => $this->input->post('role'),
                    "org_id"    => $this->session->orgId,
                ];

                $this->load->model('users');
                if ($this->getDuplicateEmail()->num_rows() > 0) {
                    $this->session->set_flashdata('error', 'Duplicate Email Address');
                } else {
                    if ($this->users->post($data)) {
                        $this->session->set_flashdata('success', 'User saved!');
                    } else {
                        $this->session->set_flashdata('error', 'Error saving to database');
                    }
                }
            }
        } else {
            $this->session->set_flashdata('error', 'Invalid Request');
        }
        redirect(base_url().'admin/users');
    }

    public function delUser()
    {
        if ($this->input->post()) {
            $this->load->model('users');
            if ($this->users->delete(['id'=>$this->input->post('id'), 'org_id' => $this->session->orgId])) {
                $this->session->set_flashdata('success', 'User Deleted!');
            } else {
                $this->session->set_flashdata('error', 'Database error');
            }
        } else {
            $this->session->set_flashdata('error', 'Invalid Request');
        }
        redirect(base_url().'admin/users');
    }

    /**
     * [newJob description]
     *
     * @return void
     */
    public function newJob()
    {
        $this->data['data']['leftnav'] = 'newQuote';

        $this->baseNewJob(['content' => 'admin/newquote', 'css' => 'admin']);
    }

    /**
     * [newJobNext description]
     *
     * @return void
     */
    public function newJobNext()
    {
        $this->data['data']['leftnav'] = 'newQuote';

        $this->baseNewJobNext(['content' => 'admin/newquotenext', 'css' => 'admin']);
    }

    /**
     * [newJobValidate description]
     *
     * @return void
     */
    public function newJobValidate()
    {
        $this->data['data']['leftnav'] = 'newQuote';

        $this->baseNewJobValidate(['content' => 'admin/newquotevalidate', 'css' => 'admin']);
    }

    /**
     * [postQuote description]
     *
     * @return void
     */
    public function postQuote()
    {
        $this->basePostQuote();
    }

    /**
     * [quoteWon description]
     *
     * @return void
     */
    public function quoteWon()
    {
        $this->baseQuoteWon();
    }

    /**
     * [archived description]
     *
     * @return void
     */
    public function archived()
    {
        $this->data['data']['content'] = 'admin/archived';
        $this->data['data']['leftnav'] = 'archived';

        $this->getQuotes('B.won = 1 AND B.org_id = ' . $this->session->orgId);
    }

    public function generatePDF($quoteId, $email = false)
    {
        $this->baseGeneratePdf($quoteId, $email);
    }

    public function logoff()
    {
        $session = [
            'orgId'     => $this->session->origOrgId,
            'role'      => $this->session->origRole,
        ];
        $this->session->set_userdata($session);

        $unset = ['origOrgId', 'origRole'];
        $this->session->unset_userdata($unset);

        redirect(base_url() . 'superadmin');
    }

    public function todo($id)
    {
        $this->data['data']['content'] = 'admin/todo';
        $this->data['data']['leftnav'] = 'configuration';
        $this->data['data']['js']      = 'admin/todo-js';

        $this->load->model('surface');
        $this->data['data']['todo'] = $this->surface->joinTodo(['A.id'=>$id]);

        $this->load->view('common/index', $this->data);
    }

    public function addTodo()
    {
        if ($this->input->post()) {

            $this->load->model('surfacetodo');
            $surfaceId = $this->input->post('id');
            $batch = [];

            foreach ($this->input->post('todo') as $key => $value) {
                $batch[] = [
                    'surface_id' => $surfaceId,
                    'todo' => $value,
                ];
            }
            foreach ($this->input->post('todonew') as $key => $value) {
                $batch[] = [
                    'surface_id' => $surfaceId,
                    'todo' => $value,
                ];
            }

            $this->surfacetodo->delete(['surface_id' => $surfaceId]);

            if ($this->surfacetodo->insertBatch($batch)) {
                $this->session->set_flashdata('success', 'Added To Do List!');
            } else {
                $this->session->set_flashdata('error', 'Database error');
            }
        } else {
            $this->session->set_flashdata('error', 'Invalid Request');
        }
        redirect(base_url().'admin/todo/'.$surfaceId);
    }

    public function deleteTodo($id, $surfaceId)
    {
        $this->load->model('surfacetodo');
        if ($this->surfacetodo->delete(['id' => $id])) {
            $this->session->set_flashdata('success', 'To Do List Deleted!');
        } else {
            $this->session->set_flashdata('error', 'Database error');
        }
        redirect(base_url().'admin/todo/'.$surfaceId);
    }
}
