<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once 'Base.php';

/**
 * Ajax class
 *
 * Class for Ajax
 */
class Ajax extends Base
{
    /**
     * [__construct description]
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function updateMaterial()
    {
        if ($this->input->post()) {
            $this->load->model('quotematerials');
            $newTotalCost = computeCost($this->input->post('unitPrice'), $this->input->post('quantity'));
            $data = [
                'quantity'      => $this->input->post('quantity'),
                'total_price'   => $newTotalCost
            ];

            $where = ['id' => $this->input->post('materialId')];

            if ($this->quotematerials->put($data, $where)) {
                $newQuote = $this->quotematerials->getCurrentQuotes(['quote_id'=>$this->input->post('quoteId'), 'B.org_id' => $this->session->orgId]);
                foreach ($newQuote->result_object() as $key => $value) {
                    $total = $value->total;
                }

                $this->load->model('quotes');
                $this->quotes->put(['sub_cost' => $total], ['id' => $this->input->post('quoteId'), 'org_id' => $this->session->orgId]);

                $result = [
                    'success'      => 'Success',
                    'newTotalCost' => number_format($newTotalCost, 2),
                    'subCost'      => $total,

                ];
            } else {
                $result = ['error'=>'Error Saving to DB'];
            }
        } else {
            $result = ['error'=>'Invalid Request'];
        }
        $this->output
             ->set_content_type('application/json')
             ->set_output(json_encode($result));
    }

    public function updateScript()
    {
        if ($this->input->post()) {
            $this->load->model('quotes');
            $data = ['script' => $this->input->post('script')];
            $where = ['id' => $this->input->post('quoteId'), 'org_id' => $this->session->orgId];
            if ($this->quotes->put($data, $where)) {
                $result = [
                    'success'      => 'Success',
                ];
            } else {
                $result = ['error'=>'Error Saving to DB'];
            }
        } else {
            $result = ['error'=>'Invalid Request'];
        }
        $this->output
             ->set_content_type('application/json')
             ->set_output(json_encode($result));
    }

    public function updateMargin()
    {
        if ($this->input->post()) {
            $this->load->model('quotes');
            $data = ['margin' => $this->input->post('margin')];
            $where = ['id' => $this->input->post('quoteId'), 'org_id' => $this->session->orgId];
            if ($this->quotes->put($data, $where)) {
                $result = [
                    'success'      => 'Success',
                ];
            } else {
                $result = ['error'=>'Error Saving to DB'];
            }
        } else {
            $result = ['error'=>'Invalid Request'];
        }
        $this->output
             ->set_content_type('application/json')
             ->set_output(json_encode($result));
    }

    public function updateExtra()
    {
        if ($this->input->post()) {
            $this->load->model('quotes');
            $data = ['extra_cost' => $this->input->post('extra')];
            $where = ['id' => $this->input->post('quoteId'), 'org_id' => $this->session->orgId];
            if ($this->quotes->put($data, $where)) {
                $result = [
                    'success'      => 'Success',
                ];
            } else {
                $result = ['error'=>'Error Saving to DB'];
            }
        } else {
            $result = ['error'=>'Invalid Request'];
        }
        $this->output
             ->set_content_type('application/json')
             ->set_output(json_encode($result));
    }

    public function updateImage($quoteId)
    {
        if (! empty($_FILES)) {
            $result = $this->doUpload($quoteId);
            if (! empty($result['upload_data'])) {
                $result = ['success'=>'Image Upload Success'];
            } else {
                $result = ['error'=>'Unable to upload file'];
            }
        } else {
            $result = ['error'=>'Invalid Request'];
        }
        $this->output
             ->set_content_type('application/json')
             ->set_output(json_encode($result));
    }

    public function updateMaterialConfig()
    {
        if ($this->input->post() && ($this->session->role == 'Admin' || $this->session->role == 'SuperAdmin')) {

            $this->load->model('materials');
            $data = [
                'name'          => $this->input->post('name'),
                'onehundredmm'  => $this->input->post('onehundred'),
                'onefiftymm'    => $this->input->post('onefifty'),
                'twotwentyfivemm' => $this->input->post('twotwentyfive'),
                'threehundredmm'  => $this->input->post('threehundred'),
            ];
            $where = ['id' => $this->input->post('id'), 'org_id' => $this->session->orgId];
            if ($this->materials->put($data, $where)) {
                $result = ['success'=>'Material updated'];
            } else {
                $result = ['error'=>'Unable to update'];
            }

        } else {
            $result = ['error'=>'Invalid Request'];
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
    }

    public function updateStaticQtyConfig()
    {
        if ($this->input->post() && ($this->session->role == 'Admin' || $this->session->role == 'SuperAdmin')) {

            $this->load->model('surfaceareasmaterials');
            $data = [
                'quantity' => $this->input->post('qty'),
            ];
            $where = ['id' => $this->input->post('id')];
            if ($this->surfaceareasmaterials->put($data, $where)) {
                $result = ['success'=>'Static Qty updated'];
            } else {
                $result = ['error'=>'Unable to update'];
            }

        } else {
            $result = ['error'=>'Invalid Request'];
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
    }

    public function updateSurfaceConfig()
    {
        if ($this->input->post() && ($this->session->role == 'Admin' || $this->session->role == 'SuperAdmin')) {

            $this->load->model('surface');
            $data = [
                'name'   => $this->input->post('name'),
                'script' => $this->input->post('script'),
            ];
            $where = ['id' => $this->input->post('id'), 'org_id' => $this->session->orgId];

            if ($this->surface->put($data, $where)) {
                $result = ['success'=>'Surface updated'];
            } else {
                $result = ['error'=>'Unable to update'];
            }

        } else {
            $result = ['error'=>'Invalid Request'];
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
    }

    public function checkUploads()
    {
        $data = $this->session->userdata('jqueryuploader');
        $result = ['error' => 'No Uploads'];
        if (is_array($data) && count($data) > 0) {
            $result = ['success' => 'Uploaded'];
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
    }

    public function getImages($id)
    {
        $result = [];
        foreach (glob('uploads/attachment-'.$id.'-*') as $file) {
            $result[] = (object) [
                        "name" => $file,
                        "size" => filesize($file),
                        "type" => "image/jpeg",
                        "url"  => base_url() . $file,
                        "deleteUrl" => base_url() . "base/uploader?file=" . explode("/", $file)[1],
                        "thumbnailUrl" => base_url() . str_replace("uploads/", "uploads/thumbnail/", $file),
                        "deleteType" => "DELETE",
                    ];
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($result));
    }
}
