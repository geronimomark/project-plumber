<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'Base.php';

/**
 * SuperAdmin class
 *
 * Class for SuperAdmin
 */
class SuperAdmin extends Base
{
    /**
     * [__construct description]
     */
    public function __construct()
    {
        parent::__construct();

        if ($this->session->role !== 'SuperAdmin') {
            $this->session->set_flashdata('error', ERR_NOT_ALLOWED);
            redirect(base_url());
        }

        $this->data['data']['title'] = 'Super Admin';
        $this->data['data']['css']   = 'admin';
        $this->data['data']['module'] = 'superadmin';
    }

    /**
     * [index description]
     *
     * @return void
     */
    public function index()
    {
        $this->data['data']['content'] = 'superadmin/index';

        $this->load->model('organisations');
        $this->data['data']['organisations'] = $this->organisations->getOrgsWithContact();

        $this->load->view('common/index', $this->data);
    }

    /**
     * [addCompany description]
     *
     * @return void
     */
    public function addCompany()
    {
        $this->data['data']['content'] = 'superadmin/addcompany';

        $this->load->view('common/index', $this->data);
    }

    public function saveCompany()
    {
        if ($this->input->post()) {
            $this->form_validation->set_rules('companyname', 'Company', 'required');
            $this->form_validation->set_rules('name', 'Contact Person', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('number', 'Contact Number', 'required');
            $this->form_validation->set_rules('address', 'Address', 'required');
            if ($this->form_validation->run() === false) {
                $this->session->set_flashdata('error', validation_errors());
            } else {
                $data = [
                    "title"             => $this->input->post('companyname'),
                    "logo"              => '',
                    "address"           => $this->input->post('address'),
                    "contact_number"    => $this->input->post('number'),
                ];

                $this->load->model('organisations');

                $this->db->trans_begin();
                $orgId = $this->organisations->insertAndGetId($data);
                if (is_numeric($orgId) && $orgId > 0) {
                    $data = [
                        "full_name" => $this->input->post('name'),
                        "email"     => $this->input->post('email'),
                        "password"  => password_hash($this->input->post('password'), 1),
                        "role"      => 'Admin',
                        "org_id"    => $orgId,
                    ];

                    $this->load->model('users');
                    if ($this->getDuplicateEmail()->num_rows() > 0) {
                        $this->session->set_flashdata('error', 'Duplicate Email Address');
                        $this->db->trans_rollback();
                    } else {

                        $users = $this->users->post($data);

                        $this->load->model('materials');
                        $materials = $this->materials->generateOrganisationMaterials($orgId);

                        $this->load->model('surface');
                        $surface = $this->surface->generateOrganisationSurfaceArea($orgId);

                        $this->load->model('surfaceareasmaterials');
                        $surfaceareasmaterials = $this->surfaceareasmaterials->generateOrganisationSurfaceAreaMaterials($orgId);

                        if ($users && $materials && $surface && $surfaceareasmaterials) {
                            $this->session->set_flashdata('success', 'Company saved!');
                            $this->db->trans_commit();
                        } else {
                            $this->session->set_flashdata('error', 'Error saving to database');
                            $this->db->trans_rollback();
                        }
                    }
                } else {
                    $this->session->set_flashdata('error', 'Error saving to database');
                    $this->db->trans_rollback();
                }
            }
        } else {
            $this->session->set_flashdata('error', 'Invalid Request');
        }
        redirect(base_url().'superadmin/addCompany');
    }

    public function updateCompany()
    {
        if ($this->input->post()) {
            $this->form_validation->set_rules('companyname', 'Company', 'required');
            $this->form_validation->set_rules('number', 'Contact Number', 'required');
            $this->form_validation->set_rules('address', 'Address', 'required');
            if ($this->form_validation->run() === false) {
                $this->session->set_flashdata('error', validation_errors());
            } else {
                $data = [
                    "title"             => $this->input->post('companyname'),
                    "logo"              => '',
                    "address"           => $this->input->post('address'),
                    "contact_number"    => $this->input->post('number'),
                ];

                $this->load->model('organisations');
                if ($this->organisations->put($data, ['id'=>$this->input->post('id')])) {
                    $this->session->set_flashdata('success', 'Company updated!');
                } else {
                    $this->session->set_flashdata('error', 'Error saving to database');
                }
            }
        } else {
            $this->session->set_flashdata('error', 'Invalid Request');
        }
        redirect(base_url().'superadmin');
    }

    public function administer($orgId)
    {
        $session = [
            'orgId'     => $orgId,
            'role'      => 'Admin',
            'origOrgId' => $this->session->orgId,
            'origRole'  => $this->session->role,
        ];

        $this->session->set_userdata($session);

        redirect(base_url() . 'admin');
    }

    public function editorg($id)
    {
        $this->data['data']['content'] = 'superadmin/editcompany';

        $this->load->model('organisations');
        $this->data['data']['organisation'] = $this->organisations->getWhere(['id' => $id])->result_object()[0];

        $this->load->view('common/index', $this->data);
    }

    public function delete()
    {
        if ($this->input->post()) {
            $this->form_validation->set_rules('id', 'Company ID', 'required');
            if ($this->form_validation->run() === false) {
                $this->session->set_flashdata('error', validation_errors());
            } else {
                $this->load->model('organisations');
                $params = ['id' => $this->input->post('id')];
                if ($this->organisations->delete($params)) {
                    $this->session->set_flashdata('success', 'Company deleted!');

                    // DELETE all related to Organisation
                    // 
                    // $this->load->model('materials');
                    // $this->materials->delete($params);
                    // $this->load->model('quotes');
                    // $this->quotes->delete($params);
                    // $this->load->model('surface_areas');
                    // $this->surface_areas->delete($params);
                    // $this->load->model('users');
                    // $this->users->delete($params);

                } else {
                    $this->session->set_flashdata('error', 'Error saving to database');
                }
            }
        } else {
            $this->session->set_flashdata('error', 'Invalid Request');
        }
        redirect(base_url().'superadmin');
    }
}
