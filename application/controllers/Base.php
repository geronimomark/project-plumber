<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Base class
 *
 * Class for Base
 */
class Base extends CI_Controller
{
    protected $data = [];

    public function __construct()
    {
        parent::__construct();

        $this->data['data']['project'] = "Plumber Digups";
        
        $this->load->helper(['excavation', 'computation']);
        $this->load->library('form_validation');
    }

    /**
     * Redirect to home
     *
     * @return void
     */
    public function home()
    {
        switch ($this->session->role) {
            case 'Admin':
                redirect(base_url("admin/index"));
                break;

            case 'Ops':
                redirect(base_url("ops/index"));
                break;

            case 'SuperAdmin':
                redirect(base_url("superadmin/index"));
                break;

            case 'Plumber':
                redirect(base_url("dashboard/index"));
                break;

            default:
                $this->session->set_flashdata('error', 'Session error!');
                redirect(base_url());
                break;
        }
    }

    /**
     * doUpload function
     *
     * doUpload
     *
     * @param  int $quoteNumber Quote Number
     *
     * @return void
     */
    protected function doUpload($quoteNumber)
    {
        // if ($this->session->userdata('tempImage')) {
        //     $quoteNumber = $this->session->userdata('tempImage');
        // } elseif (strpos($quoteNumber, 'new') !== false) {
        //     $quoteNumber .= $this->session->userdata('id');
        //     $this->session->set_userdata('tempImage', $quoteNumber);
        // }

        $uploadPath = './uploads/';

        if (!is_dir($uploadPath)) {
            $oldmask = umask(0);
            mkdir($uploadPath, 0777);
            chmod($uploadPath, 0777);
            umask($oldmask);
        }

        $config['upload_path']    = './uploads/';
        $config['allowed_types']  = 'gif|jpg|png';
        $config['max_size']       = 5120; // 5MB
        $config['max_width']      = 0;
        $config['max_height']     = 0;
        $config['is_image']       = 1;
        $config['file_name']      = 'attachment-'.$quoteNumber.'.jpg';
        $config['overwrite']      = true;

        $this->load->library('upload', $config);

        $files = $_FILES;

        $cpt = count($_FILES['userfile']['name']);
        // $this->session->set_userdata('imgCount', $cpt);
        $uploadFlag = false;
        for ($i = 0; $i < $cpt; $i ++) {
            $_FILES['userfile']['name'] = $files['userfile']['name'][$i];
            $_FILES['userfile']['type'] = $files['userfile']['type'][$i];
            $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i];
            $_FILES['userfile']['error'] = $files['userfile']['error'][$i];
            $_FILES['userfile']['size'] = $files['userfile']['size'][$i];

            $config['file_name'] = 'attachment-'.$quoteNumber.'-'.$i.'.jpg';
            $this->upload->initialize($config);
            $uploadFlag = $this->upload->do_upload('userfile');
        }

        if (!$uploadFlag) {
            return array('error' => $this->upload->display_errors());
            // print_r($this->upload->display_errors()); exit;
        } else {
            return array('upload_data' => $this->upload->data());
        }
    }

    protected function generateQuoteId($surfaceId, $refNumber, $address, $suburb, $problem, $script)
    {
        $this->load->model('quotes');

        $data = [
            'surface_id'    => $surfaceId,
            'ref_id'        => $refNumber,
            'address'       => $address,
            'suburb'        => $suburb,
            'problem'       => $problem,
            'script'        => $script,
            'created_by'    => $this->session->id,
            'org_id'        => $this->session->orgId,
        ];

        return $this->quotes->insertAndGetId($data);
    }

    protected function setStaticMaterials($staticMaterials, $quoteId)
    {
        $batchData = [];
        foreach ($staticMaterials->result_object() as $key => $value) {
            $batchData[] = [
                'type'          => $value->type,
                'quote_id'      => $quoteId,
                'material_name' => $value->name,
                'unit_price'    => $value->{$this->input->post('pipeDiameter')},
                'quantity'      => $value->quantity,
                'total_price'   => computeCost($value->{$this->input->post('pipeDiameter')}, $value->quantity),
            ];
        }
        return $batchData;
    }

    protected function setBatch($object, $quoteId, $qty)
    {
        $batchData = [];
        $batchData[] = [
            'type'          => $object->type,
            'quote_id'      => $quoteId,
            'material_name' => $object->name,
            'unit_price'    => $object->{$this->input->post('pipeDiameter')},
            'quantity'      => $qty,
            'total_price'   => computeCost($object->{$this->input->post('pipeDiameter')}, $qty),
        ];
        return $batchData;
    }

    protected function setTimeMaterials($timeMaterials, $quoteId, $timeQuantity)
    {
        $batchData = [];
        foreach ($timeMaterials->result_object() as $key => $value) {
            $batchData[] = [
                'type'          => $value->type,
                'quote_id'      => $quoteId,
                'material_name' => ($value->key == 'day_rate') ? 'Days Required' : 'Hours Required',
                'unit_price'    => $value->{$this->input->post('pipeDiameter')},
                'quantity'      => $timeQuantity,
                'total_price'   => computeCost($value->{$this->input->post('pipeDiameter')}, $timeQuantity),
            ];
        }
        return $batchData;
    }

    protected function setMachineMaterials($machineMaterials, $quoteId, $machineQty)
    {
        $batchData = [];
        foreach ($machineMaterials->result_object() as $key => $value) {
            if ('firstday_machineaccess' == $value->key) {
                $batchData[] = [
                    'type'          => $value->type,
                    'quote_id'      => $quoteId,
                    'material_name' => 'First Day of Machine Access',
                    'unit_price'    => $value->{$this->input->post('pipeDiameter')},
                    'quantity'      => 1,
                    'total_price'   => computeCost($value->{$this->input->post('pipeDiameter')}, 1),
                ];
            } else {
                $batchData[] = [
                    'type'          => $value->type,
                    'quote_id'      => $quoteId,
                    'material_name' => 'Days of Machine Access',
                    'unit_price'    => $value->{$this->input->post('pipeDiameter')},
                    'quantity'      => $machineQty - 1,
                    'total_price'   => computeCost($value->{$this->input->post('pipeDiameter')}, $machineQty - 1),
                ];
            }
        }
        return $batchData;
    }

    protected function setSpecialPlants($quoteId = null)
    {
        $batchData = [];
        if (is_array($this->input->post('specialPlants'))) {
            foreach ($this->input->post('specialPlants') as $key => $value) {
                $specialplants = explode("|", $value);
                switch ($this->input->post('pipeDiameter')) {
                    case 'onehundredmm':
                        $unitPrice = $specialplants[2];
                        break;
                    
                    case 'onefiftymm':
                        $unitPrice = $specialplants[3];
                        break;

                    case 'twotwentyfivemm':
                        $unitPrice = $specialplants[4];
                        break;

                    case 'threehundredmm':
                        $unitPrice = $specialplants[5];
                        break;

                    default:
                        $unitPrice = $specialplants[2];
                        break;
                }
                $batchData[] = [
                    'type'          => 3,
                    'quote_id'      => $quoteId,
                    'material_name' => $specialplants[1],
                    'unit_price'    => $unitPrice,
                    'quantity'      => 1,
                    'total_price'   => $unitPrice,
                ];
            }
        }

        if ($this->input->post('excludeCouncilFees')) {
            // Set Council Fees
            $batchData[] = [
                'type'          => 3,
                'quote_id'      => $quoteId,
                'material_name' => 'Council Fees',
                'unit_price'    => $this->input->post('councilFees'),
                'quantity'      => 1,
                'total_price'   => $this->input->post('councilFees'),
            ];
        }
        
        return $batchData;
    }

    /**
     * [basePostQuote description]
     *
     * @return [type] [description]
     */
    protected function basePostQuote()
    {
        if ($this->input->post()) {

            $surfaceId   = $this->input->post('surfaceArea');
            $refNumber   = $this->input->post('refNumber');
            $address     = $this->input->post('address');
            $suburb      = $this->input->post('suburb');
            $problem     = $this->input->post('problem');
            $script      = $this->input->post('script');

            $quoteId = $this->generateQuoteId($surfaceId, $refNumber, $address, $suburb, $problem, $script);

            if ($quoteId > 0) {

                $this->load->model('materials');

                $category    = $this->input->post('sewerOrStorm');
                $pipeSize    = $this->input->post('existingPipe');
                $location    = $this->input->post('location');

                // Set batch data for Static Materials
                $staticMaterials = $this->materials->getStaticMaterials($surfaceId);
                $staticMaterialsArr = $this->setStaticMaterials($staticMaterials, $quoteId);

                // Get surface name
                $this->load->model('surface');
                $surfaceAreaName = $this->surface->getSurfaceAreaNameById($surfaceId)[0]['name'];

                // Get Dynamic materials
                $keyArr = $this->materials->getWhere('`key` IS NOT NULL AND `org_id` = '. $this->session->orgId);
                $pvcArr = [];
                $steelReoArr = [];
                $gravelArr = [];
                $topSoilArr = [];
                $turfArr = [];
                $rubbishArr = [];
                $concreteArr = [];
                $pebblesArr = [];
                $bitumenArr = [];
                $roadPlatesArr = [];

                foreach ($keyArr->result_object() as $key => $value) {
                    switch ($value->key) {
                        // Set batch data for PVC Pipe
                        case 'pvc':
                            $excavation = getExcavationLength($this->input->post('lengthOfEx'));
                            $pvcArr = $this->setBatch($value, $quoteId, $excavation);
                            break;
                        
                        // Set batch data for Steel / Reo
                        case 'steelreo':
                            if(strpos($surfaceAreaName,'Grass') > -1 || strpos($surfaceAreaName,'Garden') > -1) {break;}
                            $steelReoLength = getSteelReoLength($this->input->post('steelReo'));
                            $steelReoArr = $this->setBatch($value, $quoteId, $steelReoLength);
                            break;
                        // Set batch data for Gravel
                        case 'gravel':
                            if(strpos($surfaceAreaName,'Grass') > -1 || strpos($surfaceAreaName,'Garden') > -1) {break;}
                            $gravelLength = getGravelLength($this->input->post('gravel'));
                            $gravelArr = $this->setBatch($value, $quoteId, $gravelLength);
                            break;

                        // Set batch data for Top Soil
                        case 'topsoil':
                            if(strpos($surfaceAreaName,'Grass') > -1 || strpos($surfaceAreaName,'Garden') > -1)
                            {
                                $topSoilLength = getTopsoilLength($this->input->post('topSoil'));
                                $topSoilArr = $this->setBatch($value, $quoteId, $topSoilLength);
                            }
                            break;

                        // Set batch data for Turf
                        case 'turf':
                            if(strpos($surfaceAreaName,'Grass') > -1)
                            {
                                $turfLength = getTurfLength($this->input->post('turf'));
                                $turfArr = $this->setBatch($value, $quoteId, $turfLength);
                            }
                            break;

                        // Set batch data for Rubbish
                        case 'rubbish':
                            $rubbishLength = getRubbishLength($this->input->post('rubbish'));
                            $rubbishArr = $this->setBatch($value, $quoteId, $rubbishLength);
                            break;

                        // Set batch data for concrete
                        case 'concrete':
                            $concreteLength = getConcreteLength($this->input->post('concrete'));
                            $concreteArr = $this->setBatch($value, $quoteId, $concreteLength);
                            break;

                        // Set batch data for pebbles
                        case 'pebbles':
                            if(strpos($surfaceAreaName,'Grass') > -1 || strpos($surfaceAreaName,'Garden') > -1) {break;}
                            $pebblesLength = getPebblesLength($this->input->post('pebbles'));
                            $pebblesArr = $this->setBatch($value, $quoteId, $pebblesLength);
                            break;
                        // Set batch data for bitumen
                        case 'bitumen':
                            if(strpos($surfaceAreaName,'Grass') > -1 || strpos($surfaceAreaName,'Garden') > -1) {break;}
                            $bitumenLength = getPebblesLength($this->input->post('bitumen'));
                            $bitumenArr = $this->setBatch($value, $quoteId, $bitumenLength);
                            break;
                        // Set batch data for Road Plates
                        case 'road_plates':
                            $roadPlatesArr = $this->setBatch($value, $quoteId, $this->input->post('roadPlates'));
                            break;

                        default:
                            # code...
                            break;
                    }
                }

                // Set batch data for Materials by Day / Hour rate
                $timeline      = $this->input->post('daysOrHours');
                $timeQuantity  = $this->input->post('numberOfDaysOrHours');
                $params['key']    = $timeline;
                $params['org_id'] = $this->session->orgId;
                $timeMaterials = $this->materials->getWhere($params);
                $timeMaterialsArr = $this->setTimeMaterials($timeMaterials, $quoteId, $timeQuantity);

                // Set batch data for Machine access
                $machineAccess = $this->input->post('machineAccess');
                $machineQty    = $this->input->post('machineDays');
                $machineMaterialsArr = [];
                if ($machineAccess == 'yes') {
                    if ($machineQty > 1) {
                        $params = '`org_id` = '.$this->session->orgId.' AND (`key` = "alldays_machineaccess" OR `key` = "firstday_machineaccess")';
                    } else {
                        $params['key'] = 'firstday_machineaccess';
                    }
                    $machineMaterials = $this->materials->getWhere($params);
                    $machineMaterialsArr = $this->setMachineMaterials($machineMaterials, $quoteId, $machineQty);
                }

                // Set batch special data
                $specialArr = $this->setSpecialPlants($quoteId);

                $batchData = combineArrays([$staticMaterialsArr, $pvcArr, $steelReoArr, $gravelArr, $topSoilArr, $turfArr, $rubbishArr, $concreteArr, $pebblesArr, $bitumenArr, $roadPlatesArr, $timeMaterialsArr, $machineMaterialsArr, $specialArr]);

                $this->load->model('quotematerials');

                if ($this->quotematerials->insertBatch($batchData)) {
                    // Compute SubCost
                    $subCost = 0;
                    foreach ($batchData as $key => $value) {
                        $subCost += $value['total_price'];
                    }
                    $this->quotes->put(['sub_cost'=>$subCost], ['id'=>$quoteId]);

                    // for ($i=0; $i < $this->session->userdata('imgCount'); $i++) {
                    //     rename('./uploads/attachment-'.$this->session->userdata('tempImage').'-'.$i.'.jpg', './uploads/attachment-'.$quoteId.'-'.$i.'.jpg');
                    // }
                    $i = 1;
                    foreach ($this->session->userdata('jqueryuploader') as $key => $value) {
                        rename('./uploads/'. $value[0]->name, './uploads/attachment-'.$quoteId.'-'.$i.'.jpg');
                        rename('./uploads/thumbnail/'. $value[0]->name, './uploads/thumbnail/attachment-'.$quoteId.'-'.$i.'.jpg');
                        $i++;
                    }
                    $this->session->set_flashdata('success', 'Quote Saved!');
                    // $this->session->unset_userdata('tempImage');
                    // $this->session->unset_userdata('imgCount');
                    $this->session->unset_userdata('jqueryuploader');
                    redirect(base_url($this->input->post('redirectSuccess')));
                }
            } else {
                $this->session->set_flashdata('error', 'Error while saving Quote!');
                redirect(base_url($this->input->post('redirectError')));
            }
        }
    }

    /**
     * [validateForm description]
     *
     * @return boolean
     */
    protected function validateForm()
    {
        $this->form_validation->set_rules('refNumber', 'Reference #', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('suburb', 'Suburb', 'required');
        $this->form_validation->set_rules('sewerOrStorm', 'Sewer or Storm', 'required');
        $this->form_validation->set_rules('problem', 'Problem', 'required');
        $this->form_validation->set_rules('existingPipe', 'Pipe Size', 'required');
        $this->form_validation->set_rules('steelReo', 'Steel / Reo Bar', 'required|numeric|greater_than[0]|less_than[13]');
        $this->form_validation->set_rules('gravel', 'Gravel', 'required|numeric|greater_than[0]|less_than[13]');
        $this->form_validation->set_rules('topSoil', 'Top Soil', 'required|numeric|greater_than[0]|less_than[13]');
        $this->form_validation->set_rules('turf', 'Turf', 'required|numeric|greater_than[0]|less_than[13]');
        $this->form_validation->set_rules('rubbish', 'Rubbish', 'required|numeric|greater_than[0]|less_than[13]');
        $this->form_validation->set_rules('concrete', 'Concrete', 'required|numeric|greater_than[0]|less_than[13]');
        $this->form_validation->set_rules('pebbles', 'Pebbles', 'required|numeric|greater_than[0]|less_than[13]');
        $this->form_validation->set_rules('bitumen', 'Bitumen', 'required|numeric|greater_than[0]|less_than[13]');
        $this->form_validation->set_rules('lengthOfEx', 'Length of Excavation', 'required|numeric|greater_than[0]|less_than[13]');
        $this->form_validation->set_rules('surfaceArea', 'Surface Area', 'required|numeric');
        $this->form_validation->set_rules('location', 'Location', 'required');
        $this->form_validation->set_rules('daysOrHours', 'Days Or Hours', 'required');
        $this->form_validation->set_rules('numberOfDaysOrHours', 'Number Of Days Or Hours', 'required|numeric');
        $this->form_validation->set_rules('machineAccess', 'Machine Access', 'required');
        if ($this->input->post('machineAccess') == 'yes') {
            $this->form_validation->set_rules('machineDays', 'Machine Days', 'required|numeric');
        }
        if ($this->input->post('excludeCouncilFees') == null) {
            $this->form_validation->set_rules('councilFees', 'Council Fees', 'required|numeric');
        }
        // $this->form_validation->set_rules('roadPlates', 'Road Plates', 'required|numeric');

        return $this->form_validation->run();
    }

    /**
     * newJob function
     *
     * newJob page
     *
     * @return void
     */
    protected function baseNewJob($data = ['content' => 'common/newjob', 'css' => 'dashboard'])
    {
        $this->session->set_userdata('jqueryuploader', []);

        $this->load->model('surface');
        $this->load->model('materials');

        $this->data['data']['title']   = 'New Job';
        $this->data['data']['content'] = $data['content'];
        $this->data['data']['js']      = 'common/newjob-js';
        $this->data['data']['css']     = $data['css'];

        $this->data['data']['cssArray'][] = "style.css";
        $this->data['data']['cssArray'][] = "jquery.fileupload.css";
        $this->data['data']['cssArray'][] = "jquery.fileupload-ui.css";

        // $this->data['data']['jsArray'][]  = "vendor/jquery.ui.widget.js";
        $this->data['data']['jsArray'][]  = "jquery.iframe-transport.js";
        $this->data['data']['jsArray'][]  = "jquery.fileupload.js";
        $this->data['data']['jsArray'][]  = "jquery.fileupload-process.js";
        $this->data['data']['jsArray'][]  = "jquery.fileupload-image.js";
        // $this->data['data']['jsArray'][]  = "jquery.fileupload-audio.js";
        // $this->data['data']['jsArray'][]  = "jquery.fileupload-video.js";
        $this->data['data']['jsArray'][]  = "jquery.fileupload-validate.js";
        $this->data['data']['jsArray'][]  = "jquery.fileupload-ui.js";

        $this->data['data']['blueimp'][]  = base_url() . "assets/js/vendor/jquery.ui.widget.js";
        $this->data['data']['blueimp'][]  = "//blueimp.github.io/JavaScript-Templates/js/tmpl.min.js";
        $this->data['data']['blueimp'][]  = "//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js";
        $this->data['data']['blueimp'][]  = "//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js";
 
        // $this->data['data']['jsArray'][]  = "main.js";


        $this->data['data']['surface'] = $this->surface->getWhere(['org_id' => $this->session->orgId]);
        $this->data['data']['specialPlant'] = $this->materials->getWhere(['type' => 3, 'org_id' => $this->session->orgId]);

        $this->load->view('common/index', $this->data);
    }

    /**
     * newJobNext function
     *
     * newJobNext page
     *
     * @return void
     */
    protected function baseNewJobNext($data = ['content' => 'common/newjobnext', 'css' => 'dashboard'])
    {
        if ($this->input->post()) {

            if ($this->validateForm() === false) {
                $this->newJob();
                return;
            }

            $this->load->model('materials');

            $this->data['data']['title']   = 'New Job Continuation';
            $this->data['data']['content'] = $data['content'];
            $this->data['data']['css']     = $data['css'];

            // Ready Static Materials Data
            $this->data['data']['staticMaterials']  = $this->materials->getStaticMaterials($this->input->post('surfaceArea'));

            // Ready Special Plant data
            $this->data['data']['specialPlants'] = $this->setSpecialPlants();

            // Get Dynamic materials
            $quoteId = null;
            $keyArr = $this->materials->getWhere('`key` IS NOT NULL AND `org_id` = '. $this->session->orgId);
            $pvcArr = [];
            $steelReoArr = [];
            $gravelArr = [];
            $topSoilArr = [];
            $turfArr = [];
            $rubbishArr = [];
            $concreteArr = [];
            $pebblesArr = [];
            $bitumenArr = [];
            $roadPlatesArr = [];

            foreach ($keyArr->result_object() as $key => $value) {
                switch ($value->key) {
                    // Set batch data for PVC Pipe
                    case 'pvc':
                        $excavation = getExcavationLength($this->input->post('lengthOfEx'));
                        $pvcArr = $this->setBatch($value, $quoteId, $excavation);
                        break;
                    
                    // Set batch data for Steel / Reo
                    case 'steelreo':
                        $steelReoLength = getSteelReoLength($this->input->post('steelReo'));
                        $steelReoArr = $this->setBatch($value, $quoteId, $steelReoLength);
                        break;

                    // Set batch data for Gravel
                    case 'gravel':
                        $gravelLength = getGravelLength($this->input->post('gravel'));
                        $gravelArr = $this->setBatch($value, $quoteId, $gravelLength);
                        break;

                    // Set batch data for Top Soil
                    case 'topsoil':
                        $topSoilLength = getTopsoilLength($this->input->post('topSoil'));
                        $topSoilArr = $this->setBatch($value, $quoteId, $topSoilLength);
                        break;

                    // Set batch data for Turf
                    case 'turf':
                        $turfLength = getTurfLength($this->input->post('turf'));
                        $turfArr = $this->setBatch($value, $quoteId, $turfLength);
                        break;

                    // Set batch data for Rubbish
                    case 'rubbish':
                        $rubbishLength = getRubbishLength($this->input->post('rubbish'));
                        $rubbishArr = $this->setBatch($value, $quoteId, $rubbishLength);
                        break;

                    // Set batch data for concrete
                    case 'concrete':
                        $concreteLength = getConcreteLength($this->input->post('concrete'));
                        $concreteArr = $this->setBatch($value, $quoteId, $concreteLength);
                        break;

                    // Set batch data for pebbles
                    case 'pebbles':
                        $pebblesLength = getPebblesLength($this->input->post('pebbles'));
                        $pebblesArr = $this->setBatch($value, $quoteId, $pebblesLength);
                        break;

                    // Set batch data for bitumen
                    case 'bitumen':
                        $bitumenLength = getPebblesLength($this->input->post('bitumen'));
                        $bitumenArr = $this->setBatch($value, $quoteId, $bitumenLength);
                        break;

                    // Set batch data for Road Plates
                    case 'road_plates':
                        $roadPlatesArr = $this->setBatch($value, $quoteId, $this->input->post('roadPlates'));
                        break;

                    default:
                        # code...
                        break;
                }
            }

            // Days / Hours
            $timeline      = $this->input->post('daysOrHours');
            $timeQuantity  = $this->input->post('numberOfDaysOrHours');
            $params['key']    = $timeline;
            $params['org_id'] = $this->session->orgId;
            $timeMaterials = $this->materials->getWhere($params);
            $timeArr = $this->setTimeMaterials($timeMaterials, null, $timeQuantity);

            $machineAccess = $this->input->post('machineAccess');
            $machineQty    = $this->input->post('machineDays');
            $machineArr = [];
            if ($machineAccess == 'yes') {
                if ($machineQty > 1) {
                    $params = '`org_id` = '.$this->session->orgId.' AND (`key` = "alldays_machineaccess" OR `key` = "firstday_machineaccess")';
                } else {
                    $params['key'] = 'firstday_machineaccess';
                }
                $machineMaterials = $this->materials->getWhere($params);
                $machineArr = $this->setMachineMaterials($machineMaterials, null, $machineQty);
            }

            $this->data['data']['dynamicMaterials'] = combineArrays([$pvcArr, $steelReoArr, $gravelArr, $topSoilArr, $turfArr, $rubbishArr, $concreteArr, $pebblesArr, $bitumenArr, $roadPlatesArr, $timeArr, $machineArr]);

            $this->load->view('common/index', $this->data);
        } else {
            redirect(base_url($this->data['data']['module'].'/newJob'));
        }
    }

    /**
     * newJobValidate function
     *
     * newJobValidate page
     *
     * @return void
     */
    protected function baseNewJobValidate($data = ['content' => 'common/newjobvalidate', 'css' => 'dashboard'])
    {
        if ($this->input->post()) {

            if ($this->validateForm() === false) {
                $this->newJob();
                return;
            }

            $this->getTotalCost();

            $this->data['data']['title']   = 'New Job Validate';
            $this->data['data']['content'] = $data['content'];
            $this->data['data']['css']     = $data['css'];

            $this->load->model('surface');
            $result = $this->surface->getWhere(['id' => $this->input->post('surfaceArea'), 'org_id' => $this->session->orgId]);

            $script = $result->result_object()[0]->script;
            // sample data
            // $script = 'Lorem |refNumber| ipsum |address| dolor |sewerOrStorm| sit |problem| amet |existingPipe|, consectetur |lengthOfEx| adipiscing |surface| elit. In |location| eu |daysOrHours| feugiat |numberOfDaysOrHours| mi. |machineAccess| Aliquam |machineDays|.';
            //
            $this->data['data']['script'] = $this->fillScript($script);

            $this->load->view('common/index', $this->data);
        } else {
            redirect(base_url($this->data['data']['module'].'/newJob'));
        }
    }

    private function getTotalCost()
    {
        $this->load->model('materials');

        // Ready Static Materials Data
        $this->data['data']['staticMaterials']  = $this->materials->getStaticMaterials($this->input->post('surfaceArea'));

        // Ready Special Plant data
        $this->data['data']['specialPlants'] = $this->setSpecialPlants();

        // Get Dynamic materials
        $quoteId = null;
        $keyArr = $this->materials->getWhere('`key` IS NOT NULL AND `org_id` = '. $this->session->orgId);
        $pvcArr = [];
        $steelReoArr = [];
        $gravelArr = [];
        $topSoilArr = [];
        $turfArr = [];
        $rubbishArr = [];
        $concreteArr = [];
        $pebblesArr = [];
        $bitumenArr = [];
        $roadPlatesArr = [];

        foreach ($keyArr->result_object() as $key => $value) {
            switch ($value->key) {
                // Set batch data for PVC Pipe
                case 'pvc':
                    $excavation = getExcavationLength($this->input->post('lengthOfEx'));
                    $pvcArr = $this->setBatch($value, $quoteId, $excavation);
                    break;
                
                // Set batch data for Steel / Reo
                case 'steelreo':
                    $steelReoLength = getSteelReoLength($this->input->post('steelReo'));
                    $steelReoArr = $this->setBatch($value, $quoteId, $steelReoLength);
                    break;

                // Set batch data for Gravel
                case 'gravel':
                    $gravelLength = getGravelLength($this->input->post('gravel'));
                    $gravelArr = $this->setBatch($value, $quoteId, $gravelLength);
                    break;

                // Set batch data for Top Soil
                case 'topsoil':
                    $topSoilLength = getTopsoilLength($this->input->post('topSoil'));
                    $topSoilArr = $this->setBatch($value, $quoteId, $topSoilLength);
                    break;

                // Set batch data for Turf
                case 'turf':
                    $turfLength = getTurfLength($this->input->post('turf'));
                    $turfArr = $this->setBatch($value, $quoteId, $turfLength);
                    break;

                // Set batch data for Rubbish
                case 'rubbish':
                    $rubbishLength = getRubbishLength($this->input->post('rubbish'));
                    $rubbishArr = $this->setBatch($value, $quoteId, $rubbishLength);
                    break;

                // Set batch data for concrete
                case 'concrete':
                    $concreteLength = getConcreteLength($this->input->post('concrete'));
                    $concreteArr = $this->setBatch($value, $quoteId, $concreteLength);
                    break;

                // Set batch data for pebbles
                case 'pebbles':
                    $pebblesLength = getPebblesLength($this->input->post('pebbles'));
                    $pebblesArr = $this->setBatch($value, $quoteId, $pebblesLength);
                    break;

                // Set batch data for bitumen
                case 'bitumen':
                    $bitumenLength = getPebblesLength($this->input->post('bitumen'));
                    $bitumenArr = $this->setBatch($value, $quoteId, $bitumenLength);
                    break;

                // Set batch data for Road Plates
                case 'road_plates':
                    $roadPlatesArr = $this->setBatch($value, $quoteId, $this->input->post('roadPlates'));
                    break;

                default:
                    # code...
                    break;
            }
        }

        // Days / Hours
        $timeline      = $this->input->post('daysOrHours');
        $timeQuantity  = $this->input->post('numberOfDaysOrHours');
        $params['key']    = $timeline;
        $params['org_id'] = $this->session->orgId;
        $timeMaterials = $this->materials->getWhere($params);
        $timeArr = $this->setTimeMaterials($timeMaterials, null, $timeQuantity);

        $machineAccess = $this->input->post('machineAccess');
        $machineQty    = $this->input->post('machineDays');
        $machineArr = [];
        if ($machineAccess == 'yes') {
            if ($machineQty > 1) {
                $params = '`org_id` = '.$this->session->orgId.' AND (`key` = "alldays_machineaccess" OR `key` = "firstday_machineaccess")';
            } else {
                $params['key'] = 'firstday_machineaccess';
            }
            $machineMaterials = $this->materials->getWhere($params);
            $machineArr = $this->setMachineMaterials($machineMaterials, null, $machineQty);
        }

        $this->data['data']['dynamicMaterials'] = combineArrays([$pvcArr, $steelReoArr, $gravelArr, $topSoilArr, $turfArr, $rubbishArr, $concreteArr, $pebblesArr, $bitumenArr, $roadPlatesArr, $timeArr, $machineArr]);

        $total = 0;
        foreach ($this->data['data']['dynamicMaterials'] as $key => $value) {
            $total += computeCost($value['unit_price'], $value['quantity']);
        }

        foreach ($this->data['data']['staticMaterials']->result_object() as $key => $value) {
            $total += computeCost($value->{$this->input->post('pipeDiameter')}, $value->quantity);
        }

        foreach ($this->data['data']['specialPlants'] as $key => $value) {
            $total += $value['total_price'];
        }

        $this->data['data']['total'] = number_format($total, 2);
    }

    private function fillScript($script)
    {
        foreach ($this->input->post() as $key => $value) {
            if (! is_array($value)) {
                $script = str_replace("|".$key."|", $this->input->post($key), $script);
            }
        }

        // remove this line if admin will add comments in script
        $script .= '<br />Comments: ' . $this->input->post('comments');
        // Add this in script ==> |comments|

        $script .= '<br />Total Cost: $' . $this->data['data']['total'];

        return $script;
    }

    /**
     * [baseQuoteWon description]
     *
     * @return void
     */
    protected function baseQuoteWon()
    {
        if ($this->input->post()) {
            $this->load->model('quotes');
            if ($this->quotes->put(['won' => 1], ['id' => $this->input->post('quoteId')])) {
                $this->session->set_flashdata('success', 'Updated Quote ' . $this->input->post('quoteId') . ' to Won');
            } else {
                $this->session->set_flashdata('error', 'Invalid Quote number!');
            }
        } else {
            $this->session->set_flashdata('error', 'Invalid Quote number!');
        }
        redirect(base_url($this->data['data']['module'].'/index'));
    }

    /**
     * [getQuotes description]
     *
     * @param  string $params Params
     *
     * @return object
     */
    protected function getQuotes($params, $js = 'common/quotes-js')
    {
        if ($js !== null) {
            $this->data['data']['js'] = $js;
        }

        $this->load->model('quotematerials');
        $this->data['data']['currentQuotes'] = $this->quotematerials->getCurrentQuotes($params);

        $this->load->view('common/index', $this->data);

        $this->load->view('common/datatable-js');
    }

    private function generatepdf($id, $true)
    {
        $this->load->model('quotematerials');
        $this->data['quotematerials'] = $this->quotematerials->getQuoteMaterialsJoinQuotes(['quote_id' => $id, 'org_id' => $this->session->orgId]);
        $this->data['id'] = $id;

        return $this->load->view('common/generatepdf', $this->data, $true);
    }

    protected function baseGeneratePdf($quoteId, $email = false)
    {
        //load the view and saved it into $html variable
        $html = $this->generatepdf($quoteId, true);

        // $html = str_replace("pdfbuttons", "pdfbuttons hidden", $html);
        // $html = str_replace("navbar navbar-inverse navbar-fixed-top", "navbar navbar-inverse navbar-fixed-top hidden", $html);
        // $html = str_replace("navbar-collapse collapse", "navbar-collapse collapse hidden", $html);
        // $html = str_replace("col-sm-3 col-md-2 sidebar collapse", "col-sm-3 col-md-2 sidebar collapse hidden", $html);

        // $replace = [
        //     'Toggle navigation',
        //     'Logout',
        //     'Plumber Digups',
        //     'Quotes',
        //     'Quote',
        //     'Configuration',
        //     'Users',
        //     'New Quote',
        //     'Won Quotes',
        //     'Generate PDF Edit',
        //     'Generate PDF',
        //     'Edit',
        //     'Won',
        //     'Quote Won',
        //     '<ul>',
        //     '</ul>',
        //     '<li>',
        //     '</li>',
        //     'Send',
        //     'E-mail',
        //     'Submit',
        //     'X',
        //     'x',
        //     'Close',
        //     'Save',
        //     'changes',
        // ];

        // $html = str_replace($replace, '', $html);

        //this the the PDF filename that user will get to download
        $pdfFilePath = "pdf-".$quoteId.".pdf";
 
        //load mPDF library
        $this->load->library('m_pdf');
 
        //generate the PDF from the given html
        $this->m_pdf->pdf->WriteHTML($html);

        if ($email === false) {
            //download it.
            $this->m_pdf->pdf->Output();
        } else {
            //or email it
            return $this->m_pdf->pdf->Output('', 'S');
        }
    }

    protected function getDuplicateEmail()
    {
        return $this->users->getWhere(["email" => $this->input->post('email')]);
    }

    public function uploader()
    {
        $this->load->library('jqueryuploader');
    }

    public function editUploader()
    {
        if ($this->session->userdata('jqueryuploader')) {
            $this->session->unset_userdata('jqueryuploader');
        }

        $referer = $_SERVER['HTTP_REFERER'];
        $referer = explode("/", $referer);
        $position = count($referer) - 1;
        $id = $referer[$position];

        $count = 0;
        foreach (glob('uploads/attachment-'.$id.'-*') as $file) {
            $count++;
        }
        $count++;

        $rename = "attachment-" . $id . "-" . $count;
        $this->session->set_userdata('jqueryuploaderRename', $rename);

        $this->load->library('jqueryuploader');
    }

    public function sendEmail()
    {
        if ($this->input->post()) {

            $quoteId = $this->input->post('quoteId');
            $email = $this->input->post('email');

            $content = $this->baseGeneratePdf($quoteId, true);

            $pdfPath = './pdf/';

            if (!is_dir($pdfPath)) {
                $oldmask = umask(0);
                mkdir($pdfPath, 0777);
                chmod($pdfPath, 0777);
                umask($oldmask);
            }

            $filename = $quoteId. '-' . date('YmdHis') . '.pdf';

            $pdf = fopen($pdfPath . $filename, 'w');
            fwrite($pdf, $content);
            fclose($pdf);

            $config = [
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://s54.web-servers.com.au',
                'smtp_port' => 465,
                'smtp_user' => 'admin@digups.perceptivemind.com.au',
                'smtp_pass' => 'iqq6kEuED(.D',
                'mailtype' => 'html',
                'charset' => 'iso-8859-1',
                'wordwrap' => true
            ];

            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            $this->email->from('admin@digups.perceptivemind.com.au', 'Plumber admin');

            $this->email->to($email);

            $this->email->subject('Subject here');
            $this->email->message('Message Here');

            // $this->email->attach($_SERVER["DOCUMENT_ROOT"] . '/pdf/' . $filename);
            $this->email->attach(base_url() . '/pdf/' . $filename);

            if ($this->email->send()) {
                $this->session->set_flashdata('success', 'Mail Sent!');
            } else {
                $this->session->set_flashdata('error', 'Failed to send email!');
            }
        } else {
            $this->session->set_flashdata('error', 'Invalid request!');

        }
        redirect(base_url() . $this->data['data']['module']);
    }
}
