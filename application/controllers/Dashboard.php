<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'Base.php';

/**
 * Dashboard class
 *
 * Class for Dashboard
 */
class Dashboard extends Base
{
    /**
     * [__construct description]
     */
    public function __construct()
    {
        parent::__construct();

        if ($this->session->role !== 'Plumber') {
            $this->session->set_flashdata('error', ERR_NOT_ALLOWED);
            redirect(base_url());
        }

        $this->data['data']['title']   = 'Dashboard';
        $this->data['data']['content'] = 'dashboard/index';
        $this->data['data']['css']     = 'dashboard';
        $this->data['data']['module']  = 'dashboard';

    }

    /**
     * [index description]
     *
     * @return void
     */
    public function index()
    {
        $this->load->view('common/index', $this->data);
    }

    /**
     * current function
     *
     * Current page
     *
     * @return void
     */
    public function current()
    {
        $this->data['data']['title']   = 'Current';
        $this->data['data']['content'] = 'dashboard/archives';

        $this->getQuotes('B.won IS NULL AND C.id = ' . $this->session->id);
    }

    /**
     * editArchives function
     *
     * editArchives page
     *
     * @param  $id id
     * @return void
     */
    public function viewQuote($id)
    {
        $this->data['data']['title']   = 'To Do';
        $this->data['data']['content'] = 'dashboard/editarchives';

        $this->load->model('quotes');
        $surface = $this->quotes->getWhere(['id'=>$id]);
        if ($surface->num_rows() > 0) {
            $surfaceId = $surface->result_object()[0]->surface_id;

            $this->load->model('surfacetodo');
            $this->data['data']['todo'] = $this->surfacetodo->getPlumberTodo(['A.surface_id'=>$surfaceId]);

            $this->load->view('common/index', $this->data);
        } else {
            $this->session->set_flashdata('error', 'Invalid Quote Id!');
            redirect(base_url().'dashboard/current');
        }
    }

    /**
     * newJob function
     *
     * newJob page
     *
     * @return void
     */
    public function newJob()
    {
        $this->baseNewJob();
    }

    /**
     * newJobNext function
     *
     * newJobNext page
     *
     * @return void
     */
    public function newJobNext()
    {
        $this->baseNewJobNext();
    }

    /**
     * newJobValidate function
     *
     * newJobValidate page
     *
     * @return void
     */
    public function newJobValidate()
    {
        $this->baseNewJobValidate();
    }

    /**
     * [postQuote description]
     *
     * @return [type] [description]
     */
    public function postQuote()
    {
        $this->basePostQuote();
    }
}
