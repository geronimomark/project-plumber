<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'Base.php';

/**
 * Login class
 *
 * Class for Login
 */
class Login extends Base
{
    /**
     * [__construct description]
     */
    public function __construct()
    {
        parent::__construct();

        $this->data['data']['title']   = 'Login';
        $this->data['data']['content'] = 'login/index';
        $this->data['data']['css']     = 'login';
    }

    /**
     * [index description]
     *
     * @return void
     */
    public function index()
    {
        $this->load->view('common/index', $this->data);

        $this->session->sess_destroy();
    }

    /**
     * [authenticate description]
     *
     * @return void
     */
    public function authenticate()
    {
        if ($this->input->post()) {
            $this->load->model('users');
            $result = $this->users->getWhere([
                'email' => $this->input->post('username')
            ]);

            $data = $result->result_object();

            if (!empty($data[0]) && password_verify($this->input->post('password'), $data[0]->password)) {

                // Set User Session data
                $session = [
                    'id'        => $data[0]->id,
                    'fullName'  => $data[0]->full_name,
                    'email'     => $data[0]->email,
                    'orgId'     => $data[0]->org_id,
                    'role'      => $data[0]->role,
                ];
                $this->session->set_userdata($session);

                switch ($data[0]->role) {
                    case 'Admin':
                        redirect(base_url("admin/index"));
                        break;

                    case 'Ops':
                        redirect(base_url("ops/index"));
                        break;

                    case 'SuperAdmin':
                        redirect(base_url("superadmin/index"));
                        break;

                    case 'Plumber':
                        redirect(base_url("dashboard/index"));
                        break;

                    default:
                        $this->session->set_flashdata('error', 'Database error!');
                        redirect(base_url());
                        break;
                }
            } else {
                $this->session->set_flashdata('error', 'Invalid Username / Password!');
                redirect(base_url());
            }
        }
    }

    public function logoff()
    {
        $this->session->set_flashdata('success', 'You have logged out!');
        redirect(base_url('login'));
    }
}
