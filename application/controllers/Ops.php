<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'Base.php';

/**
 * Ops class
 *
 * Class for Ops
 */
class Ops extends Base
{
    /**
     * [__construct description]
     */
    public function __construct()
    {
        parent::__construct();

        if ($this->session->role !== 'Ops') {
            $this->session->set_flashdata('error', ERR_NOT_ALLOWED);
            redirect(base_url());
        }

        $this->data['data']['title']   = 'Ops';
        $this->data['data']['css']     = 'ops';
        $this->data['data']['module']  = 'ops';
    }

    /**
     * [index description]
     *
     * @return void
     */
    public function index()
    {
        $this->data['data']['content'] = 'ops/index';

        $this->load->view('common/index', $this->data);
    }

    /**
     * Create Quote
     *
     * @return void
     */
    public function newJob()
    {
        $this->baseNewJob();
    }

    /**
     * newJobNext function
     *
     * newJobNext page
     *
     * @return void
     */
    public function newJobNext()
    {
        $this->baseNewJobNext();
    }

    /**
     * newJobValidate function
     *
     * newJobValidate page
     *
     * @return void
     */
    public function newJobValidate()
    {
        $this->baseNewJobValidate();
    }

    /**
     * [postQuote description]
     *
     * @return void
     */
    public function postQuote()
    {
        $this->basePostQuote();
    }

    /**
     * [quoteWon description]
     *
     * @return void
     */
    public function quoteWon()
    {
        $this->baseQuoteWon();
    }

    /**
     * [quotes description]
     *
     * @return void
     */
    public function won()
    {
        $this->data['data']['title']   = 'Ops Quotes';
        $this->data['data']['content'] = 'ops/quotes';

        $this->getQuotes('B.won = 1 AND B.org_id = ' . $this->session->orgId);
    }

    /**
     * [quotes description]
     *
     * @return void
     */
    public function quotes()
    {
        $this->data['data']['title']   = 'Ops Quotes';
        $this->data['data']['content'] = 'ops/quotes';

        $this->getQuotes('B.won IS NULL AND B.org_id = ' . $this->session->orgId);
    }

    /**
     * [viewQuote description]
     *
     * @param  int $id Quote Id
     *
     * @return void
     */
    public function viewQuote($id = null, $pdf = false)
    {
        $this->data['data']['title']   = 'Ops View Quote';
        $this->data['data']['content'] = 'ops/viewquote';

        $this->load->model('quotematerials');
        $this->data['data']['quotematerials'] = $this->quotematerials->getQuoteMaterialsJoinQuotes(['quote_id' => $id, 'org_id' => $this->session->orgId]);

        if ($this->data['data']['quotematerials']->num_rows() === 0) {
            $this->session->set_flashdata('error', 'Invalid Quote Id');
            redirect(base_url().'ops/index');
        }

        $this->data['data']['referer'] = $_SERVER['HTTP_REFERER'];

        return $this->load->view('common/index', $this->data, $pdf);
    }

    /**
     * [editQuote description]
     *
     * @param  int $id Quote Id
     *
     * @return void
     */
    public function editQuote($id = null)
    {
        $this->data['data']['title']   = 'Ops Edit Quote';
        $this->data['data']['content'] = 'ops/editquote';
        $this->data['data']['js']      = 'common/editquote-js';

        $this->data['data']['cssArray'][] = "style.css";
        $this->data['data']['cssArray'][] = "jquery.fileupload.css";
        $this->data['data']['cssArray'][] = "jquery.fileupload-ui.css";

        // $this->data['data']['jsArray'][]  = "vendor/jquery.ui.widget.js";
        $this->data['data']['jsArray'][]  = "jquery.iframe-transport.js";
        $this->data['data']['jsArray'][]  = "jquery.fileupload.js";
        $this->data['data']['jsArray'][]  = "jquery.fileupload-process.js";
        $this->data['data']['jsArray'][]  = "jquery.fileupload-image.js";
        // $this->data['data']['jsArray'][]  = "jquery.fileupload-audio.js";
        // $this->data['data']['jsArray'][]  = "jquery.fileupload-video.js";
        $this->data['data']['jsArray'][]  = "jquery.fileupload-validate.js";
        $this->data['data']['jsArray'][]  = "jquery.fileupload-ui.js";

        $this->data['data']['blueimp'][]  = base_url() . "assets/js/vendor/jquery.ui.widget.js";
        $this->data['data']['blueimp'][]  = "//blueimp.github.io/JavaScript-Templates/js/tmpl.min.js";
        $this->data['data']['blueimp'][]  = "//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js";
        $this->data['data']['blueimp'][]  = "//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js";
 
        // $this->data['data']['jsArray'][]  = "main.js";

        $this->load->model('quotematerials');
        $this->data['data']['quotematerials'] = $this->quotematerials->getQuoteMaterialsJoinQuotes(['quote_id' => $id, 'org_id' => $this->session->orgId]);

        if ($this->data['data']['quotematerials']->num_rows() === 0) {
            $this->session->set_flashdata('error', 'Invalid Quote Id');
            redirect(base_url().'ops/index');
        }

        $this->load->view('common/index', $this->data);
    }

    public function generatePDF($quoteId, $email = false)
    {
        $this->baseGeneratePdf($quoteId, $email);
    }
}
