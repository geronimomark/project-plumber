<?php
/**
 * Computation Helper
 *
 * @package     Plumber
 * @author      Mark Anthony Geronimo <mark_geronimo@rocketmail.com>
 * @copyright   Copyright (c) 2016, Perceptive Mind (http://perceptivemind.com.au/)
 * @license     http://opensource.org/licenses/MIT  MIT License
 * @link        http://perceptivemind.com.au/
 * @since       Version 1.0.0
 */
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Computation Helper
 *
 * @package     Plumber
 * @subpackage  Helpers
 * @category    Helpers
 * @author      Mark Anthony Geronimo <mark_geronimo@rocketmail.com>
 * @link        http://perceptivemind.com.au/
 */

// ------------------------------------------------------------------------

if (! function_exists('computeCost')) {

    function computeCost($unitPrice, $qty)
    {
        return $unitPrice * $qty;
    }

}

if (! function_exists('combineArrays')) {
    function combineArrays($arrays)
    {
        $combinedArray = [];
        foreach ($arrays as $key => $value) {
            foreach ($value as $key2 => $value2) {
                $combinedArray[] = $value2;
            }
        }
        return $combinedArray;
    }
}

if (! function_exists('computeTotalCost')) {
    function computeTotalCost($subCost, $margin, $extraCost)
    {
        return number_format($subCost + (! empty($margin) ? $subCost * ($margin / 100) : 0) + $extraCost, 2, '.', '');
    }
}

if (! function_exists('computeGST')) {
    function computeGST($total)
    {
        return number_format($total * 1.1, 2, '.', '');
    }
}