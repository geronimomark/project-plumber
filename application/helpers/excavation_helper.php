<?php
/**
 * Excavation Helper
 *
 * @package     Plumber
 * @author      Mark Anthony Geronimo <mark_geronimo@rocketmail.com>
 * @copyright   Copyright (c) 2016, Perceptive Mind (http://perceptivemind.com.au/)
 * @license     http://opensource.org/licenses/MIT  MIT License
 * @link        http://perceptivemind.com.au/
 * @since       Version 1.0.0
 */
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Excavation Helper
 *
 * @package     Plumber
 * @subpackage  Helpers
 * @category    Helpers
 * @author      Mark Anthony Geronimo <mark_geronimo@rocketmail.com>
 * @link        http://perceptivemind.com.au/
 */

// ------------------------------------------------------------------------

if (! function_exists('getExcavationLength')) {

    function getExcavationLength($input)
    {
        $map = [
            "1"  => 3,
            "2"  => 4,
            "3"  => 6,
            "4"  => 6,
            "5"  => 8,
            "6"  => 9,
            "7"  => 12,
            "8"  => 12,
            "9"  => 12,
            "10" => 12,
            "11" => 14,
            "12" => 14,
        ];
        if (!empty($map[(string) $input])) {
            return $map[(string) $input];
        }
        return false;
    }

}

if (! function_exists('getSteelReoLength')) {

    function getSteelReoLength($input)
    {
        $map = [
            "1"  => 8,
            "2"  => 14,
            "3"  => 20,
            "4"  => 26,
            "5"  => 32,
            "6"  => 38,
            "7"  => 44,
            "8"  => 50,
            "9"  => 56,
            "10" => 62,
            "11" => 68,
            "12" => 74,
        ];
        if (!empty($map[(string) $input])) {
            return $map[(string) $input];
        }
        return false;
    }

}

if (! function_exists('getGravelLength')) {

    function getGravelLength($input)
    {
        $map = [
            "1"  => 2,
            "2"  => 4,
            "3"  => 6,
            "4"  => 8,
            "5"  => 10,
            "6"  => 12,
            "7"  => 14,
            "8"  => 16,
            "9"  => 18,
            "10" => 20,
            "11" => 22,
            "12" => 24,
        ];
        if (!empty($map[(string) $input])) {
            return $map[(string) $input];
        }
        return false;
    }

}

if (! function_exists('getTopsoilLength')) {

    function getTopsoilLength($input)
    {
        $map = [
            "1"  => 1,
            "2"  => 2,
            "3"  => 3,
            "4"  => 3,
            "5"  => 4,
            "6"  => 5,
            "7"  => 6,
            "8"  => 7,
            "9"  => 8,
            "10" => 9,
            "11" => 11,
            "12" => 12,
        ];
        if (!empty($map[(string) $input])) {
            return $map[(string) $input];
        }
        return false;
    }

}

if (! function_exists('getTurfLength')) {

    function getTurfLength($input)
    {
        $map = [
            "1"  => 1,
            "2"  => 2,
            "3"  => 2,
            "4"  => 3,
            "5"  => 4,
            "6"  => 4,
            "7"  => 5,
            "8"  => 6,
            "9"  => 6,
            "10" => 7,
            "11" => 8,
            "12" => 8,
        ];
        if (!empty($map[(string) $input])) {
            return $map[(string) $input];
        }
        return false;
    }

}

if (! function_exists('getRubbishLength')) {

    function getRubbishLength($input)
    {
        $map = [
            "1"  => 250,
            "2"  => 250,
            "3"  => 350,
            "4"  => 480,
            "5"  => 550,
            "6"  => 650,
            "7"  => 950,
            "8"  => 950,
            "9"  => 950,
            "10" => 1100,
            "11" => 1100,
            "12" => 1500,
        ];
        if (!empty($map[(string) $input])) {
            return $map[(string) $input];
        }
        return false;
    }

}

if (! function_exists('getConcreteLength')) {

    function getConcreteLength($input)
    {
        $map = [
            "1"  => 8,
            "2"  => 16,
            "3"  => 24,
            "4"  => 32,
            "5"  => 40,
            "6"  => 48,
            "7"  => 56,
            "8"  => 64,
            "9"  => 72,
            "10" => 80,
            "11" => 88,
            "12" => 96,
        ];
        if (!empty($map[(string) $input])) {
            return $map[(string) $input];
        }
        return false;
    }

}

if (! function_exists('getPebblesLength')) {

    function getPebblesLength($input)
    {
        $map = [
            "1"  => 2,
            "2"  => 4,
            "3"  => 6,
            "4"  => 8,
            "5"  => 10,
            "6"  => 12,
            "7"  => 14,
            "8"  => 16,
            "9"  => 18,
            "10" => 20,
            "11" => 22,
            "12" => 24,
        ];
        if (!empty($map[(string) $input])) {
            return $map[(string) $input];
        }
        return false;
    }

}

if (! function_exists('getBitumenLength')) {

    function getBitumenLength($input)
    {
        $map = [
            "1"  => 2,
            "2"  => 4,
            "3"  => 6,
            "4"  => 8,
            "5"  => 10,
            "6"  => 12,
            "7"  => 14,
            "8"  => 16,
            "9"  => 18,
            "10" => 20,
            "11" => 22,
            "12" => 24,
        ];
        if (!empty($map[(string) $input])) {
            return $map[(string) $input];
        }
        return false;
    }

}
